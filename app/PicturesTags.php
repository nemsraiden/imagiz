<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PicturesTags extends Model
{
    protected $table = 'pictures_tags';

    public function users(){
        return $this->belongsTo('App\User','users_id','id');
    }
    public function pictures(){
        return $this->belongsTo('App\Pictures');
    }

    protected $fillable = ['users_id','pictures_id'];
}
