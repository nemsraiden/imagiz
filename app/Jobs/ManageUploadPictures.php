<?php

namespace App\Jobs;

use App\Http\Controllers\ImagizController;
use App\Pictures;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Queue;
use Intervention\Image\Facades\Image;

class ManageUploadPictures extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * @var
     */
    private $user_id;
    /**
     * @var
     */
    private $album_id;
    /**
     * @var
     */
    private $name;
    /**
     * @var
     */
    private $directory;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id,$album_id,$name,$directory)
    {
        $this->user_id = $user_id;
        $this->album_id = $album_id;
        $this->name = $name;
        $this->directory = $directory;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try{

            $name_ultra = 'ultra_'.$this->name;
            $name_big = 'big_'.$this->name;
            $name_normal = 'middle_'.$this->name;
            $name_thumb = 'thumb_'.$this->name;
            $name_thumb2 = 'thumb2_'.$this->name;

            if(file_exists('public'.$this->directory.'/'.$this->name)){
                $path = 'public'.$this->directory.'/';
            }
            else if(file_exists(public_path().''.$this->directory.'/'.$this->name)){
                $path = public_path().''.$this->directory.'/';
            }
            else{
                Queue::failing(function ($connection, $job, $data) {

                });
            }

            $img = Image::make($path.$this->name)->orientate();

            $img->interlace();
            if(file_exists($path.$this->name)) unlink($path.$this->name);
            $img->save($path.$this->name);
            list($width, $height, $type, $attr) = getimagesize($path.$this->name);

            // ultra
            if($width >= 2500) {
                $img = Image::make($path.$this->name)->orientate();
                $img->resize(2500, null, function($constraint){
                    $constraint->aspectRatio();
                });
                $img->save($path . $name_ultra);
            } else {
                $img->save($path . $name_ultra);
            }


            // big
            if($width >= 1300) {
                $img = Image::make($path.$this->name)->orientate();
                $img->resize(1300, null, function($constraint){
                    $constraint->aspectRatio();
                });
                $img->save($path . $name_big);
            } else {
                $img->save($path . $name_big);
            }


            // middle
            if($width >= 380) {
                $img->resize(380, null, function($constraint){
                    $constraint->aspectRatio();
                });
                $img->save($path . $name_normal);
            } else {
                $img->save($path . $name_normal);
            }


            // thumb
            $img = Image::make($path.$this->name)->orientate();
            $img->fit(200, 200);
            $img->save($path . $name_thumb);

            // thumb2

            $img = Image::make($path.$this->name)->orientate();
            $img->resize(180, null, function($constraint){
                $constraint->aspectRatio();
            });
            $img->save($path . $name_thumb2);

            // on vérifie que toutes les images existent

            if(file_exists($path.$this->name) && file_exists($path . $name_big) && file_exists($path . $name_normal) && file_exists($path . $name_thumb) && file_exists($path . $name_thumb2)){
                $pic = new Pictures();

                $pic->name = $this->name;
                $pic->directory = $this->directory;
                $pic->albums_id = $this->album_id;
                $pic->users_id = $this->user_id;

                $pic->save();

                imagizController::alertesNewPhotos($this->album_id,$this->user_id);
            }
            else{
                Queue::failing(function ($connection, $job, $data) {
                    error_log('fail');
                });
            }

        }
        catch(\Exception $e){
            Queue::failing(function ($connection, $job, $data) {
                error_log('fail');
            });
        }

    }
}
