<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Queue;
use ZipArchive;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ManageUploadZip extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    use DispatchesJobs;
    /**
     * @var
     */
    private $user_id;
    /**
     * @var
     */
    private $album_id;
    /**
     * @var
     */
    private $zip_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id, $album_id, $zip_name)
    {
        $this->user_id = $user_id;
        $this->album_id = $album_id;
        $this->zip_name = $zip_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $zip_path = public_path().'/uploads/zip/uploaded/';
        $zip_folder = str_replace('.zip','',$this->zip_name);
        $zipArchive = new ZipArchive;
        if(!$zipArchive->open($zip_path.$this->zip_name)){
            Queue::failing(function ($connection, $job, $data) {

            });
        }
        try{
            $zipArchive->extractTo($zip_path.$zip_folder);
        }
        catch(\Exception $e){
            Queue::failing(function ($connection, $job, $data) {

            });
        }

        $zipArchive->close();

        $photosdir = public_path().'/uploads/photos/'.$this->album_id.'/';
        if (!File::exists($photosdir)){
            File::makeDirectory($photosdir, 0777,true);
        }
        $allowed_ext = ['png','jpg','jpeg'];

        $files = scandir($zip_path.$zip_folder);

        foreach($files as $file){
            if($file == '.' || $file == '..') continue;

            $ext = pathinfo($zip_path.$zip_folder.$file, PATHINFO_EXTENSION);
            if(in_array($ext,$allowed_ext)){
                $file_path = $this->user_id.'_'.$this->album_id.'_'.date('YmdHi').'_'.rand(1000,9999).'.'.$ext;
                copy($zip_path.$zip_folder.'/'.$file,$photosdir.$file_path);
                unlink($zip_path.$zip_folder.'/'.$file);

                $job = (new ManageUploadPictures($this->user_id, $this->album_id, $file_path, '/uploads/photos/'.$this->album_id));
                $this->dispatch($job);
            }

        }
        rmdir($zip_path.$zip_folder);
        unlink($zip_path.$this->zip_name);
    }
}
