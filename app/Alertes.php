<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alertes extends Model
{
    protected $fillable = array('creator_users_id','receptor_users_id','type','is_read');

    public function creator(){
        return $this->belongsTo('App\User','creator_users_id','id');
    }

    public function albums(){
        return $this->belongsTo('App\Albums');
    }

    public function users(){
        return $this->belongsTo('App\User','creator_users_id','id');
    }
}
