<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Albums extends Model
{
    protected $fillable = array('nom','description','albums_id');

    public function pictures(){
        return $this->hasMany('App\Pictures');
    }
    public function albums_participants(){
        return $this->hasMany('App\AlbumsParticipants');
    }

    public function users(){
        return $this->hasOne('App\User','id','ID_proprietaire');
    }

    public function canAccess($user_id,$album_id){
        $is_participant = AlbumsParticipants::where('albums_id',$album_id)->where('users_id',$user_id)->exists();
        if($is_participant){
            return true;
        }

        $is_viewer = Viewers::where('albums_id',$album_id)->where('users_id',$user_id)->exists();
        if($is_viewer){
            return true;
        }

        $is_proprietaire = $this->where('id',$album_id)->where('ID_proprietaire',$user_id)->exists();
        if($is_proprietaire){
            return true;
        }

        return false;
    }
}
