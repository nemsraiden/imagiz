<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumsParticipants extends Model
{
    protected $table = 'albums_participants';

    public function users(){
        return $this->belongsTo('App\User');
    }
    public function albums(){
        return $this->belongsTo('App\Albums');
    }

    protected $fillable = ['users_id','albums_id'];
}
