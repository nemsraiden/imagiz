<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Pictures extends Model
{
    protected $fillable = array('name','directory','annee','mois','ID_proprietaire');

    public function albums(){
        return $this->belongsTo('App\Albums');
    }
    public function pictures_tags(){
        return $this->hasMany('App\PicturesTags');
    }

    public function users(){
        return $this->belongsTo('App\User');
    }

    public function getNextOrPreviousUnstaged($id_picture,$id_album,$sign,$is_proprietaire = false){

        if($sign == '<') $order = 'DESC';
        if($sign == '>') $order = 'ASC';

        $picture = $this
            ->select('pictures.id','pictures.albums_id','pictures.name','pictures.directory','pictures.type')
            ->where(function ($query) use ($is_proprietaire) {
                if(!$is_proprietaire) {
                    $query->where('pictures.users_id', Auth::id());
                }
            })
            ->where('pictures.id', $sign, $id_picture)
            ->where('albums_id',$id_album)
            ->where(function ($query) {
                $query->where('type', '=', null)
                    ->orWhere('type', '=', 'personnes');
            })
            ->leftJoin('pictures_tags', function($join) {
                $join->on('pictures_tags.pictures_id', '=', 'pictures.id');
            })
            ->whereNull('pictures_tags.pictures_id')
            ->orderBy('pictures.id', $order)
            ->limit(1)
            ->first();

        return $picture;
    }
}
