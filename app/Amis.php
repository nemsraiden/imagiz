<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amis extends Model
{
    public function users(){
        return $this->belongsTo('App\User','users_id_amis');
    }
}
