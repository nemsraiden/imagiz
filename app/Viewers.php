<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Viewers extends Model
{
    protected $table = 'viewers';

    public function users(){
        return $this->belongsTo('App\User');
    }
    public function albums(){
        return $this->belongsTo('App\Albums');
    }
}
