<?php

namespace App\Http\Controllers;



use App\Alertes;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends ImagizController
{
    public function index(){

        $notifications = Alertes::where('receptor_users_id', Auth::id())->orderBy('is_read','ASC')->orderBy('created_at','DESC')->with('users')->with('albums')->get();

        Alertes::where('receptor_users_id', Auth::id())
            ->with('users')
            ->with('albums')
            ->update(['is_read' => 1]);

        $arianne = [
            'Home' => url('/'),
            'Notifications'=> ''
        ];

        return view('notifications.index', compact('notifications','notifications_old', 'arianne'));
    }

}
