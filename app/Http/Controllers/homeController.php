<?php

namespace App\Http\Controllers;

use App\Albums;
use App\AlbumsParticipants;
use App\MyClass\AlbumsClass;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class homeController extends Controller
{
    public function index(){

        if(!Auth::user()) {
			return view('home');
		} else {
			//return view('home_connected');
			return view('home');
		}

    }

}
