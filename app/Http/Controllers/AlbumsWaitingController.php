<?php

namespace App\Http\Controllers;

use App\Albums;
use App\Pictures;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class AlbumsWaitingController extends ImagizController
{

    public function __construct()
    {

        $this->middleware('auth');
        $this->middleware('isParticipantOrViewer');

        parent::__construct();
    }


    /**
     * retourne la vue du dossier temporaire
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function waiting(){

        // on vérifie si cet album temporaire existe deja
        $album = Albums::where('ID_proprietaire',Auth::user()->id)->where('is_waiting_album',1)->with(['pictures' => function ($query) {
            $query->limit(1); // les vraies images sont récup par ajax
        }])->first();
        $pictures = array();

        if(count($album) > 0){
            $waiting_album_already_created = true;

            $pictures = array();
            $i=0;
            foreach($album->pictures as $picture){

                $pictures[$i]['big'] = $picture->directory.'/'.$picture->name;
                $pictures[$i]['thumb'] = $picture->directory.'/thumb2_'.$picture->name;
                $pictures[$i]['middle'] = $picture->directory.'/middle_'.$picture->name;
                $pictures[$i]['id'] = $picture->id;
                $pictures[$i]['link'] = route('PictureInAlbum',['id' => $album->id, 'id_picture' => $picture->id]);


                $i++;
            }
        }
        else{
            $waiting_album_already_created = false;
        }

        $arianne = [
            'Album' => url('/albums'),
            'Dossier temporaire' => ''
        ];


        $others_albums = Albums::where('ID_proprietaire',Auth::user()->id)->where('is_waiting_album',0)->orderby('nom')->get();
        return view('albums.waiting',compact('waiting_album_already_created','album','pictures','others_albums', 'arianne'));
    }

    /*public function waitingPictures(){
        $album = Albums::where('ID_proprietaire',Auth::user()->id)->where('is_waiting_album',1)->with(['pictures' => function ($query) {
        }])->first();
        $pictures = array();

        if(count($album) > 0){

            $pictures = array();
            $i=0;
            foreach($album->pictures as $picture){

                $pictures[$i]['src'] = $picture->directory.'/'.$picture->name;
                $pictures[$i]['srct'] = $picture->directory.'/middle_'.$picture->name;
                $pictures[$i]['customData'] = $picture->id;


                $i++;
            }
        }

        $results['success'] = true;
        $results['pictures'] = $pictures;
        return Response::json($results);
    }*/


    /**
     * @return mixed
     * retourne un json des images du dossier temporaire
     */
    public function waitingPicturesJson(){
        $album = Albums::where('ID_proprietaire',Auth::user()->id)->where('is_waiting_album',1)->with(['pictures' => function ($query) {
            $query->orderBy('created_at','ASC');
        }])->first();
        $pictures = array();

        if(count($album) > 0){

            $pictures = array();
            $i=0;
            foreach($album->pictures as $picture){

                $pictures[$i]['src'] = $picture->directory.'/'.$picture->name;
                $pictures[$i]['srct'] = $picture->directory.'/middle_'.$picture->name;
                $pictures[$i]['customData'] = $picture->id;
                $pictures[$i]['kind'] = 'image';
                $pictures[$i]['ID'] = $picture->id;
                $pictures[$i]['title'] = '';
                $pictures[$i]['description'] = '';
                $pictures[$i]['imgtWidth'] = 150;
                $pictures[$i]['imgtHeight'] = 99;


                $i++;
            }
        }

        return Response::json($pictures);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * Créé le dossier temporaire
     */
    public function waitingCreate(){

        $album = Albums::where('ID_proprietaire',Auth::user()->id)->where('is_waiting_album',1)->with(['pictures' => function ($query) {
        }])->first();

        if(count($album) == 0){
            // il n'existe pas encore d'album waiting
            $album = new Albums;

            $album->ID_proprietaire = Auth::user()->id;
            $album->is_waiting_album = true;

            $album->save();

            return redirect('/albums/'.$album->id.'/photos');
        }


    }

    /**
     * @param $ids_list
     * @return mixed
     * Supprime une photo du dossier temporaire
     */
    public function waitingDelete($ids_list){
        $ids = explode(',',$ids_list);

        foreach($ids as $id){
            // on vérifie que l'image appartient bien au bon albums
            // on récup l'album dossier temporaire
            $album = Albums::where('ID_proprietaire',Auth::user()->id)->where('is_waiting_album',1)->first();
            $picture = Pictures::where('id',$id)->first();
            if($album->id == $picture->albums_id){

                if(file_exists('uploads/photos/'.$album->id.'/'.$picture->name)){
                    unlink('uploads/photos/'.$album->id.'/'.$picture->name);
                }
                if(file_exists('uploads/photos/'.$album->id.'/thumb_'.$picture->name)){
                    unlink('uploads/photos/'.$album->id.'/thumb_'.$picture->name);
                }
                if(file_exists('uploads/photos/'.$album->id.'/thumb2_'.$picture->name)){
                    unlink('uploads/photos/'.$album->id.'/thumb2_'.$picture->name);
                }
                if(file_exists('uploads/photos/'.$album->id.'/middle_'.$picture->name)){
                    unlink('uploads/photos/'.$album->id.'/middle_'.$picture->name);
                }

                $picture->delete();
            }


        }

        $results['success'] = true;
        return Response::json($results);
    }


    /**
     * @param $ids_list
     * @param $to_album_id
     * @return mixed
     * Déplace une photo du dossier temporaire
     */
    public function waitingMove($ids_list,$to_album_id){
        $ids = explode(',',$ids_list);

        // on vérifie qu'il est bien propriétaire de l'album vers lequel on veut déplacer les images
        $album_to = Albums::where('id',$to_album_id)->first();

        if(isset($album_to) && $this->isProprietaire($album_to,Auth::id())) {

            foreach($ids as $id){
                // on vérifie que l'image appartient bien au bon albums
                // on récup l'album dossier temporaire
                $album = Albums::where('ID_proprietaire',Auth::user()->id)->where('is_waiting_album',1)->first();
                $picture = Pictures::where('id',$id)->first();

                if (!File::exists(public_path().'/uploads/photos/'.$to_album_id.'/')){
                    File::makeDirectory(public_path().'/uploads/photos/'.$to_album_id.'/', 0777,true);
                }

                if($album->id == $picture->albums_id){

                    $picture_oldname = $picture->name;
                    $picture->albums_id = $to_album_id;
                    $picture->name = str_replace('_'.$album->id.'_','_'.$to_album_id.'_',$picture->name);
                    $picture->directory = str_replace('/'.$album->id,'/'.$to_album_id,$picture->directory);

                    if(file_exists('uploads/photos/'.$album->id.'/'.$picture_oldname)){
                        if(!copy('uploads/photos/'.$album->id.'/'.$picture_oldname,'uploads/photos/'.$to_album_id.'/'.$picture->name)){
                            return Response::json(['success' => false]);
                        }
                        unlink('uploads/photos/'.$album->id.'/'.$picture_oldname);
                    }
                    if(file_exists('uploads/photos/'.$album->id.'/thumb_'.$picture_oldname)){
                        if(!copy('uploads/photos/'.$album->id.'/thumb_'.$picture_oldname,'uploads/photos/'.$to_album_id.'/thumb_'.$picture->name)){
                            return Response::json(['success' => false]);
                        }
                        unlink('uploads/photos/'.$album->id.'/thumb_'.$picture_oldname);
                    }
                    if(file_exists('uploads/photos/'.$album->id.'/thumb2_'.$picture_oldname)){
                        if(!copy('uploads/photos/'.$album->id.'/thumb2_'.$picture_oldname,'uploads/photos/'.$to_album_id.'/thumb2_'.$picture->name)){
                            return Response::json(['success' => false]);
                        }
                        unlink('uploads/photos/'.$album->id.'/thumb2_'.$picture_oldname);
                    }
                    if(file_exists('uploads/photos/'.$album->id.'/middle_'.$picture_oldname)){
                        if(!copy('uploads/photos/'.$album->id.'/middle_'.$picture_oldname,'uploads/photos/'.$to_album_id.'/middle_'.$picture->name)){
                            return Response::json(['success' => false]);
                        }
                        unlink('uploads/photos/'.$album->id.'/middle_'.$picture_oldname);
                    }


                    $picture->save();

                }


            }

            $results['success'] = true;
        }

        else{
            $results['success'] = false;
        }


        return Response::json($results);
    }

}
