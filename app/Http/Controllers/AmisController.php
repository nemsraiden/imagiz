<?php

namespace App\Http\Controllers;



use App\Albums;
use App\Amis;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AmisController extends ImagizController
{
    public function index(){

        $amis = $this->getAmis(Auth::user()->id);

        $arianne = [
            'Home' => url('/'),
            'Liste d\'amis' => ''
        ];

        return view('amis.index', compact('amis', 'arianne'));
    }

    public function recherche(Request $request){


        $user_id = Auth::user()->id;

        if(!empty( $request->get('email') )) {

            $searchuser = User::where('email',$request->get('email'))->first();
            if($searchuser === null){

                Session::flash('error', 'Aucun utilisateur ne possède cette adresse email');

                return back();

            }

            else{

                if(Amis::where('users_id',$user_id)->where('users_id_amis',$searchuser->id)->first() !== null){
                    Session::flash('error', 'Tu es déjà amis avec cette personne');

                    return back();
                }
                else{

                    $amis = new Amis();
                    $amis->users_id = $user_id;
                    $amis->users_id_amis = $searchuser->id;
                    $amis->save();

                    Session::flash('success', $searchuser->last_name." ".$searchuser->first_name." a été ajouté a ta liste d'amis");

                    return back();

                }

            }

        }

        else{

            $id_amis = $request->get('q_id');

            if($id_amis == ''){
                Session::flash('error', 'Cet utilisateur ne semble pas exister');

                return back();
            }
            else{
                $searchuser = User::where('id',$id_amis)->first();

                if(Amis::where('users_id',$user_id)->where('users_id_amis',$searchuser->id)->first() !== null){
                    Session::flash('error', 'Tu es déjà amis avec cette personne');

                    return back();
                }
                else{

                    $amis = new Amis();
                    $amis->users_id = $user_id;
                    $amis->users_id_amis = $searchuser->id;
                    $amis->save();

                    Session::flash('success', $searchuser->last_name." ".$searchuser->first_name." a été ajouté a ta liste d'amis");

                    return back();

                }
            }

        }


    }

    public function remove($id){

        Amis::where('users_id',Auth::user()->id)->where('users_id_amis',$id)->delete();

        Session::flash('success', "Votre amis a été retiré de votre liste avec succès");

        return back();

    }

    public function add($id, $album_id){
        if(Amis::where('users_id',Auth::user()->id)->where('users_id_amis',$id)->exists()) {
            Session::flash('error', 'Tu es déjà amis avec cette personne');
            return redirect('/amis');
        } else {

            // on vérifie qu'ils ont bien accès à l'album recçu en paramètre
            // pour éviter d'ajouter n'importe qui
            $album = Albums::where('id', $album_id)->first();
            $listParticipants = array_column($this->getListParticipants($album_id)->toArray(),'users_id');
            $listViewers = array_column($this->getListViewers($album_id)->toArray(),'users_id');

            if(!in_array($id, $listParticipants) && !in_array($id, $listViewers) && $id != $album->ID_proprietaire){
                Session::flash('error', 'Vous ne pouvez pas ajouter cet utilisateur');
                return redirect('/amis');
            }

            $amis = new Amis();
            $amis->users_id = Auth::id();
            $amis->users_id_amis = $id;
            $amis->save();

            $searchuser = User::where('id',$id)->first();

            Session::flash('success', $searchuser->last_name." ".$searchuser->first_name." a été ajouté a ta liste d'amis");
            return redirect('/amis');
        }
    }

}
