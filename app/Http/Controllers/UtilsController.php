<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;


class UtilsController extends ImagizController
{

    public function createNewResizeImage(){

        if(Auth::user()){
            if(Auth::user()->email == 'schoonbroodt.fabian@gmail.com'){


                $this->listFolderFiles('uploads/photos');


            }
        }

    }

    public function listFolderFiles($dir){
        $ffs = scandir($dir);
        foreach($ffs as $ff){
            if($ff != '.' && $ff != '..'){

                if(!is_dir($dir.'/'.$ff)){

                    $nameTab = explode('_',$ff);
                    if(substr_count($ff,'_') == 3){
                        // c'est la photo original
                        list($width, $height, $type, $attr) = getimagesize($dir . '/' . $ff);

                        if(!file_exists($dir.'/big_'.$ff  )) {
                            Log::info('big : '.$dir . '/' . $ff);
                            if($width >= 1300) {
                                echo 'Create middle : '.$ff.'<br/>';
                                $img = Image::make($dir . '/' . $ff);

                                $img->resize(1300, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                });

                                $img->save($dir . '/big_' . $ff);
                            } else {
                                echo 'Create middle (fake) : '.$ff.'<br/>';
                                $img = Image::make($dir . '/' . $ff);
                                $img->save($dir . '/big_' . $ff);
                            }

                        }
                        if(!file_exists($dir.'/ultra_'.$ff  )) {
                            Log::info('ultra : '.$dir . '/' . $ff);
                            if($width >= 2500) {
                                echo 'Create Ultra : '.$ff.'<br/>';
                                $img = Image::make($dir . '/' . $ff);

                                $img->resize(2500, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                });

                                $img->save($dir . '/ultra_' . $ff);
                            } else {
                                echo 'Create Ultra (fake) : '.$ff.'<br/>';
                                $img = Image::make($dir . '/' . $ff);
                                $img->save($dir . '/ultra_' . $ff);
                            }

                        }
                    }

                }

                if(is_dir($dir.'/'.$ff)) $this->listFolderFiles($dir.'/'.$ff);
            }
        }
    }



}