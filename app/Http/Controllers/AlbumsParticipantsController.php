<?php

namespace App\Http\Controllers;

use App\Albums;
use App\AlbumsParticipants;
use App\Alertes;
use App\Pictures;
use App\PicturesTags;
use App\User;
use App\Viewers;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Jenssegers\Date\Date;

class AlbumsParticipantsController extends ImagizController
{
    public function __construct()
    {

        $this->middleware('auth');
        $this->middleware('isProprietaire');

        parent::__construct();
    }


    /**
     * Affiche la liste des participants, permet d'en ajouter et d'en supprimer
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id)
    {

        $album = Albums::find($id);

        $date = new Date('now', 'Europe/Brussels');
        $date->month = $album->mois+1;
        $album->mois =  $date->format('F');

        $album->proprietaire_nom = $this->getAlbumProprietaire($album->ID_proprietaire);
        $album->owner_nom = $this->getAlbumOwner($album->ID_proprietaire);

        $listParticipants = $this->getListParticipants($id);
        $listViewers = $this->getListViewers($id);

        $amis = $this->getAmis(Auth::user()->id);

        $selectAmis = array();
        foreach($amis as $ami){
            $selectAmis[$ami->users->id] = $ami->users->first_name.' '.$ami->users->last_name;
        }

        $arianne = [
            'Album' => url('/albums'),
            $album->nom => url('/albums/'.$album->id),
            'Participants' => ''

        ];

        return view('albums.participants.index', compact('album','listParticipants','listViewers','selectAmis', 'arianne'));
    }


    /**
     * Ajout un participant a un album
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($id, Request $request){

        $id_participant = $request->get('amis_selection');
        $is_admin = $request->get('is_admin');
        $user = User::find($id_participant);
        $album = Albums::find($id);

        if(empty($user) || $id_participant == ''){
            Session::flash('error',"Aucun participant n'a été choisis");
        }
        else if($id_participant == $album->ID_proprietaire){
            Session::flash('error',"Tu ne peux pas choisir le propriétaire comme participant");
        }
        else if(AlbumsParticipants::where('albums_id',$id)->where('users_id',$id_participant)->exists()){
            Session::flash('error',"Cette personne est déjà participant de cet album");
        }
        else if(Viewers::where('albums_id',$id)->where('users_id',$id_participant)->exists()){
            Session::flash('error',"Cette personne est déjà viewer de cet album");
        }
        else{
            $albums_participants = new AlbumsParticipants();
            $albums_participants->users_id = $id_participant;
            $albums_participants->albums_id = $id;
            if($is_admin === 'on'){
                $albums_participants->type_privilege = 'Administrateur';
            }
            $albums_participants->save();


            if(!Alertes::where('receptor_users_id',$id_participant)->where('albums_id',$id)->where('type','newalbumparticipant')->where('is_read',0)->exists()){
                $alerte = new Alertes();
                $alerte->creator_users_id = Auth::user()->id;
                $alerte->receptor_users_id = $id_participant;
                $alerte->albums_id = $id;
                $alerte->count = 1;
                $alerte->type = 'newalbumparticipant';
                $alerte->save();
            }


            Session::flash('success',$user->first_name.' '.$user->last_name.' a été ajouté comme participant a cet album');
        }


        return back();

    }


    /**
     * Ajout un participant a un album. Sauf que cette personne n'est pas encore inscrite sur le site
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store_fictif($id, Request $request){

        $last_name = $request->get('last_name');
        $first_name = $request->get('first_name');

        if($last_name == null || $first_name == null){
            Session::flash('error',"Il faut indiquer un nom et prénom");
            return back();
        }

        $user = new User();
        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->email = 'fictif_'.uniqid().'_'.date('Ymd').'@imagiz.be';
        $user->password = uniqid();
        $user->fictif = true;
        $user->save();


        $albums_participants = new AlbumsParticipants();
        $albums_participants->users_id = $user->id;
        $albums_participants->albums_id = $id;
        $albums_participants->save();

        Session::flash('success',$first_name.' '.$last_name.' a été ajouté comme participant a cet album');



        return back();

    }


    /**
     * retire un participant d'un album
     * @param $id
     * @param $id_participant
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove($id,$id_participant){

        $pictures = Pictures::where('albums_id', $id)->select('id')->get();

        $picturesIds = [];
        foreach($pictures as $picture){
            $picturesIds[] = $picture->id;
        }

        PicturesTags::whereIn('pictures_id',$picturesIds)->where('users_id',$id_participant)->delete();


        DB::table('users')
            ->where('id', $id_participant)
            ->where('fictif', 1)
            ->delete();

        DB::table('albums_participants')
            ->where('users_id', $id_participant)
            ->where('albums_id', $id)
            ->delete();


        if(!Alertes::where('receptor_users_id',$id_participant)->where('albums_id',$id)->where('type','removealbumparticipant')->where('is_read',0)->exists()){
            $alerte = new Alertes();
            $alerte->creator_users_id = Auth::user()->id;
            $alerte->receptor_users_id = $id_participant;
            $alerte->albums_id = $id;
            $alerte->count = 1;
            $alerte->type = 'removealbumparticipant';
            $alerte->save();

        }


        return back();
    }


    /**
     * Retourne la vue pour associe un participant fictif a un vrai utilisateur
     * @param $id
     * @param $id_participant
     * @return \Illuminate\View\View
     */
    public function associer($id,$id_participant){

        $album = Albums::find($id);
        $album->proprietaire_nom = $this->getAlbumProprietaire($album->ID_proprietaire);

        $user_participant_fictif = User::find($id_participant);


        $listParticipants = $this->getListParticipants($id);
        $listViewers = $this->getListViewers($id);

        $amis = $this->getAmis(Auth::user()->id);

        $selectAmis = array();
        foreach($amis as $ami){
            $selectAmis[$ami->users->id] = $ami->users->first_name.' '.$ami->users->last_name;
        }

        $arianne = [
            'Album' => url('/albums'),
            $album->nom => url('/albums/'.$album->id),
            'Participants' => url('/albums/'.$album->id.'/participants'),
            'Associer ' => 'Associer '.$user_participant_fictif->first_name.' '.$user_participant_fictif->last_name

        ];


        return view('albums.participants.associer', compact('album','user_participant_fictif','listParticipants','listViewers','amis','selectAmis', 'arianne'));
    }


    /**
     * associe un participant fictif a un vrai utilisateur
     * @param Request $request
     * @param $id
     * @param $id_participant
     * @return \Illuminate\View\View
     */
    public function associerPost(Request $request,$id,$id_participant){

        $album = Albums::find($id);

        $user_participant_fictif = User::find($id_participant);
        $id_user = $request->get('amis_selection');
        $user = User::find($id_user);
        if(empty($user)){
            Session::flash('error', "Tu as essayé d'associer l'utilisateur fictif à un utilisateur innexistant");
        }
        else{

            if($id_user == $album->ID_proprietaire){
                Session::flash('error',"Tu ne peux pas associé un utilisateur fictif avec le propriétaire de l'album");
            }
            else if(AlbumsParticipants::where('albums_id',$id)->where('users_id',$id_user)->exists()){
                Session::flash('error',"Cette personne est déjà participant de cet album");
            }
            else if(Viewers::where('albums_id',$id)->where('users_id',$id_user)->exists()){
                Session::flash('error',"Cette personne est déjà viewer de cet album");
            }
            else{
                $pictures = Pictures::where('albums_id',$id)->select('id')->get()->toArray();
                $pictures_ids = array_column($pictures, 'id');

                AlbumsParticipants::where('albums_id',$id)->where('users_id',$id_participant)->update(['users_id' => $id_user]);
                PicturesTags::whereIn('pictures_id',$pictures_ids)->where('users_id',$id_participant)->update(['users_id' => $id_user]);

                DB::table('users')
                    ->where('id', $id_participant)
                    ->where('fictif', 1)
                    ->delete();


                if(!Alertes::where('receptor_users_id',$id_user)->where('albums_id',$id)->where('type','newalbumparticipant')->where('is_read',0)->exists()){
                    $alerte = new Alertes();
                    $alerte->creator_users_id = Auth::user()->id;
                    $alerte->receptor_users_id = $id_user;
                    $alerte->albums_id = $id;
                    $alerte->count = 1;
                    $alerte->type = 'newalbumparticipant';
                    $alerte->save();
                }


                Session::flash('success', 'Tu as associé '.$user_participant_fictif->first_name.' '.$user_participant_fictif->last_name.' avec un compte existant ('.$user->first_name.' '.$user->last_name.')');

                return redirect('/albums/'.$album->id.'/participants');

            }
        }

        return back();

    }


    /**
     * Modifie les privilege d'un participant
     * @param $album_id
     * @param $id_participant
     * @param $type_privilege
     * @return \Illuminate\Http\RedirectResponse
     */
    public function privilege($album_id,$id_participant,$type_privilege){

        if($type_privilege == 'Administrateur'){
            AlbumsParticipants::where('albums_id',$album_id)->where('users_id',$id_participant)->update(['type_privilege' => $type_privilege]);
        }
        else if($type_privilege == 'remove'){
            AlbumsParticipants::where('albums_id',$album_id)->where('users_id',$id_participant)->update(['type_privilege' => 'Utilisateur normal']);
        }

        return back();

    }


    /**
     * Permet d'inviter une personne en tant que participant à un album
     * @param $album_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function invitation($album_id){

        $album = Albums::find($album_id);
        $album->proprietaire_nom = $this->getAlbumProprietaire($album->ID_proprietaire);

        $listParticipants = $this->getListParticipants($album_id);
        $listViewers = $this->getListViewers($album_id);

        $arianne = [
            'Album' => url('/albums'),
            $album->nom => url('/albums/'.$album->id),
            'Participants' => url('/albums/'.$album->id.'/participants'),
            'Inviter' => ''

        ];

        return view('albums.participants.inviter', compact('album','listParticipants','listViewers', 'arianne'));

    }


    /**
     * créé un lien qui permet à des gens de rejoindre un album en tant que participant
     * @param $album_id
     * @return mixed
     */
    public function invitationTokenGenerate($album_id)
    {
        $album = Albums::find($album_id);

        if ($album->token_participants == '') {
            $album->token_participants = str_random(20);
        }

        Albums::where('id', $album_id)->update(['token_participants' => $album->token_participants]);

        return response()->json(['success' => true, 'token' => $album->token_participants]);
    }


    /**
     * retire le lien qui permets à des gens de rejoindr eun album en tant que participant
     * @param $album_id
     * @return mixed
     */
    public function invitationTokenRemove($album_id){

        Albums::where('id', $album_id)->update(['token_participants' => '']);

        return response()->json(['success' => true]);
    }



}
