<?php
namespace App\Http\Controllers;


use Google_Client;
use Google_Http_MediaFileUpload;
use Google_Service_YouTube;
use Google_Service_YouTube_Video;
use Google_Service_YouTube_VideoSnippet;
use Google_Service_YouTube_VideoStatus;
use Illuminate\Support\Facades\Session;

class VideosController extends ImagizController{

    public function uploadYoutube(){


        $OAUTH2_CLIENT_ID = '523943823359-qeuhjghvhgi9k86l47e8l7g4qkunamgk.apps.googleusercontent.com';
        $OAUTH2_CLIENT_SECRET = 'ztZ_Qxyxvo1gi3S7qTkEg9FE';

        $client = new Google_Client();
        $client->setClientId($OAUTH2_CLIENT_ID);
        $client->setClientSecret($OAUTH2_CLIENT_SECRET);
        $client->setScopes('https://www.googleapis.com/auth/youtube');
        $redirect = filter_var('http://' . $_SERVER['HTTP_HOST'] .'/video/upload/',
            FILTER_SANITIZE_URL);
        $client->setRedirectUri($redirect);
        $client->setAccessType("offline");
        $client->refreshToken('ya29.GlvGBAdDqKYrX-QHpP8HhwmNMNg00AIBmWFaGC41Eapd9ge92HI30DxOrg-XF8AbDfPuNvLlwpG2wAduTiDsLG-JYzfnQbQc-TsjwDIzeLEHYWIQV8YLFCUTVfue');

        // Define an object that will be used to make all API requests.
        $youtube = new Google_Service_YouTube($client);

        // Check if an auth token exists for the required scopes
        $tokenSessionKey = 'token-' . $client->prepareScopes();
        if (isset($_GET['code'])) {

            if (strval(Session::get('state')) !== strval($_GET['state'])) {
                die('The session state did not match.');
            }

            $client->authenticate($_GET['code']);

            Session::set($tokenSessionKey,$client->getAccessToken());
            header('Location: ' . $redirect);
        }

        if (Session::has($tokenSessionKey) )  {
            $client->setAccessToken(Session::get($tokenSessionKey));
        }

        // Check to ensure that the access token was successfully acquired.
        if ($client->getAccessToken()) {

            $htmlBody = '';

            try{
                // REPLACE this value with the path to the file you are uploading.
                $videoPath = public_path().'/uploads/videos/tmp/'.Session::get('file_to_upload_name');
                // Create a snippet with title, description, tags and category ID
                // Create an asset resource and set its snippet metadata and type.
                // This example sets the video's title, description, keyword tags, and
                // video category.
                $snippet = new Google_Service_YouTube_VideoSnippet();
                $snippet->setTitle("Test title");
                $snippet->setDescription("Test description");
                $snippet->setTags(array("tag1", "tag2"));

                // Numeric video category. See
                // https://developers.google.com/youtube/v3/docs/videoCategories/list
                $snippet->setCategoryId("22");

                // Set the video's status to "public". Valid statuses are "public",
                // "private" and "unlisted".
                $status = new Google_Service_YouTube_VideoStatus();
                $status->privacyStatus = "unlisted";

                // Associate the snippet and status objects with a new video resource.
                $video = new Google_Service_YouTube_Video();
                $video->setSnippet($snippet);
                $video->setStatus($status);

                // Specify the size of each chunk of data, in bytes. Set a higher value for
                // reliable connection as fewer chunks lead to faster uploads. Set a lower
                // value for better recovery on less reliable connections.
                $chunkSizeBytes = 1 * 1024 * 1024;

                // Setting the defer flag to true tells the client to return a request which can be called
                // with ->execute(); instead of making the API call immediately.
                $client->setDefer(true);

                // Create a request for the API's videos.insert method to create and upload the video.
                $insertRequest = $youtube->videos->insert("status,snippet", $video);

                // Create a MediaFileUpload object for resumable uploads.
                $media = new Google_Http_MediaFileUpload(
                    $client,
                    $insertRequest,
                    'video/*',
                    null,
                    true,
                    $chunkSizeBytes
                );
                $media->setFileSize(filesize($videoPath));


                // Read the media file and upload it chunk by chunk.
                $status = false;
                $handle = fopen($videoPath, "rb");
                while (!$status && !feof($handle)) {
                    $chunk = fread($handle, $chunkSizeBytes);
                    $status = $media->nextChunk($chunk);
                }

                fclose($handle);

                // If you want to make other calls after the file upload, set setDefer back to false
                $client->setDefer(false);


                $htmlBody .= "<h3>Video Uploaded</h3><ul>";
                $htmlBody .= sprintf('<li>%s (%s)</li>',
                    $status['snippet']['title'],
                    $status['id']);

                $htmlBody .= '</ul>';

            } catch (Google_Service_Exception $e) {
                $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
                    htmlspecialchars($e->getMessage()));
            } catch (Google_Exception $e) {
                $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
                    htmlspecialchars($e->getMessage()));
            }
            Session::set($tokenSessionKey,$client->getAccessToken());

        }  else {
            // If the user hasn't authorized the app, initiate the OAuth flow
            $state = mt_rand();
            $client->setState($state);
            Session::put('state', $state);
            $authUrl = $client->createAuthUrl();


        }

        return view('video.upload',compact('htmlBody','authUrl'));



    }


}