<?php

namespace App\Http\Controllers;

use App\Albums;
use App\AlbumsParticipants;
use App\Alertes;
use App\Viewers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AlbumsJoinController extends ImagizController
{


    public function index($album_id, $token){

        $album = Albums::where('id',$album_id)->where(function ($query) use ($token) {
            $query->where('token_participants', $token)
                ->orWhere('token_viewers', $token);
        })->with(['pictures' => function ($query) {
            $query->orderBy('created_at','desc')->limit(8);
        }])->first();

        if($album == null) return redirect('/');

        $pictures = array();
        $i=0;
        foreach($album->pictures as $picture){

            $pictures[$i]['big'] = $picture->directory.'/'.$picture->name;
            $pictures[$i]['thumb'] = $picture->directory.'/thumb2_'.$picture->name;
            $pictures[$i]['middle'] = $picture->directory.'/middle_'.$picture->name;
            $pictures[$i]['id'] = $picture->id;
            $pictures[$i]['link'] = route('PictureInAlbum',['id' => $album->id, 'id_picture' => $picture->id]);


            $i++;
        }

        return view('albums.join.index',compact('album', 'token', 'pictures'));

    }

    public function sign($album_id, $token){

        if(Auth::user()){

            $album = Albums::find($album_id);

            if($album->token_participants == $token){

                $id_participant = Auth::id();
                $album = Albums::find($album_id);

                if($id_participant == $album->ID_proprietaire){
                    $error = true;
                    $message = "Tu ne peux pas etre propriétaire et participant de cet album";
                }
                else if(AlbumsParticipants::where('albums_id',$album_id)->where('users_id',$id_participant)->exists()){
                    $error = true;
                    $message = "Tu es déjà participant de cet album";
                }
                else if(Viewers::where('albums_id',$album_id)->where('users_id',$id_participant)->exists()){
                    $error = true;
                    $message = "Tu ne peux pas etre participant si tu es déjà viewer de cet album";
                }
                else{

                    $participants = new AlbumsParticipants();
                    $participants->users_id = $id_participant;
                    $participants->albums_id = $album_id;
                    $participants->save();


                    if(!Alertes::where('receptor_users_id',$album->ID_proprietaire)->where('albums_id',$album_id)->where('type','participantjoinalbum')->where('is_read',0)->exists()){
                        $alerte = new Alertes();
                        $alerte->creator_users_id = Auth::id();
                        $alerte->receptor_users_id = $album->ID_proprietaire;
                        $alerte->albums_id = $album_id;
                        $alerte->count = 1;
                        $alerte->type = 'participantjoinalbum';
                        $alerte->save();
                    }

                    $error = false;
                    $message = "Vous avez rejoins l'album ". $album->nom ." en tant que participant";
                }
            }
            else if($album->token_viewers == $token){

                $id_viewer = Auth::id();
                $album = Albums::find($album_id);

                if($id_viewer == $album->ID_proprietaire){
                    $error = true;
                    $message = "Tu ne peux pas etre propriétaire et viewer de cet album";
                }
                else if(AlbumsParticipants::where('albums_id',$album_id)->where('users_id',$id_viewer)->exists()){
                    $error = true;
                    $message = "Tu ne peux pas etre viewer si tu es déjà participant de cet album";
                }
                else if(Viewers::where('albums_id',$album_id)->where('users_id',$id_viewer)->exists()){
                    $error = true;
                    $message = "Tu es déjà viewer de cet album";
                }
                else{

                    $viewer = new Viewers();
                    $viewer->users_id = $id_viewer;
                    $viewer->albums_id = $album_id;
                    $viewer->save();

                    if(!Alertes::where('receptor_users_id',$album->ID_proprietaire)->where('albums_id',$album_id)->where('type','viewerjoinalbum')->where('is_read',0)->exists()){

                        $alerte = new Alertes();
                        $alerte->creator_users_id = $id_viewer;
                        $alerte->receptor_users_id = $album->ID_proprietaire;
                        $alerte->albums_id = $album_id;
                        $alerte->count = 1;
                        $alerte->type = 'viewerjoinalbum';
                        $alerte->save();
                    }

                    $error = false;
                    $message = "Vous avez rejoins l'album ". $album->nom ." en tant que viewer";
                }

            }

            return view('albums.join.sign',compact('album','token','error','message'));
        }
        else{
            return redirect()->guest('/user/login');
        }

    }

}