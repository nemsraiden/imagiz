<?php

namespace App\Http\Controllers;

use App\AlbumsParticipants;
use App\Alertes;
use App\Viewers;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        if(Auth::check()){
            $alertes = new Alertes();

            $myalerts = $alertes->where('receptor_users_id',Auth()->user()->id)
                ->where('is_read',false)
                ->with('creator')
                ->with('albums')
                ->get();

            view()->share('myalerts_count', count($myalerts));

        }
        else{

            view()->share('myalerts_count', 0);
        }

    }



}
