<?php

namespace App\Http\Controllers\Auth;

use App\Alertes;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;


    protected $redirectPath = '/albums';
    protected $loginPath = '/user/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest', ['except' => 'getLogout', 'except' => 'user/inscription']);
        //Session::flush();

        if(Session::get('url.intended') != ''){
            if(strpos(Session::get('url.intended'),'/join/') !== false){
                $this->redirectPath = Session::get('url.intended');
            }

        }

        if(Auth::check()){
            $alertes = new Alertes();

            $myalerts = $alertes->where('receptor_users_id',Auth()->user()->id)
                ->where('is_read',false)
                ->with('creator')
                ->with('albums')
                ->get();

            view()->share('myalerts_count', count($myalerts));

        }
        else{

            view()->share('myalerts_count', 0);
        }

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'last_name' => 'required|max:255',
            'first_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }


    /**
     * Get a validator for an incoming edit request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorEdit(array $data)
    {
        return Validator::make($data, [
            'last_name' => 'required|max:255',
            'first_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.Auth::user()->id,
            'password' => 'confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        Session::flash('success', "Inscription au site réussie !<br/>Merci de vous connecter");

        return User::create([
            'last_name' => $data['last_name'],
            'first_name' => $data['first_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


    /**
     * edit a user.
     *
     * @return view
     */
    protected function edit()
    {

        $user = Auth::user();

        $arianne = [
            'Home' => url('/'),
            'Mon compte'=> ''
        ];

        return view('auth.edit',compact('user', 'arianne'));
    }


    /**
     * update a user.
     *
     * @param  array  $data
     * @return User
     */
    protected function update(Request $request)
    {
        $validator = $this->validatorEdit($request->all());

        if ($validator->fails()) {
            return redirect('user/edit')
                ->withErrors($validator)
                ->withInput();
        }
        else{

            $user = Auth::user();

            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->email = $request->get('email');

            if($request->get('password')){
                $user->password = bcrypt($request->get('password'));
            }

            $user->save();

            Session::flash('success', "Votre compte a été modifié !");

            return view('auth.edit',compact('user'));

        }
    }
}
