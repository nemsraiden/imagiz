<?php

namespace App\Http\Controllers;

use App\Albums;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AlbumsDiaporama extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('isParticipantOrViewer');

        parent::__construct();
    }


    public function index($id){

        $album = Albums::where('id',$id)->with(['pictures' => function ($query) {
            $query->orderBy('created_at','desc');
        }])->first();


        $pictures = array();
        $i=0;
        foreach($album->pictures as $picture){

            $pictures[$i]['ultra'] = $picture->directory.'/ultra_'.$picture->name;
            $pictures[$i]['big'] = $picture->directory.'/big_'.$picture->name;
            $pictures[$i]['thumb'] = $picture->directory.'/thumb2_'.$picture->name;
            $pictures[$i]['middle'] = $picture->directory.'/middle_'.$picture->name;
            $pictures[$i]['id'] = $picture->id;
            $pictures[$i]['link'] = route('PictureInAlbum',['id' => $album->id, 'id_picture' => $picture->id]);


            $i++;
        }

        $arianne = [
            'Album' => url('/albums'),
            'Diaporama' => '',
        ];


        return view('albums.diaporama.index',compact('album','pictures', 'arianne'));
    }
}
