<?php

namespace App\Http\Controllers;

use App\Albums;
use App\AlbumsParticipants;
use App\albumsZip;
use App\Alertes;
use App\Pictures;
use App\PicturesTags;
use App\User;
use Illuminate\Http\Request;
use App\MyClass\AlbumsClass;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use ZipArchive;

class AlbumsPictures extends ImagizController
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('isParticipantOrViewer');

        parent::__construct();
    }


    /**
     * Vue d'une picture dans un album
     * @param PicturesTags $picturesTags
     * @param $id
     * @param $id_picture
     * @return \Illuminate\View\View
     */
    public function index(PicturesTags $picturesTags,$id,$id_picture){


        $album = Albums::find($id);

        $listParticipants = $this->getListParticipants($album->id);
        $album->adminList = $this->getAdminList($listParticipants);

        $picture = Pictures::where('id',$id_picture)->with('users')->first();
        if(!$picture){
            return redirect('/albums/'.$album->id);
        }
        if(preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"])){
            $picture->image = $picture->directory.'/middle_'.$picture->name;

        } else {
            $picture->image = $picture->directory.'/big_'.$picture->name;

        }

        // picture suivante
        $picture->next = $this->getPictureSuivante($id_picture,$id);

        // picture precedante
        $picture->previous = $this->getPicturePrecedante($id_picture,$id);

        // c'est ma photo ou je suis admin ?
        $picture->iamownerandadmin = $this->iamOwnerAndAdmin($picture,Auth::id(),$album->id);

        // liste des participants qui peuvent etre tagué
        $selectInfos = $this->getSelectInfos($id, $picturesTags,$id_picture,$album);

        // liste des personnes tagué
        $tagsList = $this->getTagsList($picturesTags,$id_picture);

        $routePicture = 'PictureInAlbum';

        $album_class = new AlbumsClass();
        $albums_class = $album_class->getAlbums();

        $selectListAlbums = array();
        foreach($albums_class as $one_album){
            if($one_album->id != $id){

                // on vérifie qu'il est admin ou propriétaire.

                $album_test = Albums::where('id',$one_album->id)->first();
                $participants = $this->getListParticipants($one_album->id);
                if($this->isProprietaire($album_test,Auth::id()) or $this->isAdmin($participants, Auth::id())) {
                    $selectListAlbums[$one_album->id] = $one_album->nom;
                }

            }
        }

        $arianne = [
            'Album' => url('/albums'),
            $album->nom => url('/albums/'.$album->id),
            'Photo #'.$picture->id => '',
        ];


        return view('albums.pictures.index',compact('album','picture','selectInfos','tagsList','routePicture','selectListAlbums', 'arianne', 'participants'));
    }


    /**
     * Vue d'une picture dans un album (mais uniquement les non taguées)
     * @param PicturesTags $picturesTags
     * @param $id
     * @param $id_picture
     * @return \Illuminate\View\View
     */
    public function indexUntaged(PicturesTags $picturesTags,$id,$id_picture){


        $album = Albums::find($id);
        $listParticipants = $this->getListParticipants($album->id);
        $album->adminList = $this->getAdminList($listParticipants);

        if(!$this->isProprietaire($album,Auth::id()) and !$this->isAdmin($listParticipants, Auth::id())){
            return redirect('/albums/'.$album->id);
        }

        $picture = Pictures::find($id_picture);
        $picture->image = $picture->directory.'/'.$picture->name;

        // picture suivante
        $picture->next = $this->getPictureSuivante($id_picture,$id,true);

        // picture precedante
        $picture->previous = $this->getPicturePrecedante($id_picture,$id,true);

        // liste des participants qui peuvent etre tagué
        $selectInfos = $this->getSelectInfos($id, $picturesTags,$id_picture,$album);

        // liste des personnes tagué
        $tagsList = $this->getTagsList($picturesTags,$id_picture);

        $routePicture = 'PictureUntagedInAlbum';


        $album_class = new AlbumsClass();
        $albums_class = $album_class->getAlbums();

        $selectListAlbums = array();
        foreach($albums_class as $one_album){
            if($one_album->id != $id){

                // on vérifie qu'il est admin ou propriétaire.

                $album_test = Albums::where('id',$one_album->id)->first();
                $participants = $this->getListParticipants($one_album->id);
                if($this->isProprietaire($album_test,Auth::id()) or $this->isAdmin($participants, Auth::id())) {
                    $selectListAlbums[$one_album->id] = $one_album->nom;
                }

            }
        }

        $arianne = [
            'Album' => url('/albums'),
            $album->nom => url('/albums/'.$album->id),
            'Photo #'.$picture->id => '',
        ];


        return view('albums.pictures.index',compact('album','picture','selectInfos','tagsList','routePicture','selectListAlbums', 'arianne', 'participants'));
    }

    /**
     * récupère l'i de la photo suivante
     * @param $id_picture
     * @param $id_album
     * @return string
     */
    protected function getPictureSuivante($id_picture,$id_album,$untaged = false){

        if($untaged){
            $pictures = new Pictures();
            $picture_next = $pictures->getNextOrPreviousUnstaged($id_picture,$id_album,'>');
            if(isset($picture_next->id)){
                return $picture_next->id;
            }
            else{
                return '';
            }
        }
        else{
            $picture_next = Pictures::where('id', '<', $id_picture)->where('albums_id',$id_album)->orderBy('id', 'desc')->limit(1)->first();
            if(isset($picture_next->id)){
                return $picture_next->id;
            }
            else{
                $picture_next = Pictures::where('id', '>', $id_picture)->where('albums_id',$id_album)->orderBy('id', 'desc')->limit(1)->first();
                if(isset($picture_next->id)) {
                    return $picture_next->id;
                }
            }
        }



    }

    /**
     * récupère l'i de la photo précédente
     * @param $id_picture
     * @return string
     */
    protected function getPicturePrecedante($id_picture,$id_album,$untaged = false){
        if($untaged){
            $pictures = new Pictures();
            $picture_next = $pictures->getNextOrPreviousUnstaged($id_picture,$id_album,'<');
            if(isset($picture_next->id)){
                return $picture_next->id;
            }
            else{
                return '';
            }
        }
        else{
            $picture_next = Pictures::where('id', '>', $id_picture)->where('albums_id',$id_album)->orderBy('id', 'asc')->limit(1)->first();
            if(isset($picture_next->id)){
                return $picture_next->id;
            }
            else{
                $picture_next = Pictures::where('id', '<', $id_picture)->where('albums_id',$id_album)->orderBy('id', 'asc')->limit(1)->first();
                if(isset($picture_next->id)){
                    return $picture_next->id;
                }
            }
        }



    }


    /**
     * Recupère la liste des personnes qui peuvent etre tagué sur une photo
     * @param $id_album
     * @param $picturesTags
     * @param $id_picture
     * @param $album
     * @return array
     */
    protected function getSelectInfos($id_album,$picturesTags,$id_picture,$album){

        // liste des participants a un album
        $listParticipants = $this->getListParticipants($id_album);

        $selectInfos = [];

        // on parcoure les participant d'un album et on vérifie que on ajoute pas dans la liste une personne déjà tagué

        foreach($listParticipants as $participant){
            $user = User::find($participant->users_id);

            $pic_tag = $picturesTags->where('users_id',$user->id)->where('pictures_id',$id_picture)->get();

            if ($pic_tag == null or $pic_tag->isEmpty()){
                $selectInfos[$participant->users_id] = $user->first_name.' '.$user->last_name;
            }


        }

        // on récupère le propriétaire de l'album et on vérifie qu'il n'est pas déjà tagué

        $user = User::find($album->ID_proprietaire);
        $pic_tag = $picturesTags->where('users_id',$user->id)->where('pictures_id',$id_picture)->get();

        if ($pic_tag == null or $pic_tag->isEmpty()){
            $selectInfos[$user->id] = $user->first_name.' '.$user->last_name;
        }

        asort($selectInfos);

        return $selectInfos;
    }

    /**
     * recupère la liste des personnes tagué sur une photo
     * @param $picturesTags
     * @param $id_picture
     * @return array
     */
    protected function getTagsList($picturesTags,$id_picture){
        $tags = $picturesTags->with('users')->where('pictures_id',$id_picture)->get();

        $tagsList = [];
        $i=0;
        foreach($tags as $tag){
            $temp = $tag->users()->first();
            $tagsList[$i]['nom'] = $temp->first_name.' '.$temp->last_name;
            $tagsList[$i]['id'] = $temp->id;
            $i++;
        }

        return $tagsList;
    }


    /**
     * Ajoute des personnes a une images
     * @param Request $request
     * @param $id_album
     * @param $id_picture
     * @return \Illuminate\Http\JsonResponse
     */
    public function tagUser(Request $request,$id_album,$id_picture){
        $user_id = $request->input('user_id');
        if(empty($user_id)) return response()->json(['success' => false]);
        $album = Albums::find($id_album);

        $participants = $this->getListParticipants($id_album);
        $adminList = [];
        foreach($participants as $participant){
            if($participant->type_privilege == 'Administrateur'){
                $adminList[] = $participant->users->id;
            }
        }


        if($this->isProprietaire($album,Auth::id()) or $this->isAdmin($participants, Auth::id())) {

            $pictures_tags = new PicturesTags();
            $pictures_tags->users_id = $user_id;
            $pictures_tags->pictures_id = $id_picture;

            $pictures_tags->save();


            if($user_id != Auth::id()){
                $alerte = Alertes::where('albums_id',$id_album)
                    ->where('type','newtaged')
                    ->where('is_read',0)
                    ->where('receptor_users_id',$user_id)
                    ->where('pictures_id',$id_picture)
                    ->first();

                if($alerte == null){
                    $alerte = new Alertes();
                    $alerte->creator_users_id = Auth::id();
                    $alerte->receptor_users_id = $user_id;
                    $alerte->albums_id = $id_album;
                    $alerte->pictures_id = $id_picture;
                    $alerte->type = 'newtaged';
                    $alerte->save();
                }
            }


            return response()->json(['success' => true]);
        }


    }

    /**
     * retire une personne d'une image
     * @param Request $request
     * @param $id_album
     * @param $id_picture
     * @return \Illuminate\Http\JsonResponse
     */
    public function tagUserRemove($id_album,$id_picture,$user_id){
        $album = Albums::find($id_album);

        $participants = $this->getListParticipants($id_album);
        $adminList = [];
        foreach($participants as $participant){
            if($participant->type_privilege == 'Administrateur'){
                $adminList[] = $participant->users->id;
            }
        }

        if($this->isProprietaire($album,Auth::id()) or $this->isAdmin($participants, Auth::id())) {
            PicturesTags::where('pictures_id', $id_picture)->where('users_id', $user_id)->delete();

            return response()->json(['success' => true]);
        }


    }


    /**
     * Modifier le type d'une image
     * @param Request $request
     * @param $id_album
     * @param $id_picture
     * @return \Illuminate\Http\JsonResponse
     */
    public function tagType(Request $request,$id_album,$id_picture){
        $picture_type_photo = $request->input('picture_type_photo');

        $allow_types = ['inconnu','paysage','personnes','personnes_non_participants'];

        if(!in_array($picture_type_photo,$allow_types)){
            return response()->json(['success' => false]);
        }

        if($picture_type_photo == 'inconnu') $picture_type_photo = null;

        Pictures::where('id', $id_picture)->update(['type' => $picture_type_photo, 'onlytagged' => 0]);
        PicturesTags::where('pictures_id',$id_picture)->delete();

        return response()->json(['success' => true]);


    }


    /**
     * Considère une image comme la jaquette de l'album
     * @param Request $request
     * @param $id_album
     * @param $id_picture
     * @return \Illuminate\Http\JsonResponse
     */
    public function tagJaquette(Request $request,$id_album,$id_picture){
        $picture_type_photo = $request->input('picture_jaquette');

        if($picture_type_photo == 'true'){
            Pictures::where('albums_id', $id_album)->where('is_jaquette',1)->update(['is_jaquette' => 0]);
            Pictures::where('id', $id_picture)->update(['is_jaquette' => 1]);
        }
        else{
            Pictures::where('id', $id_picture)->update(['is_jaquette' => 0]);
        }



        return response()->json(['success' => true]);


    }



    /**
     * retirer une picture d'un album
     * @param $id_album
     * @param $id_picture
     */
    public function remove($id_album,$id_picture){
        $album = Albums::where('id',$id_album)->with(['pictures' => function ($query) {
        }])->first();

        $participants = $this->getListParticipants($id_album);

        if($this->isProprietaire($album,Auth::id()) or $this->isAdmin($participants, Auth::id())) {

            $picturesList = [];

            foreach($album->pictures as $picture){
                $picturesList[] = $picture->id;
            }

            if(in_array($id_picture,$picturesList)){
                PicturesTags::where('pictures_id',$id_picture)->delete();

                $picture = Pictures::where('id',$id_picture)->first();

                if(file_exists('uploads/photos/'.$id_album.'/'.$picture->name)){
                    unlink('uploads/photos/'.$id_album.'/'.$picture->name);
                }
                if(file_exists('uploads/photos/'.$id_album.'/thumb_'.$picture->name)){
                    unlink('uploads/photos/'.$id_album.'/thumb_'.$picture->name);
                }
                if(file_exists('uploads/photos/'.$id_album.'/thumb2_'.$picture->name)){
                    unlink('uploads/photos/'.$id_album.'/thumb2_'.$picture->name);
                }
                if(file_exists('uploads/photos/'.$id_album.'/middle_'.$picture->name)){
                    unlink('uploads/photos/'.$id_album.'/middle_'.$picture->name);
                }

                $picture->delete();
            }

            Session::flash('success', "L'image a été supprimé.");

            $picture_prec = $this->getPictureSuivante($id_picture,$id_album);
            if($picture_prec != ''){
                return redirect('/albums/'.$id_album.'/pictures/'.$picture_prec);
            }
            else{
                return redirect('/albums/'.$id_album);
            }



        }
    }


    /**
     * faire pivoter une image sur la gauche ou droite
     * @param $id_album
     * @param $id_picture
     * @param $direction
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function pivoter($id_album,$id_picture,$direction){
        $album = Albums::where('id',$id_album)->with(['pictures' => function ($query) {
        }])->first();

        $participants = $this->getListParticipants($id_album);

        if($this->isProprietaire($album,Auth::id()) or $this->isAdmin($participants, Auth::id())) {
            $picturesList = [];

            foreach($album->pictures as $picture){
                $picturesList[] = $picture->id;
            }

            if(in_array($id_picture,$picturesList)){
                $picture = Pictures::where('id',$id_picture)->first();

                if($this->isProprietaire($album,Auth::id()) or $this->iamOwnerAndAdmin($picture,Auth::id(),$album->id)) {

                    $pictureName = explode('.', $picture->name);
                    $chiffre = explode('_', $pictureName[0]);
                    // on change le nom pour eviter d'avoir un probleme de cache
                    $newname = $chiffre[0] . '_' . $chiffre[1] . '_' . $chiffre[2] . '_' . rand(1000, 9999) . '.' . $pictureName[1];


                    $img = Image::make('uploads/photos/' . $id_album . '/' . $picture->name);


                    if ($direction == 'gauche') {
                        $img->rotate(-90);
                    }
                    if ($direction == 'droite') {
                        $img->rotate(+90);
                    }

                    $img->save('uploads/photos/' . $id_album . '/' . $newname);
                    $img2 = $img;

                    $img->fit(200, 200);
                    $img->save('uploads/photos/' . $id_album . '/thumb_' . $newname);


                    $img2->resize(180, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img2->save('uploads/photos/' . $id_album . '/thumb2_' . $newname);


                    $img2->resize(500, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img2->save('uploads/photos/' . $id_album . '/middle_' . $newname);


                    unlink('uploads/photos/' . $id_album . '/' . $picture->name);
                    unlink('uploads/photos/' . $id_album . '/thumb_' . $picture->name);
                    unlink('uploads/photos/' . $id_album . '/thumb2_' . $picture->name);
                    unlink('uploads/photos/' . $id_album . '/middle_' . $picture->name);

                    Pictures::where('id', $id_picture)->update(['name' => $newname]);


                    return redirect('/albums/' . $id_album . '/pictures/' . $id_picture);
                }

            }
        }

        return back();
    }


    /**
     * permettre le téléchargement d'une image
     * @param $id_album
     * @param $id_picture
     * @return \Illuminate\Http\RedirectResponse
     */
    public function download($id_album,$id_picture){
        $album = Albums::where('id',$id_album)->with(['pictures' => function ($query) {
        }])->first();

        $participants = $this->getListParticipants($id_album);

        if($this->isProprietaire($album,Auth::id()) or $this->isAdmin($participants, Auth::id())) {
            $picturesList = [];

            foreach($album->pictures as $picture){
                $picturesList[] = $picture->id;
            }

            if(in_array($id_picture,$picturesList)){
                $picture = Pictures::where('id',$id_picture)->first();

                header('Content-Type: application/force-download');
                header("Content-Transfer-Encoding: binary");
                header('Content-Disposition: attachment; filename='.basename($picture->name));
                header('Pragma: no-cache');
                header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
                header('Expires: 0');
                readfile('uploads/photos/'.$id_album.'/'.$picture->name);
                exit();
            }
        }

        return back();
    }



    /**
     * permettre le téléchargement un album en zip
     * @param $id_album
     * @param $id_picture
     * @return \Illuminate\Http\RedirectResponse
     */
    public function downloadZip($id_album){

        $album = Albums::where('id',$id_album)->with(['pictures' => function ($query) {
        }])->first();

        $participants = $this->getListParticipants($id_album);

        if($this->isProprietaire($album,Auth::id()) or $this->isAdmin($participants, Auth::id())) {

            $zip_name = Auth::id().'_'.$id_album.'_'.date('YmdHis').'.zip';
            $zip_path = public_path().'/uploads/zip/downloaded'.$zip_name;
            $zip = new ZipArchive();
            $zip->open($zip_path,false ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE);

            foreach($album->pictures as $picture){
                $zip->addFile(public_path().$picture->directory.'/'.$picture->name,$picture->name);
            }

            $zip->close();

            $albumsZip =  new albumsZip();
            $albumsZip->user_id = Auth::id();
            $albumsZip->album_id = $id_album;
            $albumsZip->path = $zip_name;
            $albumsZip->save();

            header("Content-Type: application/zip");
            header("Content-Disposition: attachment; filename=$zip_name");
            header("Content-Length: " . filesize('uploads/zip/downloaded'.$zip_name));
            readfile('uploads/zip/downloaded'.$zip_name);
            exit();

        }

        return back();
    }

    /**
     * @param $id_album
     * @param $id_picture
     * @param $onlytagged
     */
    public function onlyTagged($id_album,$id_picture,$onlytagged){

        $album = Albums::where('id',$id_album)->first();

        $participants = $this->getListParticipants($id_album);

        if($this->isProprietaire($album,Auth::id()) or $this->isAdmin($participants, Auth::id())) {
            if($onlytagged == 'true') Pictures::where('id',$id_picture)->update(['onlytagged' => 1]);
            if($onlytagged == 'false') Pictures::where('id',$id_picture)->update(['onlytagged' => 0]);
        }

        return response()->json(['success' => true]);
    }


    /**
     * @param $id_album
     * @param $id_picture
     * @param $ids_albums
     * @param $delete_file
     * @return \Illuminate\Http\JsonResponse
     * déplace une image dans un autre album
     */
    public function move($id_album, $id_picture, $ids_albums, $delete_file){
        // on vérifie que il a le droit de déplacer l'image dans ces albums
        $albums_ids = explode(',',$ids_albums);
        foreach($albums_ids as $album_id){
            $album = Albums::where('id',$album_id)->first();
            $participants = $this->getListParticipants($album_id);
            if(!$this->isProprietaire($album,Auth::id()) and !$this->isAdmin($participants, Auth::id())) {
                return response()->json(['success' => false]);

            }
        }
       $picture =  Pictures::where('id',$id_picture)->first();
        if($picture->albums_id != $id_album){
            // ce n'est pas normal, le nombre doit etre le meme
            return response()->json(['success' => false]);

        }

        foreach($albums_ids as $new_album_id){

            $new_picture = new Pictures();
            $extension = explode('.',$picture->name)[1];
            $new_picture->name = Auth::id().'_'.$new_album_id.'_'.date('YmdHi').'_'.rand(1000,9999).'.'.$extension;
            $new_picture->directory = '/uploads/photos/'.$new_album_id;
            $new_picture->albums_id = $new_album_id;
            $new_picture->users_id = Auth::id();
            $new_picture->save();

            if(!file_exists('uploads/photos/'.$new_album_id.'/')){
                mkdir('uploads/photos/'.$new_album_id.'/');
            }

            if(file_exists('uploads/photos/'.$id_album.'/'.$picture->name)){
                copy('uploads/photos/'.$id_album.'/'.$picture->name,'uploads/photos/'.$new_album_id.'/'.$new_picture->name);
            }
            if(file_exists('uploads/photos/'.$id_album.'/thumb_'.$picture->name)){
                copy('uploads/photos/'.$id_album.'/thumb_'.$picture->name,'uploads/photos/'.$new_album_id.'/thumb_'.$new_picture->name);

            }
            if(file_exists('uploads/photos/'.$id_album.'/thumb2_'.$picture->name)){
                copy('uploads/photos/'.$id_album.'/thumb2_'.$picture->name,'uploads/photos/'.$new_album_id.'/thumb2_'.$new_picture->name);

            }
            if(file_exists('uploads/photos/'.$id_album.'/middle_'.$picture->name)){
                copy('uploads/photos/'.$id_album.'/middle_'.$picture->name,'uploads/photos/'.$new_album_id.'/middle_'.$new_picture->name);

            }

        }

        if($delete_file == 'true'){
            $picture->delete();
            if(file_exists('uploads/photos/'.$id_album.'/'.$picture->name)){
                unlink('uploads/photos/'.$id_album.'/'.$picture->name);
            }
            if(file_exists('uploads/photos/'.$id_album.'/thumb_'.$picture->name)){
                unlink('uploads/photos/'.$id_album.'/thumb_'.$picture->name);
            }
            if(file_exists('uploads/photos/'.$id_album.'/thumb2_'.$picture->name)){
                unlink('uploads/photos/'.$id_album.'/thumb2_'.$picture->name);
            }
            if(file_exists('uploads/photos/'.$id_album.'/middle_'.$picture->name)){
                unlink('uploads/photos/'.$id_album.'/middle_'.$picture->name);
            }
        }

        return response()->json(['success' => true]);

    }


    /**
     * @param $picture
     * @param $connected_user_id
     * @param $adminlist
     * @return bool
     * Je suis propriétaire d'une photo et admin de l'album
     */
    private function iamOwnerAndAdmin($picture, $connected_user_id, $albumid){

        $listParticipants = $this->getListParticipants($albumid);
        $adminList = $this->getAdminList($listParticipants);

        if($picture->users_id == $connected_user_id && in_array($connected_user_id,$adminList)){
            return true;
        }
        return false;
    }


}
