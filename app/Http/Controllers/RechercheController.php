<?php

namespace App\Http\Controllers;

use App\Albums;
use App\AlbumsParticipants;
use App\MyClass\AlbumsClass;
use App\Pictures;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RechercheController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        parent::__construct();

    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $album = new AlbumsClass();
        $albums = $album->getAlbums();

        $selectList = array();
        $ids = [];
        foreach($albums as $album){
            $selectList[$album->id] = $album->nom;
            $ids[] = $album->id;
        }



        $albums_result = [];

        $arianne = [
            'Home' => url('/'),
            'Recherche'=> ''
        ];

        return view('recherche.index',compact('albums','selectList','participants','albums_result', 'arianne'));
    }

    /**
     * @param $albums_ids
     * @return \Illuminate\Http\JsonResponse
     * retourne la liste des participants d'un album
     */
    public function getListParticipantsForAlbum($albums_ids){

        $albums_ids = explode(',',$albums_ids);


        $participants = [];
        foreach($albums_ids as $album_id){
            $listParticipants = $this->getListParticipants([$album_id]);

            $album = Albums::where('id',$album_id)->first();
            $ID_proprietaire = $album->ID_proprietaire;

            if(!in_array($ID_proprietaire,$participants)){
                $proprietaire = User::find($ID_proprietaire);
                $participants[$ID_proprietaire] = $proprietaire->first_name.' '.$proprietaire->last_name;
            }
            foreach($listParticipants as $participant){
                if(!in_array($participant->users->id,$participants)){
                    $participants[$participant->users->id] = $participant->users->first_name.' '.$participant->users->last_name;
                }
            }
        }
        asort($participants);

        return response()->json($participants);
    }





    /**
     * @param Request $request
     * @param Albums $albumsModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function doSearch(Request $request,Albums $albumsModel)
    {
        $albums_selection = $request->input('albums_selection');
        $checkbox_personnes = $request->input('checkbox_personnes');
        $checkbox_paysage = $request->input('checkbox_paysage');
        $personnes_selection = $request->input('personnes_selection');
        $checkbox_together_on_photo = $request->input('checkbox_together_on_photo');
        $checkbox_alone_on_photo = $request->input('checkbox_alone_on_photo');

        // on verifie qu'il est bien participant ou propriétaire ou viewers d'un album
        $albumsIds = [];
        foreach ($albums_selection as $album) {
            if ($albumsModel->canAccess(Auth::user()->id, $album)) $albumsIds[] = $album;
        }

        $whereInType = [];

        if ($checkbox_personnes == 'true' && $checkbox_paysage == 'true') {
            $whereInType[] = 'paysage';
            $whereInType[] = 'personnes';
        } elseif ($checkbox_personnes == 'true') {
            $whereInType[] = 'personnes';
            $whereInType[] = 'personnes_non_participants';
        } elseif ($checkbox_paysage == 'true') {
            $whereInType[] = 'paysage';
        }

        $albums_search = Pictures::whereIn('albums_id', $albumsIds)

            ->where(function ($query) use($whereInType) {
                $query->whereIn('type', $whereInType)
                //->orWhere('type',null)
                ;
            })
            ->with(['users' => function ($query) {
            }])
            ->with(['albums' => function ($query) {
            }])
            ->with(['pictures_tags' => function ($query) use($personnes_selection, $checkbox_alone_on_photo) {
                $query->with(['users' => function ($query) {
                }]);
                if(!empty($personnes_selection) && $checkbox_alone_on_photo != 'true'){
                    // si $checkbox_alone_on_photo = true, on  a demandé a ce que certaines personnes soient seuls sur la photo
                    // donc on est obligé de récupérer toutes les photos ou des personnes sont taguuées pour les trier plus loin...
                    $query->whereIn('users_id',$personnes_selection);
                }

            }]);

        if($checkbox_alone_on_photo == 'true'){
            $albums_search->where('onlytagged',true);
        }

        $albums_result =  $albums_search->get()
            ->shuffle();


        $html = '<div id="lightgallery">';
        foreach ($albums_result as $picture){

            if(!empty($personnes_selection) and count($picture->pictures_tags) == 0){

                continue;
            }


            if($checkbox_together_on_photo == 'true' && !empty($personnes_selection)){
                $all_presents = true;
                $list_tagged_perons_ids = [];
                foreach($picture->pictures_tags as $picture_tags){
                    $list_tagged_perons_ids[] = $picture_tags->users->id;
                }
                foreach($personnes_selection as $ps){
                    if(!in_array($ps,$list_tagged_perons_ids)) {
                        $all_presents = false;
                        continue;
                    }
                }

                // si une des personnes de la list n'est pas taguées sur cette photo, on passe a la suivante
                if(!$all_presents) continue;
            }

            if($checkbox_alone_on_photo == 'true'  && !empty($personnes_selection)){
                $is_alone = true;

                foreach($picture->pictures_tags as $picture_tags){
                    if(!in_array($picture_tags->users->id, $personnes_selection)){
                        $is_alone = false;
                        continue;
                    }

                }

                // si une des personnes tagués n'est pas dans la liste des personnes selectionné, on passe a la photo suivante
                if(!$is_alone) continue;
            }



            $html .= '<a class="jg-entry" href="'.$picture['directory'].'/ultra_'.$picture['name'].'" data-download-url="'.$picture['directory'].'/'.$picture['name'].'" data-sub-html="#desc_'.$picture->id.'">
                    <img src="'.$picture['directory'].'/thumb_'.$picture['name'].'" class="image" />
                    <div class="bgc">
                        <div class="bgd">'.$picture->albums->nom.'</div>
                    </div>
                    <div class="desc" id="desc_'.$picture->id.'">';
                        if(!empty($picture->users)){
                            $html .='<strong>Ajouté par :</strong> '.$picture->users->first_name.' '.$picture->users->last_name.' <br/>';

                        }
                        $html .= '<strong>Type de photo :</strong> '.$picture->type.' <br/>';
                        if($picture->type == 'personnes'){
                            $html .= '<strong>Liste des personnes taguées :</strong> ';

                            foreach($picture->pictures_tags as $picture_tags){
                                $html .= $picture_tags->users->first_name.' '.$picture_tags->users->last_name.',';
                            }
                            $html .= '';
                        }
                $html .= '</div>';
            $html .= '</a>';
        }
        $html .= '</div>';

        return response()->json(['success' => true, 'html' => $html]);

    }




    protected function getListParticipants($ids = []){

        $listParticipants = AlbumsParticipants::with(['users' => function ($query) {

        }])->whereIn('albums_id', $ids)->groupBy('users_id')->get();

        return $listParticipants;
    }
}
