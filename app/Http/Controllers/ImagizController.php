<?php

namespace App\Http\Controllers;

use App\AlbumsParticipants;
use App\Alertes;
use App\Amis;
use App\User;
use App\Viewers;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class ImagizController extends Controller
{

    /**
     * affiche la liste des utilisateurs du site dans un formulaire autocomplete en ajax
     * @return mixed
     */
    public function autocomplete(){

        $term = Input::get('term');

        $results = array();

        $queries = DB::table('users')
            ->where('fictif',0)
            ->where(function($query) use ($term) {
                return $query->where('first_name', 'LIKE', '%'.$term.'%')
                    ->orWhere('last_name', 'LIKE', '%'.$term.'%');
            })
            ->take(5)->get();

        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->id, 'value' => $query->first_name.' '.$query->last_name ];
        }
        return Response::json($results);
    }



    public static function alertesNewPhotos($idalbum,$user_id){

        $participants = AlbumsParticipants::where('albums_id',$idalbum)->whereHas('users', function ($query) {
            $query->where('fictif',0);
        })->get();


        foreach($participants as $participant){

            $alerte = Alertes::where('albums_id',$idalbum)->where('type','newpicture')->where('is_read',0)->where('receptor_users_id',$participant->users_id)->first();
            if($alerte == null){
                $alerte = new Alertes();
                $alerte->creator_users_id = $user_id;
                $alerte->receptor_users_id = $participant->users_id;
                $alerte->albums_id = $idalbum;
                $alerte->type = 'newpicture';
                $alerte->save();
            }
            else{
                $alerte->count++;
                $alerte->save();
            }
        }

    }





    /**
     * @param $listParticipants
     * @return array
     */
    protected function getAdminList($listParticipants){
        $adminList = [];

        foreach($listParticipants as $participant){
            if($participant->type_privilege == 'Administrateur'){
                $adminList[] = $participant->users->id;
            }
        }
        return $adminList;
    }

    /**
     * @param $album
     * @param $user_id
     * @return bool
     */
    protected function isProprietaire($album,$user_id){
        if($album->ID_proprietaire == $user_id) return true;
        else return false;
    }

    /**
     * @param $listParticipants
     * @param $user_id
     * @return bool
     */
    protected function isAdmin($listParticipants,$user_id){

        foreach($listParticipants as $participant){
            if($participant->type_privilege == 'Administrateur'){
                if($user_id == $participant->users->id){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param $album
     * @param $listParticipants
     * @param $user_id
     * @return bool
     */
    protected function isProprietaireOrAdmin($album,$listParticipants,$user_id){

        if($album->ID_proprietaire == $user_id) return true;

        foreach($listParticipants as $participant){
            if($participant->type_privilege == 'Administrateur' && $user_id == $participant->users->id){
                return true;
            }
        }

        return false;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getListParticipants($id){

        $listParticipants = AlbumsParticipants::with(['users' => function ($query) {

        }])->where('albums_id', '=',$id)->get();

        return $listParticipants;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getListViewers($id){
        $listViewers = Viewers::with(['users' => function ($query) {

        }])->where('albums_id', '=',$id)->get();

        return $listViewers;
    }

    /**
     * @param $id_proprietaire
     * @return string
     */
    protected function getAlbumProprietaire($id_proprietaire){
        $proprietaire = User::find($id_proprietaire);

        return $proprietaire->first_name. ' ' .$proprietaire->last_name. ' (Propriétaire)';
    }
    /**
     * @param $id_proprietaire
     * @return string
     */
    protected function getAlbumOwner($id_proprietaire){
        $proprietaire = User::find($id_proprietaire);

        return $proprietaire->first_name. ' ' .$proprietaire->last_name;
    }


    protected function getAmis($id){

        $amis = Amis::where('users_id',$id)->with(['users' => function ($query) {

            $query->orderBy('first_name','asc');

        }])->get();

        return $amis;
    }



}
