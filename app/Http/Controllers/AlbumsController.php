<?php

namespace App\Http\Controllers;

use App\Albums;
use App\AlbumsParticipants;
use App\Alertes;
use App\Http\Requests\AlbumsRequest;
use App\Http\Requests\PhotosRequest;
use App\Jobs\ManageUploadPictures;
use App\Jobs\ManageUploadZip;
use App\MyClass\AlbumsClass;
use App\Pictures;
use App\PicturesCron;
use App\PicturesTags;
use App\User;
use App\Viewers;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Jenssegers\Date\Date;

class AlbumsController extends ImagizController
{

    protected $listMonth = ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'];
    protected $listYear = [];

    public function __construct()
    {

        $this->middleware('auth');
        $this->middleware('isParticipantOrViewer');

        $listYear = [];
        $listYear[0] = 'Pas de date précise';
        $year = Date('Y');

        for($i=0;$i<20;$i++){
            $listYear[$year] = $year;
            $year--;
        }

        $this->listYear = $listYear;

        parent::__construct();
    }













    /**
     * Affiche la liste des albums
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $album = new AlbumsClass();
        $albums = $album->getAlbums(true);

        $last_uploaded_image = Pictures::where('users_id',Auth::user()->id)->orderBy('created_at','desc')->first();

        $arianne = [
            'Home' => url('/'),
            'Liste des albums'=> ''
        ];

        return view('albums.index', compact('albums','last_uploaded_image', 'arianne'));
    }

    /**
     * Affiche la page pour créer un nouvel album
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        $album = new AlbumsClass();
        $albums = $album->getAlbums();

        $listMonth = $this->listMonth;
        $listYear = $this->listYear;

        $arianne = [
            'Album' => url('/albums'),
            'Créer un album photo' => ''
        ];

        return view('albums.create')->with(compact('listYear','listMonth','albums', 'arianne'));
    }

    /**
     * Crée un nouvel album
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(AlbumsRequest $albumsRequest)
    {
        $album = new Albums;

        $album->nom = $albumsRequest->input('nom');
        $album->description = $albumsRequest->input('description');
        $album->annee = $albumsRequest->input('annee');
        $album->mois = $albumsRequest->input('mois');
        $album->ID_proprietaire = Auth::user()->id;

        $album->save();

        Session::flash('success', "Votre nouveau album a été créé !");

        return redirect('/albums');

    }

    /**
     * retourne la vue pour editer un album
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $album = Albums::find($id);

        if(!$this->isProprietaire($album,Auth::id())){
            return redirect('/');
        }

        $album->proprietaire_nom = $this->getAlbumProprietaire($album->ID_proprietaire);

        $date = new Date('now', 'Europe/Brussels');
        $date->month = $album->mois+1;
        $album->mois =  $date->format('F');


        $listMonth = $this->listMonth;
        $listYear = $this->listYear;

        $listParticipants = $this->getListParticipants($id);
        $listViewers = $this->getListViewers($id);
        $amis = $this->getAmis(Auth::id());

        $arianne = [
            'Album' => url('/albums'),
            $album->nom => url('/albums/'.$album->id),
            'Modifier cet album' => ''
        ];

        return view('albums.edit',compact('album','listYear','listMonth','listParticipants','listViewers', 'arianne', 'amis'));
    }


    /**
     * edite un album
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(AlbumsRequest $albumsRequest, $id)
    {
        $album = Albums::find($id);

        if(!$this->isProprietaire($album,Auth::id())){
            return back();
        }
        $album->nom = $albumsRequest->input('nom');
        $album->description = $albumsRequest->input('description');
        $album->annee = $albumsRequest->input('annee');
        $album->mois = $albumsRequest->input('mois');
        $album->save();

        Session::flash('success', "L'album a été correctement modifié");

        return redirect('/albums/'.$album->id);
    }















    /**
     * Affiche un album (les images, etc)
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $album = Albums::where('id',$id)->with(['pictures' => function ($query) {
            $query->orderBy('id','desc');
        }])->first();

        if($album->is_waiting_album){
            return redirect('/albums-waiting/');
        }


        $date = new Date('now', 'Europe/Brussels');
        $date->month = $album->mois+1;
        $album->mois =  $date->format('F');

        $album->owner_nom = $this->getAlbumOwner($album->ID_proprietaire);
        $album->proprietaire_nom = $this->getAlbumProprietaire($album->ID_proprietaire);

        $pictures = array();
        $i=0;
        foreach($album->pictures as $picture){

            $pictures[$i]['big'] = $picture->directory.'/'.$picture->name;
            $pictures[$i]['thumb'] = $picture->directory.'/thumb2_'.$picture->name;
            $pictures[$i]['id'] = $picture->id;

            $i++;
        }

        $listParticipants = $this->getListParticipants($id);
        $listViewers = $this->getListViewers($id);
        $amis = $this->getAmis(Auth::id());

        $album->adminList = $this->getAdminList($listParticipants);

        // next untaged picture
        $picturesModel = new Pictures();
        $next_untaged_picture = $picturesModel->getNextOrPreviousUnstaged(0,$album->id,'>', $this->isProprietaire($album,Auth::id()));

        $arianne = [
            'Album' => url('/albums'),
            $album->nom => '',
        ];

        return view('albums.show',compact('album','pictures','listParticipants','listViewers', 'amis','next_untaged_picture', 'arianne'));
    }


    /**
     * Permet d'aficher la page d'ajout de photo
     *
     * @param  int  $id
     * @return View
     */
    public function photosAddView($id){
        $album = Albums::find($id);

        $participants = $this->getListParticipants($id);
        if(!$this->isProprietaireOrAdmin($album,$participants,Auth::id())){
            return redirect('/albums/'.$album->id);
        }

        if($album->is_waiting_album) {
            $arianne = [
                'Album' => url('/albums'),
                'Dossier temporaire' => url('/albums-waiting'),
                'Ajout de photo' => ''
            ];
        } else {
            $arianne = [
                'Album' => url('/albums'),
                $album->nom => url('/albums/'.$album->id),
                'Ajout de photo' => ''
            ];
        }



        return view('albums.photos',compact('album', 'arianne'));
    }

    /**
     * Permet d'ajouter une photo
     *
     * @param  int  $id
     * @return View
     */
    public function photosAdd($id, PhotosRequest $photosrequest){

        $directory = '/uploads/photos/'.$id;

        $album = Albums::find($id);
        $participants = $this->getListParticipants($id);

        if(!$this->isProprietaireOrAdmin($album,$participants,Auth::id())){
            die();
        }

        if (!File::exists(public_path().$directory)){
            File::makeDirectory(public_path().$directory, 0777,true);
        }

        $files = $photosrequest->file('file');

        foreach ($files as $file) {

            $name = Auth::id().'_'.$id.'_'.date('YmdHi').'_'.rand(1000,9999);
            $file_path = $name.'.'.$file->getClientOriginalExtension();

            $file->move('uploads/photos/'.$id.'/', $name .'.'. $file->getClientOriginalExtension());

            $job = (new ManageUploadPictures(Auth::id(), $id, $file_path, $directory));
            $this->dispatch($job);

        }

        return response()->json(['result' => 'ok']);

    }



    /**
     * supprimé un album
     * @param $album_id
     */
    public function remove($id_album){

        $album = Albums::where('id',$id_album)->first();

        if($this->isProprietaire($album,Auth::id())) {

            $this->removePhotosFromAlbum($id_album);

            if(file_exists('uploads/photos/'.$id_album.'/')){
                rmdir('uploads/photos/'.$id_album.'/');
            }

            $album->delete();

            $participants = AlbumsParticipants::where('albums_id',$id_album)->whereHas('users', function ($query) {
                $query->where('fictif',0);
            })->get();

            foreach($participants as $participant){

                $alerte = new Alertes();
                $alerte->creator_users_id = Auth::id();
                $alerte->receptor_users_id = $participant->users_id;
                $alerte->albums_id = $id_album;
                $alerte->type = 'deletealbum';
                $alerte->save();
            }

            AlbumsParticipants::where('albums_id',$id_album)->delete();



            $viewers = Viewers::where('albums_id',$id_album)->with('users')->get();

            foreach($viewers as $viewer){

                $alerte = new Alertes();
                $alerte->creator_users_id = Auth::id();
                $alerte->receptor_users_id = $viewer->users_id;
                $alerte->albums_id = $id_album;
                $alerte->count = 1;
                $alerte->type = 'deletealbum';
                $alerte->save();
            }

            Viewers::where('albums_id',$id_album)->delete();

            return redirect('/albums');

        }


    }


    /**
     * supprimé toutes les photos d'un album
     * @param $album_id
     */
    public function removePhotos($id_album){

        $album = Albums::where('id',$id_album)->first();

        if($this->isProprietaire($album,Auth::id())) {

            $this->removePhotosFromAlbum($id_album);

            return redirect('/albums');

        }
    }

    /**
     * Uploader un zip contenant des photos
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function uploadZip($id, Request $request){

        $zip_ = Auth::id().'_'.$id.'_'.date('YmdHis');
        $zip_name = $zip_.'.zip';
        $zip_path = public_path().'/uploads/zip/uploaded/';

        $file = $request->file('zip_file');
        if(!$file->getClientOriginalName()){
            return response()->json(['result' => false, 'message' => 'Il y a eu un problème lors de la lecture du zip']);
        }
        $file->move($zip_path,$zip_name);

        $job = (new ManageUploadZip(Auth::id(), $id, $zip_name));
        $this->dispatch($job);


        Session::flash('success', "Votre zip a été importé avec succès. <br/>Le traitement est en cours...");
        return response()->json(['result' => 'ok']);
    }





    /**
     * Permet d'aficher la page d'ajout de video
     *
     * @param  int  $id
     * @return View
     */
    /*public function videosAddView($id){
        $album = Albums::find($id);

        $participants = $this->getListParticipants($id);

        if(!$this->isProprietaireOrAdmin($album,$participants,Auth::id())){
            return back();
        }

        $album->proprietaire_nom = $this->getAlbumProprietaire($album->ID_proprietaire);

        $listParticipants = $this->getListParticipants($id);
        $listViewers = $this->getListViewers($id);

        return view('albums.videos',compact('album','listParticipants','listViewers'));
    }*/


    /**
     * Permet d'ajouter une video
     *
     * @param  int  $id
     * @return View
     */
    /*public function videoAdd($id, PhotosRequest $photosrequest)
    {

        $album = Albums::find($id);
        $participants = $this->getListParticipants($id);

        if (!$this->isProprietaireOrAdmin($album, $participants, Auth::id())) {
            die();
        }

        $file = $photosrequest->file('file');
        if(!is_dir(public_path().'/uploads/videos/tmp/')){
            mkdir(public_path().'/uploads/videos/tmp/',0777,true);
        }
        $file_to_upload_name = $file->getFilename().'.'.$file->getClientOriginalExtension();
        $file->move(public_path().'/uploads/videos/tmp/',$file_to_upload_name);

        Session::put('file_to_upload_name', $file_to_upload_name);

        return redirect('/video/upload');

    }*/


    /**
     * permet de supprimer toutes les photos d'un album
     * @param $id_album
     */
    private function removePhotosFromAlbum($id_album){

        $album = Albums::where('id',$id_album)->with(['pictures' => function ($query) {
        }])->first();

        foreach($album->pictures as $picture){

            PicturesTags::where('pictures_id',$picture->id)->delete();

            $picture = Pictures::where('id',$picture->id)->first();

            if(file_exists('uploads/photos/'.$id_album.'/'.$picture->name)){
                unlink('uploads/photos/'.$id_album.'/'.$picture->name);
            }
            if(file_exists('uploads/photos/'.$id_album.'/thumb_'.$picture->name)){
                unlink('uploads/photos/'.$id_album.'/thumb_'.$picture->name);
            }
            if(file_exists('uploads/photos/'.$id_album.'/thumb2_'.$picture->name)){
                unlink('uploads/photos/'.$id_album.'/thumb2_'.$picture->name);
            }
            if(file_exists('uploads/photos/'.$id_album.'/middle_'.$picture->name)){
                unlink('uploads/photos/'.$id_album.'/middle_'.$picture->name);
            }

            $picture->delete();

        }
    }


    public function config($id)
    {
        $album = Albums::where('id',$id)->with(['pictures' => function ($query) {
            $query->orderBy('id','desc');
        }])->first();

        $date = new Date('now', 'Europe/Brussels');
        $date->month = $album->mois+1;
        $album->mois =  $date->format('F');

        $album->owner_nom = $this->getAlbumOwner($album->ID_proprietaire);
        $album->proprietaire_nom = $this->getAlbumProprietaire($album->ID_proprietaire);

        $listParticipants = $this->getListParticipants($id);
        $listViewers = $this->getListViewers($id);
        $amis = $this->getAmis(Auth::id());

        $album->adminList = $this->getAdminList($listParticipants);

        $arianne = [
            'Album' => url('/albums'),
            $album->nom => url('/albums/'.$album->id),
            'Participants / viewers' => ''

        ];

        return view('albums.config',compact('album','listParticipants','listViewers','amis', 'arianne'));
    }


}
