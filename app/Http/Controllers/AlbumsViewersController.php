<?php

namespace App\Http\Controllers;

use App\Albums;
use App\AlbumsParticipants;
use App\Alertes;
use App\Amis;
use App\Pictures;
use App\PicturesTags;
use App\User;
use App\Viewers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Jenssegers\Date\Date;

class AlbumsViewersController extends ImagizController
{
    public function __construct()
    {

        $this->middleware('auth');
        $this->middleware('isProprietaire');

        parent::__construct();
    }

    /**
     * Affiche la page pour ajouter / supprimer des viewer
     *
     * @return Response
     */
    public function index($id)
    {
        $album = Albums::find($id);

        $date = new Date('now', 'Europe/Brussels');
        $date->month = $album->mois+1;
        $album->mois =  $date->format('F');

        $album->proprietaire_nom = $this->getAlbumProprietaire($album->ID_proprietaire);
        $album->owner_nom = $this->getAlbumOwner($album->ID_proprietaire);

        $listViewers = $this->getListViewers($id);
        $listParticipants = $this->getListParticipants($id);

        $amis = $this->getAmis(Auth::user()->id);

        $selectAmis = array();
        foreach($amis as $ami){
            $selectAmis[$ami->users->id] = $ami->users->first_name.' '.$ami->users->last_name;
        }

        $arianne = [
            'Album' => url('/albums'),
            $album->nom => url('/albums/'.$album->id),
            'Viewers' => ''

        ];

        return view('albums.viewers.index', compact('album','listViewers','listParticipants','selectAmis', 'arianne'));
    }


    /**
     * Ajoute un viewer a un album
     *
     * @param Requests\AlbumsRequest $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($id, Request $request){

        $id_viewer = $request->get('amis_selection');
        $user = User::find($id_viewer);
        $album = Albums::find($id);

        if(empty($user) || $id_viewer == ''){
            Session::flash('error',"Aucun viewer n'a été choisis");
        }
        else if($id_viewer == $album->ID_proprietaire){
            Session::flash('error',"Tu ne peux pas choisir le propriétaire comme viewer");
        }
        else if(AlbumsParticipants::where('albums_id',$id)->where('users_id',$id_viewer)->exists()){
            Session::flash('error',"Cette personne est déjà participant de cet album");
        }
        else if(Viewers::where('albums_id',$id)->where('users_id',$id_viewer)->exists()){
            Session::flash('error',"Cette personne est déjà viewer de cet album");
        }
        else{
            $viewer = new Viewers();
            $viewer->users_id = $id_viewer;
            $viewer->albums_id = $id;
            $viewer->save();

            if(!Alertes::where('receptor_users_id',$id_viewer)->where('albums_id',$id)->where('type','newalbumviewer')->where('is_read',0)->exists()){

                $alerte = new Alertes();
                $alerte->creator_users_id = Auth::user()->id;
                $alerte->receptor_users_id = $id_viewer;
                $alerte->albums_id = $id;
                $alerte->count = 1;
                $alerte->type = 'newalbumviewer';
                $alerte->save();
            }

            Session::flash('success',$user->first_name.' '.$user->last_name.' a été ajouté comme viewer a cet album');
        }


        return back();

    }


    /**
     * retire un viewer d'un album
     *
     * @param $id
     * @param $id_viewer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove($id,$id_viewer){

        DB::table('viewers')
            ->where('users_id', $id_viewer)
            ->where('albums_id', $id)
            ->delete();

        if(!Alertes::where('receptor_users_id',$id_viewer)->where('albums_id',$id)->where('type','removealbumviewer')->where('is_read',0)->exists()){
            $alerte = new Alertes();
            $alerte->creator_users_id = Auth::user()->id;
            $alerte->receptor_users_id = $id_viewer;
            $alerte->albums_id = $id;
            $alerte->count = 1;
            $alerte->type = 'removealbumviewer';
            $alerte->save();
        }


        return back();
    }


    /**
     * Permet d'inviter une personne en tant que viewer à un album
     * @param $album_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function invitation($album_id){

        $album = Albums::find($album_id);
        $album->proprietaire_nom = $this->getAlbumProprietaire($album->ID_proprietaire);

        $listParticipants = $this->getListParticipants($album_id);
        $listViewers = $this->getListViewers($album_id);

        $arianne = [
            'Album' => url('/albums'),
            $album->nom => url('/albums/'.$album->id),
            'Viewers' => url('/albums/'.$album->id.'/viewers'),
            'Inviter' => '',

        ];

        return view('albums.viewers.inviter', compact('album','listParticipants','listViewers', 'arianne'));

    }


    /**
     * créé un lien qui permet à des gens de rejoindre un album en tant que viewer
     * @param $album_id
     * @return mixed
     */
    public function invitationTokenGenerate($album_id)
    {
        $album = Albums::find($album_id);

        if ($album->token_viewers == '') {
            $album->token_viewers = str_random(20);
        }

        Albums::where('id', $album_id)->update(['token_viewers' => $album->token_viewers]);

        return response()->json(['success' => true, 'token' => $album->token_viewers]);
    }


    /**
     * retire le lien qui permets à des gens de rejoindr eun album en tant que viewer
     * @param $album_id
     * @return mixed
     */
    public function invitationTokenRemove($album_id){

        Albums::where('id', $album_id)->update(['token_viewers' => '']);

        return response()->json(['success' => true]);
    }


}
