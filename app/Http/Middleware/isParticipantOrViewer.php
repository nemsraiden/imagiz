<?php

namespace App\Http\Middleware;

use App\Albums;
use App\AlbumsParticipants;
use App\User;
use App\Viewers;
use Closure;
use Illuminate\Support\Facades\Auth;

class isParticipantOrViewer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->route()->albums != ''){
            $album_id = $request->route()->albums;
        }
        else if($request->route()->id != ''){
            $album_id = $request->route()->id;
        }
        else $album_id = '';
        if($album_id != null){
            $user_id = Auth::user()->id;
            $album = Albums::find($album_id);


            $proprietaire = User::find($album->ID_proprietaire);

            $result = albumsParticipants::where('albums_id',$album_id)->where('users_id',$user_id)->first();
            $result2 = Viewers::where('albums_id',$album_id)->where('users_id',$user_id)->first();

            if(empty($result) && empty($result2) && $proprietaire->id != $user_id){
                if ($request->ajax()) {
                    return response('Unauthorized.', 401);
                } else {
                    return redirect('/');
                }
            }
        }

        return $next($request);
    }
}
