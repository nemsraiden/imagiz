<?php

namespace App\Http\Middleware;

use App\Albums;
use App\AlbumsParticipants;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class isParticipant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if($request->route()->albums != null){
            $album_id = $request->route()->albums;
        }
        else if($request->route()->id != null){
            $album_id = $request->route()->id;
        }
        else $album_id = '';

        if($album_id != null){
            $user_id = Auth::user()->id;
            $album = Albums::find($album_id);

            $proprietaire = User::find($album->ID_proprietaire);


            $result = albumsParticipants::where('albums_id',$album_id)->where('users_id',$user_id)->first();
            if(empty($result) && $proprietaire->id != $user_id){
                if ($request->ajax()) {
                    return response('Unauthorized.', 401);
                } else {
                    return redirect('/');
                }
            }
        }

        return $next($request);
    }
}
