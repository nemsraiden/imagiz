<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'homeController@index');


/* ***************************************
Album
**************************************** */
Route::resource('albums', 'AlbumsController');
Route::get('albums/{id}/photos', 'AlbumsController@photosAddView');
Route::post('albums/{id}/photosZip', [
    'as' => 'uploadZip',
    'uses' => 'AlbumsController@uploadZip'
]);
Route::post('albums/{id}/photosPost', [
    'as' => 'PostPhotos',
    'uses' => 'AlbumsController@photosAdd'
]);
Route::get('albums/{id}/remove', 'AlbumsController@remove');
Route::get('albums/{id}/removePhotos', 'AlbumsController@removePhotos');
Route::get('albums/{id}/config', 'AlbumsController@config');



// album participant
Route::get('albums/{id}/participants', 'AlbumsParticipantsController@index');
Route::post('albums/{id}/participants', 'AlbumsParticipantsController@store');
Route::get('albums/{id}/participants/remove/{id_participant}', [
    'as' => 'participantRemove',
    'uses' => 'AlbumsParticipantsController@remove'
]);
Route::post('albums/{id}/participants/add', [
    'as' => 'participantAddFictif',
    'uses' => 'AlbumsParticipantsController@store_fictif'
]);
Route::get('albums/{id}/participants/associer/{id_participant}', [
    'as' => 'participantAssocier',
    'uses' => 'AlbumsParticipantsController@associer'
]);
Route::post('albums/{id}/participants/associer/{id_participant}', 'AlbumsParticipantsController@associerPost');

// changer les privilège d'un participant
Route::get('albums/{id}/participants/privilege/{id_participant}/{type_privilege}', [
    'as' => 'participantPrivilege',
    'uses' => 'AlbumsParticipantsController@privilege'
]);
Route::get('albums/{id}/participants/inviter', 'AlbumsParticipantsController@invitation');
Route::get('albums/{id}/participants/inviter/tokenGenerate', 'AlbumsParticipantsController@invitationTokenGenerate');
Route::get('albums/{id}/participants/inviter/tokenRemove', 'AlbumsParticipantsController@invitationTokenRemove');


// album viewers
Route::get('albums/{id}/viewers', 'AlbumsViewersController@index');
Route::post('albums/{id}/viewers', 'AlbumsViewersController@store');
Route::get('albums/{id}/viewers/remove/{id_viewer}', [
    'as' => 'viewerRemove',
    'uses' => 'AlbumsViewersController@remove'
]);
Route::get('albums/{id}/viewers/inviter', 'AlbumsViewersController@invitation');
Route::get('albums/{id}/viewers/inviter/tokenGenerate', 'AlbumsViewersController@invitationTokenGenerate');
Route::get('albums/{id}/viewers/inviter/tokenRemove', 'AlbumsViewersController@invitationTokenRemove');

//autocomplete
Route::get('search/autocomplete', 'AlbumsController@autocomplete');


// dossier temporaire
Route::get('albums-waiting', 'AlbumsWaitingController@waiting');
//Route::get('albums-waiting/pictures', 'AlbumsWaitingController@waitingPictures';
Route::get('albums-waiting/picturesJson', 'AlbumsWaitingController@waitingPicturesJson');
Route::get('albums-waiting/create', 'AlbumsWaitingController@waitingCreate');
Route::get('albums-waiting/delete/{ids}', 'AlbumsWaitingController@waitingDelete');
Route::get('albums-waiting/move/{ids}/to/{to_album_id}', 'AlbumsWaitingController@waitingMove');


// videos
Route::get('albums/{id}/videos', 'AlbumsController@videosAddView');
Route::post('albums/{id}/videos', 'AlbumsController@videoAdd');
Route::get('video/upload', 'VideosController@uploadYoutube');


// album join
Route::get('albums/{id}/join/{token}', 'AlbumsJoinController@index');
Route::get('albums/{id}/join/{token}/sign', 'AlbumsJoinController@sign');


/* ***************************************
Pictures
**************************************** */
// vue principal
Route::get('albums/{id}/pictures/{id_picture}', [
    'as' => 'PictureInAlbum',
    'uses' => 'AlbumsPictures@index'
]);

// retirer une picture
Route::get('albums/{id}/pictures/{id_picture}/remove', 'AlbumsPictures@remove');

// afficher les picture non tagée
Route::get('albums/{id}/pictures/untaged/{id_picture}', [
    'as' => 'PictureUntagedInAlbum',
    'uses' => 'AlbumsPictures@indexUntaged'
]);

// tag personne sur picture
Route::post('albums/{id}/pictures/{id_picture}/tag', [
    'as' => 'tagOnPhoto',
    'uses' => 'AlbumsPictures@tagUser'
]);
// remove tag personne sur picture
Route::get('albums/{id}/pictures/{id_picture}/tagRemove/{id_user}', [
    'as' => 'tagOnPhotoRemove',
    'uses' => 'AlbumsPictures@tagUserRemove'
]);
// change le type d'une picture
Route::post('albums/{id}/pictures/{id_picture}/tagtype', [
    'as' => 'tagTypeOnPhoto',
    'uses' => 'AlbumsPictures@tagType'
]);
// set this picture as potentiel jaquette
Route::post('albums/{id}/pictures/{id_picture}/tagJaquetteOnPhoto', [
    'as' => 'tagJaquetteOnPhoto',
    'uses' => 'AlbumsPictures@tagJaquette'
]);
// pivoter picture
Route::get('albums/{id}/pictures/{id_picture}/pivoter/{direction}', [
    'as' => 'pivoter',
    'uses' => 'AlbumsPictures@pivoter'
]);

// download picture
Route::get('albums/{id}/pictures/{id_picture}/download/', [
    'as' => 'downloadPicture',
    'uses' => 'AlbumsPictures@download'
]);
Route::get('albums/{id}/download/', [
    'as' => 'downloadPictureZip',
    'uses' => 'AlbumsPictures@downloadZip'
]);
// seul les personnes taguées sont présentes
Route::get('albums/{id}/pictures/{id_picture}/{onlytagged}', [
    'as' => 'onlyTagged',
    'uses' => 'AlbumsPictures@onlyTagged'
]);



// diaporama des pictures
Route::get('albums/{id}/diaporama/', [
    'as' => 'diaporama',
    'uses' => 'AlbumsDiaporama@index'
]);


// déplacer picture
Route::get('albums/{id}/pictures/{id_picture}/move/{albums_id}/{keep_files}', [
    'as' => 'deplacer_picture',
    'uses' => 'AlbumsPictures@move'
]);


/* ***************************************
Videos
**************************************** */



/* ***************************************
Search
**************************************** */
Route::get('recherche/','RechercheController@index');

Route::post('search/', [
    'as' => 'search_ajax',
    'uses' => 'RechercheController@doSearch'
]);

Route::get('recherche/list_participants/{albums_ids}', [
    'as' => 'getListParticipantsForAlbum',
    'uses' => 'RechercheController@getListParticipantsForAlbum'
]);



/* ***************************************
Users
**************************************** */
// Authentication routes...
Route::get('user/login', [
    'uses' =>'Auth\AuthController@getLogin',
    'as' => 'login'
]);
Route::post('user/login', 'Auth\AuthController@postLogin');
Route::get('user/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('user/inscription', 'Auth\AuthController@getRegister');
Route::post('user/inscription', 'Auth\AuthController@postRegister');

// editprofil routes...
Route::get('user/edit', 'Auth\AuthController@edit');
Route::post('user/edit', 'Auth\AuthController@update');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

/* ***************************************
resizeImage script
**************************************** */

route::get('resizeImage/','UtilsController@createNewResizeImage');


/* ***************************************
amis
**************************************** */

Route::get('amis', 'AmisController@index');
Route::get('amis/delete/{id}', 'AmisController@remove');
Route::get('amis/add/{id}/fromalbum/{album_id}','AmisController@add');
Route::post('amis', 'AmisController@recherche');

/* ***************************************
notifications
**************************************** */

Route::get('notifications', 'NotificationsController@index');



