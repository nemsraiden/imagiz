<?php

namespace App\MyClass;

use App\Albums;
use App\AlbumsParticipants;
use App\Viewers;
use Illuminate\Support\Facades\Auth;


class AlbumsClass{

    function getAlbums($withimage = false){
        if(!Auth::check()) return [];
        
        $aps = AlbumsParticipants::where('users_id', '=', Auth::user()->id)->get();

        $ids =[];
        foreach($aps as $ap){
            $ids[] = $ap->albums_id;
        }

        $aviewers = Viewers::where('users_id', '=', Auth::user()->id)->get();
        foreach($aviewers as $av){
            $ids[] = $av->albums_id;
        }

        $albums = Albums::whereIn('id', $ids)->orWhere('ID_proprietaire',Auth::user()->id)->where('is_waiting_album','0')->orderBy('nom','ASC')->get();

        if($withimage){

            foreach ($albums as $album) {
                if(!$album->pictures->isEmpty()){

                    $def_image_choose = false;

                    foreach($album->pictures as $pic){
                        if($pic->is_jaquette){
                            $image = $pic;
                            $def_image_choose = true;
                        }
                    }


                    if(!$def_image_choose){

                        // si on a pas de jaquette de préférence, on choisis aléatoirement
                        $image = $album->pictures->shuffle()->first();
                    }


                    $album->image = $image->directory.'/thumb_'.$image->name;
                }
                else{
                    $album->image = 'img/nopicture.png';

                }
            }

        }
        return $albums;


    }

}

