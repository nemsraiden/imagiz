<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alertes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creator_users_id');
            $table->integer('receptor_users_id');
            $table->enum('type',['newalbumparticipant','newpicture','newalbumviewer','newtaged','deletealbum','removelabumparticipant','removealbumviewer','participantjoinalbum','viewerjoinalbum']);
            $table->boolean('is_read')->default(false);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alertes');
    }
}
