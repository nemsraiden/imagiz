<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTokensToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('albums', function (Blueprint $table) {
            $table->string('token_participants')->default('')->nullable()->after('ID_proprietaire');
            $table->string('token_viewers')->default('')->nullable()->after('ID_proprietaire');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('albums', function (Blueprint $table) {
            $table->removeColumn('token_participants');
            $table->removeColumn('token_viewers');
        });
    }
}
