<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypePrivilegeToAlbumsParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('albums_participants', function (Blueprint $table) {
            $table->enum('type_privilege',['Utilisateur normal','Administrateur'])->default('Utilisateur normal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('albums_participants', function (Blueprint $table) {
            $table->removeColumn('type_privilege');
        });
    }
}
