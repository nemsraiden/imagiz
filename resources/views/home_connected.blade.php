@extends('app')

@section('jumbotron')

    @include('jumbotron')

@endsection


@section('content')

    <div class="starter-template informations home_connected">

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p class="lead lead-green" style="text-align:center;">@lang('menu.mes_albums_photos')</p>
                    </div>
                </div>

                <div class="avantages">
                    <div class="av-img"> {!! HTML::image('img/album-photos.png', '', array( 'class' => 'img-responsive')) !!} </div>
                    <br>
                    <a href="albums/" class="btn btn-green"> @lang('albums.afficher_tous')</a>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p class="lead lead-green" style="text-align:center;">@lang('menu.recherche_photo')</p>
                    </div>
                </div>

                <div class="avantages">
                    <div class="av-img"> {!! HTML::image('img/1302.png', '', array( 'class' => 'img-responsive')) !!} </div>
                    <br>
                    <a href="recherche/" class="btn btn-green"> @lang('recherche.recherche_photos')</a>

                </div>
            </div>

        </div>
    </div>


@endsection

