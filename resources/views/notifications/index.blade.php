@extends('app')

@section('page-class')register @endsection

@section('content')


    <div class="gestion">
        <h1>@lang('notifications.mes_notifications')</h1>
    </div>

    @foreach($notifications as $notification)

        <div class="notification @if($notification->is_read == 0) new @endif">

            @if($notification->type == 'newalbumparticipant')

                <h4>@lang('notifications.new_album')</h4>

                <div class="cadre">

                    <p>@lang('notifications.txt_1', ['album_nom' => $notification->albums->nom, 'first_name' => $notification->users->first_name, 'last_name' => $notification->users->last_name])  <br>
                        @lang('notifications.txt_2')</p>
                    <br>
                    <a href="/albums/{{$notification->albums_id}}" class="btn btn-default">@lang('notifications.txt_3')</a>

                </div>

            @elseif($notification->type == 'newpicture')

                <h4>@lang('notifications.new_photos')</h4>

                <div class="cadre">

                    <p>@lang('notifications.txt_5', ['first_name' => $notification->users->first_name, 'last_name' => $notification->users->last_name, 'count' => $notification->count, 'album_nom' => $notification->albums->nom ] )</p>
                    <br>
                    <a href="/albums/{{$notification->albums_id}}" class="btn btn-default">@lang('notifications.txt_4')</a>

                </div>

            @elseif($notification->type == 'newalbumviewer')

                <h4>@lang('notifications.new_album')</h4>

                <div class="cadre">

                    <p>@lang('notifications.txt_6', ['album_nom' => $notification->albums->nom, 'first_name' => $notification->users->first_name, 'last_name' => $notification->users->last_name]) <br>
                        @lang('notifications.txt_7')
                        <br>@lang('notifications.txt_8')</p>
                    <br>
                    <a href="/albums/{{$notification->albums_id}}" class="btn btn-default">@lang('notifications.txt_3')</a>


                </div>


            @elseif($notification->type == 'newtaged')

                <h4>@lang('notifications.tagged') !</h4>

                <div class="cadre">

                    <p> @lang('notifications.txt_9', ['first_name' => $notification->users->first_name, 'last_name' => $notification->users->last_name, 'album_nom' => $notification->albums->nom ] )<br>
                        @lang('notifications.txt_10')</p>
                    <br>
                    <a href="/albums/{{$notification->albums_id}}" class="btn btn-default">@lang('notifications.txt_4')</a>
                    <a href="/albums/{{$notification->albums_id}}/pictures/{{$notification->pictures_id}}" class="btn btn-default">@lang('notifications.txt_11')</a>


                </div>

            @elseif($notification->type == 'deletealbum')

                <h4>@lang('notifications.album_delete')</h4>

                <div class="cadre">

                    <p>@lang('notifications.txt_12', ['first_name' => $notification->users->first_name, 'last_name' => $notification->users->last_name, 'album_nom' => $notification->albums->nom ] ) <br>
                        @lang('notifications.txt_13')  </p>


                </div>

            @elseif($notification->type == 'removealbumparticipant')

                <h4>@lang('notifications.album_remove')</h4>

                <div class="cadre">

                    <p>@lang('notifications.txt_15', ['first_name' => $notification->users->first_name, 'last_name' => $notification->users->last_name, 'album_nom' => $notification->albums->nom ] )
                        @lang('notifications.txt_13') </p>

                </div>

            @elseif($notification->type == 'removealbumviewer')

                <h4>@lang('notifications.album_remove')</h4>

                <div class="cadre">

                    <p>@lang('notifications.txt_16', ['first_name' => $notification->users->first_name, 'last_name' => $notification->users->last_name, 'album_nom' => $notification->albums->nom ] )
                        @lang('notifications.txt_13') </p>

                </div>

            @elseif($notification->type == 'participantjoinalbum')

                <h4>@lang('notifications.new_participant')</h4>

                <div class="cadre">

                    <p>@lang('notifications.txt_17', ['first_name' => $notification->users->first_name, 'last_name' => $notification->users->last_name, 'album_nom' => $notification->albums->nom ] )
                        <br>   @lang('notifications.txt_14') </p>

                </div>

            @elseif($notification->type == 'viewerjoinalbum')

                <h4>@lang('notifications.new_viewers')</h4>

                <div class="cadre">

                    <p>@lang('notifications.txt_18', ['first_name' => $notification->users->first_name, 'last_name' => $notification->users->last_name, 'album_nom' => $notification->albums->nom ] )
                        <br>  @lang('notifications.txt_14') </p>

                </div>

            @endif

            <div class="time">
                {{ Carbon\Carbon::parse($notification->updated_at)->format('d/m/Y à H:i:s') }}
            </div>

        </div>

    @endforeach


@endsection
