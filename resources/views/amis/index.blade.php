@extends('app')

@section('page-class')register @endsection

@section('content')

    <div class="gestion">
        <h1>@lang('amis.amis_list')</h1>
    </div>


    @if(Session::has('error'))

        <div class="alert alert-danger">{!! Session::get('error') !!}</div>

    @endif

    @if(Session::has('success'))

        <div class="alert alert-success">{!! Session::get('success') !!}</div>

    @endif


    <div class="ajout-participant">

        <h4>@lang('amis.add_amis')</h4>

        <p>@lang('amis.txt_1')</p>

        <div class="form-search">
            {!! BootForm::open(['autocomplete' => 'off'])->post() !!}

            <div class="row">
                <div class="col-md-6">
                    {!! BootForm::email(trans('amis.txt_2')." :", 'email')->placeholder(trans('amis.txt_3')) !!}
                    {!! BootForm::submit(trans('amis.txt_4')) !!}
                </div>
                <div class="col-md-6">
                    {!! BootForm::text(trans('amis.txt_5')." :", 'q')->placeholder(trans('amis.txt_6')) !!}
                    <div class="liste-participants-resultats"></div>
                    {!! BootForm::submit(trans('amis.txt_7')) !!}
                    <input type="hidden" id="q_id" name="q_id"/>
                </div>
            </div>

            {!! BootForm::close() !!}
        </div>



    </div>

    <h2>Liste</h2>

    <table class="table table-condensed table-bordered table-hover amis table-striped">
        <thead class="thead-dark">
            <tr>
                <th scope="col" >@lang('amis.nom')</th>
                <th scope="col" >@lang('amis.prenom')</th>
                <th scope="col" class="hideonphone" >@lang('amis.email')</th>
                <th scope="col" ></th>
            </tr>
        </thead>
        <tbody>
            @foreach($amis as $ami)
                <tr>
                    <td> {{ $ami->users->last_name }}</td>
                    <td> {{ $ami->users->first_name }}</td>
                    <td class="hideonphone"> {{ $ami->users->email }}</td>
                    <td style="text-align: right">
                        <a class="btn btn-primary" href="/amis/delete/{{$ami->users->id}}"> <i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hideonphone">@lang('amis.supprimer')</span></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>



@endsection
