@extends('app')

@section('jumbotron')

    @include('jumbotron')

@endsection


@section('content')

    <div class="home_desc">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>Imagiz.be</h1>
                <ul >
                    <li>@lang('home.jumbotron_1')</li>
                    <li>@lang('home.jumbotron_2')</li>
                    <li>@lang('home.jumbotron_3')</li>
                </ul>
            </div>

            <div class="col-md-12 text-center">
                <a href="{{url('/albums')}}" class="create-album">>>> Créer son album photo <<<</a>
            </div>
        </div>
    </div>

    <div class="starter-template informations">
        <h1 class="lead-green" style="text-align:center;">@lang('informations.txt_1') </h1>


        <div class="row presentation">
            <div class="col-md-4">
                <div class="step">
                    <p class="lead">Etape 1 :<br/> @lang('informations.txt_2')</p>
                    <div class="image">
                        {!! HTML::image('img/album_vacances.jpg', '', array('width' => '300')) !!}
                    </div>
                    <p>@lang('informations.txt_3')</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="step">
                    <p class="lead">Etape 2 :<br/> @lang('informations.txt_4')</p>
                    <div class="image">
                        {!! HTML::image('img/albumv.1_1.png', '', array( 'width' => '300')) !!}
                    </div>
                    <p>@lang('informations.txt_5')</p>
                </div>

            </div>
            <div class="col-md-4">
                <div class="step">
                    <p class="lead">Etape 3 :<br/> @lang('informations.txt_6')</p>
                    <div class="image">
                        {!! HTML::image('img/baby-album5.jpg', '', array( 'width' => '300')) !!}
                    </div>
                    <p>@lang('informations.txt_7')</p>

                </div>
            </div>

        </div>

        <br> <br>

        <div class="row">
            <div class="col-md-12">
                <p class="lead lead-green" style="text-align:center;">@lang('informations.txt_8')</p>
            </div>
        </div>

        <div class="avantages">
            <div class="row">
                <div class="col-md-4">
                    {!! HTML::image('img/1302.png', '', array( 'class' => 'img-responsive')) !!}

                </div>

                <div class="col-md-6">
                    <h3>@lang('informations.txt_9')</h3>

                    <p><strong>@lang('informations.txt_10')</strong></p>

                    <ul>
                        <li>@lang('informations.txt_11')</li>
                        <li>@lang('informations.txt_12')</li>
                        <li>@lang('informations.txt_13')</li>
                        <li>...</li>
                    </ul>
                </div>
                <div class="col-md-2"></div>
            </div>

        </div>

    </div>





@endsection

