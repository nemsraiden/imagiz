
@extends('index')



@section('app')

    @yield('jumbotron')

<div class="container @yield('page-class','')" id="main">

    <div class="row">
        <div class="col-md-8">
            @yield('content')
        </div>
        <div class="col-md-4">
            <div class="sidebar">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('home.espace_membres')</div>
                    <div class="panel-body">
                        @include('auth.login_form')
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

