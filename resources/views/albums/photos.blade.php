@extends('app')

@section('content')

    <div class="gestion">

        @if(!$album->is_waiting_album)
            <h1>@lang('albums.txt_10',['album_nom' => $album->nom])</h1>
        @else
            <h1>@lang('albums.dossier_temporaire')</h1>
        @endif

    </div>


    <div class="row">
        <div class="col-md-12">

            @if(Session::has('error'))

                <div class="alert alert-danger">{!! Session::get('error') !!}</div>

            @endif

            @if(Session::has('success'))

                <div class="alert alert-success">{!! Session::get('success') !!}</div>

            @endif

            <h2 class="bordered">@lang('albums.photo_add')</h2>

            <p>@lang('albums.txt_11')</p>

            <div class="alert alert-success2" id="upload_all_success" style="display: none">
                @lang('albums.txt_14')
            </div>
            <div class="alert alert-danger" id="upload_all_error" style="display: none">
                @lang('albums.txt_14_')
            </div>

            <div class="file-uploadd">
                {!! Form::open(array('files'=>true)) !!}

                <input type="file" id="images_file" name="file[]" multiple="multiple" class="file-loading" accept="image/*" data-browse-on-zone-click="true"  />
                <input type="hidden" id="album_id" value="{{$album->id}}"  />

                {!! Form::close() !!}
            </div>

                <br>

            <div class="hideonphone">
                <h2 class="bordered">@lang('albums.txt_15')</h2>

                <div id="zipUploadError" class="alert alert-danger" style="display: none"></div>
                <div id="zipUploadSuccess" class="alert alert-success" style="display: none">@lang('albums.txt_16')</div>

                <p>@lang('albums.txt_17')</p>
                <p>@lang('albums.txt_18')</p>

                {!! Form::open() !!}

                <input id="zip_file" name="zip_file" multiple type="file" required>

                {!! Form::close() !!}

            </div>


        </div>
    </div>






@endsection