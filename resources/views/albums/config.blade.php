@extends('app')

@section('page-class')albumshow @endsection

@section('content')

    <div class="gestion">

        <h1>{{$album->nom}}</h1>

    </div>

    <div class="row">
        <div class="col-md-4">

            @include('albums.right-menu-about-the-album')

        </div>

    </div>


@endsection