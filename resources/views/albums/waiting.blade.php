@extends('app')

@section('page-class')diaporama @endsection

@section('include_js')
    <script>

        $(document).ready(function(){

            loadNano([]);

            function loadNano(pictures,selectMode){

                if(typeof selectMode == "undefined") selectMode = false;

                $("#nanoGallery1a").nanoGallery({
                    thumbnailWidth: 'auto',
                    thumbnailHeight: 200,

                    colorScheme: 'none',
                    thumbnailHoverEffect: [{ name: 'labelAppear75', duration: 300 }],
                    theme: 'light',
                    thumbnailGutterWidth : 0,
                    thumbnailGutterHeight : 0,
                    itemsSelectable : true,
                    i18n: { thumbnailImageDescription: 'Afficher la photo', thumbnailAlbumDescription: 'Open Album' },
                    thumbnailLabel: { display: true, position: 'overImageOnMiddle', align: 'center' },

                    //items:pictures,
                    //itemsBaseURL: '../../',
                    //dataSorting: 'random',
                    kind: 'json',
                    jsonProvider: '/albums-waiting/picturesJson',

                    fnThumbnailHover: function ($elt, item) {
                        if(!$("#select_waiting_photos").hasClass('active')){
                            $elt.children('.ngChekbox').hide();
                        }
                        else{
                            $elt.children('.ngChekbox').show();
                            $('.ngChekbox').closest('.nanoGalleryThumbnailContainer').click(function(){
                                showPhotosAction();
                            });
                        }
                    },
                    fnInitGallery : function(){
                        $("#nanoGallery1a").nanoGallery('setSelectMode', false);
                    }

                });


                $(".waiting-loader").hide();
                $("#nanoGallery1a").fadeIn();
            }

            function showPhotosAction(){
                var nb = $('.ngChekbox:checked').length;

                if(nb > 0){

                    $(".gerer-photos-waiting span").text(nb);

                    $(".gerer-photos-waiting .txt1").hide();
                    $(".gerer-photos-waiting .txt2").show();

                }
                else{
                    $(".gerer-photos-waiting .txt1").show();
                    $(".gerer-photos-waiting .txt2").hide();
                }
            }


            $(window).load(function(){
                $(".waiting-loader").hide();
                $("#nanoGallery1a").fadeIn();

            });

            $("#select_waiting_photos").click(function(){

                if(!$(this).hasClass('active')) {
                    $(".options-waiting-photos").show();
                    $("#selectAllDiv").show();
                    $(this).addClass('active');
                    $('#confirm_group').hide();
                    $('#confirm_group .spinner').hide();
                    $('#move_photos_to_album_id').hide();
                    $("#nanoGallery1a").nanoGallery('setSelectMode', true);
                    $(".ngChekbox").show();
                    $(".nanoGalleryContainer .nanoGalleryThumbnailContainer input").css('opacity', '1');
                    $(".labelImage.nGEvent").hide();
                    showPhotosAction();
                    $("#photos_todo").val('');
                    $(".nanoGalleryThumbnailContainer").click(function(e) {
                        // hack rajouter à cause d'un soucis sur mobile
                        var that = $(this);
                        setTimeout(function() {
                            e.preventDefault();
                            e.stopPropagation();
                            if(!that.children('.subcontainer.nGEvent').hasClass('selected2')) {
                                that.children('.ngChekbox').prop('checked', true);
                                that.children('.subcontainer.nGEvent').addClass('selected');
                                that.children('.subcontainer.nGEvent').addClass('selected2');
                                showPhotosAction();
                            } else {
                                that.children('.ngChekbox').prop('checked', false);
                                that.children('.subcontainer.nGEvent').removeClass('selected');
                                that.children('.subcontainer.nGEvent').removeClass('selected2');
                                showPhotosAction();
                            }
                        }, 0);
                    });

                } else {
                    $(".options-waiting-photos").hide();
                    $("#selectAllDiv").hide();
                    $(this).removeClass('active');

                    showPhotosAction();
                    $(".ngChekbox").hide();
                    $(".ngChekbox").attr('checked',false);
                    $("#nanoGallery1a").nanoGallery('setSelectMode', false);
                    $(".nanoGalleryContainer .nanoGalleryThumbnailContainer input").css('opacity', '0');
                    $(".labelImage.nGEvent").show();

                }



            });

            $("#move_photos_choose_album").click(function(){
                $(".gerer-photos-waiting .txt2").hide();
                $(".gerer-photos-waiting .txt3").show();
            });

            var confirm_action = '';

            $("#photos_todo").change(function() {
                if($(this).val() === 'delete') {
                    $('#confirm_group').show();
                    $('#move_photos_to_album_id').hide();
                    confirm_action = 'delete';
                } else if($(this).val() === 'move') {
                    $('#confirm_group').hide();
                    $('#move_photos_to_album_id').show();
                    confirm_action = 'move';
                }

            });

            $("#move_photos_to_album_id").change(function() {
                $('#confirm_group').show();
            });

            $("#confirm_action").click(function() {
                var items = $('#nanoGallery1a').nanoGallery('getSelectedItems');
                $("#confirm_group .spinner").show();
                $("#confirm_action").attr('disabled', true);
                var l = items.length;
                var id, elt, ids='';
                for (var j = 0; j < l; j++) {
                    elt = items[j].$elt;
                    id = parseInt(items[j].GetID());
                    if(ids != '') ids += ',';
                    ids += id;
                }

                if(confirm_action === 'move') {
                    var href = $("#get_js_url").val()+'/albums-waiting/move/'+ids+'/to/'+$("#move_photos_to_album_id").val();
                    $.get(
                        href,
                        {
                        },
                        function( data ) {
                            if(!data.success){
                                //alert('Il y a eu une erreur.');
                            }
                            else{


                                $(".gerer-photos-waiting span").text(0);
                                $(".gerer-photos-waiting .txt1").show();
                                $(".gerer-photos-waiting .txt2").hide();

                                $("#nanoGallery1a").nanoGallery('reload');
                                $("#select_waiting_photos").trigger('click');
                                $("#select_all").prop('checked', false);
                                $("#confirm_group .spinner").hide();
                                $("#confirm_action").attr('disabled', false);

                            }
                        },
                        'json'
                    );
                } else if(confirm_action === 'delete') {



                    var href = $("#get_js_url").val()+'/albums-waiting/delete/'+ids;
                    $.get(
                        href,
                        {
                        },
                        function( data ) {
                            if(!data.success){
                                //alert('Il y a eu une erreur.');
                            }
                            else{

                                $(".gerer-photos-waiting span").text(0);
                                $(".gerer-photos-waiting .txt1").show();
                                $(".gerer-photos-waiting .txt2").hide();

                                $("#nanoGallery1a").nanoGallery('reload');
                                $("#select_waiting_photos").trigger('click');
                                $("#select_all").prop('checked', false);
                                $("#confirm_group .spinner").hide();
                                $("#confirm_action").attr('disabled', false);

                            }
                        },
                        'json'
                    );
                }
            });


            $("#select_all").click(function() {

                if(document.getElementById("select_all").checked){
                    var items = $('#nanoGallery1a').nanoGallery('getItems');
                    $('#nanoGallery1a').nanoGallery('selectItems', items);
                    showPhotosAction();
                } else {
                    var items = $('#nanoGallery1a').nanoGallery('getItems');
                    $('#nanoGallery1a').nanoGallery('unselectItems', items);
                    showPhotosAction();
                }
            })

        });

        $(document).click(function() {
            $('.actions ul li ul').slideUp();
        });

    </script>
@endsection


@section('content')

    <div class="gestion">

        <h1>@lang('albums.dossier_temporaire')</h1>

        <div class="actions">
            <ul>
                <li>
                    <a href="@if($waiting_album_already_created) /albums/{{$album->id}}/photos @else /albums-waiting/create @endif" class="btn btn-default">
                        <i class="fa fa-plus-circle"></i> <span>@lang('albums.photo_add')</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="btn btn-default submenu">
                        <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="#" id="select_waiting_photos">
                                <i class="fa fa-list" aria-hidden="true"></i>Gérer les photos
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>

        </div>
    </div>

    @if(!$waiting_album_already_created)
        @lang('albums.txt_30')
    @endif

    @if(count($pictures) > 0)
    <div class="row">
        <div class="col-md-12 options-waiting-photos" style="display: none">

            <div class="gerer-photos-waiting">
                <div class="txt1">
                    @lang('albums.txt_33')
                </div>
                <div class="txt2" style="display: none">

                    <strong><span>x</span></strong> Photo(s) sélectionée(s). <br>
                    <div class="checkbox">
                        <input type="checkbox" id="select_all">
                        <label for="select_all" style="font-size: 16px; display: flex;  align-items: center;">
                            Sélectionner tout
                        </label>
                    </div>
                    <select class="form-control" name="photos_todo" id="photos_todo" style="margin-top: 10px">
                        <option value="" disabled selected>Choississez une action...</option>
                        <option value="move">Déplacer</option>
                        <option value="delete">Supprimer</option>
                    </select>

                    <select class="form-control" name="move_photos_to_album_id" id="move_photos_to_album_id" style="display: none; margin-top: 12px;">
                        <option value="" disabled selected>Choississez un album...</option>
                        @foreach($others_albums as $other_album)
                            <option value="{{ $other_album->id }}">{{ $other_album->nom }}</option>
                        @endforeach
                    </select>

                    <div class="confirm-group" id="confirm_group" style="display: none">
                        <button class="btn btn-default" id="confirm_action">Confirmer</button>
                        <div class="spinner"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Traitement en cours...</div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <div id="nanoGallery1a" style="display: none; ">

    </div>
    <div class="waiting-loader" style="position: fixed">
        <span class="waiting-loader-children"></span>
        {!! HTML::image('img/cube.gif','Loading...' ) !!}
    </div>
    @else
        @lang('albums.txt_31')
    @endif


@endsection