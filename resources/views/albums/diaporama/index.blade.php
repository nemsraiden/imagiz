@extends('app')

@section('page-class')diaporama @endsection

@section('include_js')
    <script>
        $(document).ready(function(){
            $("#nanoGallery1a").nanoGallery({
                thumbnailWidth: 'auto',
                thumbnailHeight: 200,

                colorScheme: 'none',
                thumbnailHoverEffect: [{ name: 'labelAppear75', duration: 300 }],
                theme: 'light',
                thumbnailGutterWidth : 0,
                thumbnailGutterHeight : 0,
                i18n: { thumbnailImageDescription: 'Afficher la photo', thumbnailAlbumDescription: 'Open Album' },
                thumbnailLabel: { display: true, position: 'overImageOnMiddle', align: 'center' },
                items:[
                    @foreach($pictures as $picture)

                        {
                            src: '{{$picture['ultra']}}',
                            srct: '{{$picture['middle']}}',	// thumbnail url
                            title: '{{ $album->nom }}'       // thumbnail title
                        },

                    @endforeach
                ],
                itemsBaseURL: '../../',
                dataSorting: 'random',
            });

            $(window).load(function(){
                $(".waiting-loader").fadeOut();
            });
        })

    </script>
@endsection

@section('content')

    <div class="gestion">

        <h1>@lang('albums_diaporama.txt_1',['album_nom' => $album->nom])</h1>

    </div>

    <div style="position:relative;">
        <div class="waiting-loader" style="position: fixed; height: 180px" >
            <span class="waiting-loader-children"></span>
            {!! HTML::image('img/cube.gif','Loading...' ) !!}
        </div>


        <div id="nanoGallery1a" style="min-height: 200px">

            @foreach($pictures as $picture)

                <a href="{{$picture['big']}}" data-ngthumb="{{$picture['middle']}}" data-ngdesc=""  >
                <!--{{ $album->nom }}-->
                </a>

            @endforeach

        </div>


        <hr/>
    </div>

    <a href="/albums/{{$album->id}}" class="btn btn-default"><span class="fa fa-arrow-left "></span> @lang('albums_diaporama.back_to_album')</a>



@endsection



