@extends('app')

@section('content')

    <h1>Albums photos << {{ $album->nom }} >></h1>

    <div class="row">
        <div class="col-md-12">



            <h2 class="bordered">Ajouter des vidéos</h2>

            <p>Merci de choisir la vidéo que vous désirez ajouter</p>

            <div class="alert alert-success2" id="upload_all_success" style="display: none">La vidéo a été uploadée et ajoutée a votre album</div>


            {!! Form::open(array('files'=>true)) !!}

            <input type="file" id="videos_file" name="file"  />
            <input type="hidden" id="album_id" value="{{$album->id}}"  />

            <br>

            <input type="submit" class="btn btn-primary" value="Uploader la vidéo">

            {!! Form::close() !!}
            <hr/>

            <a href="/albums/{{$album->id}}/" class="btn btn-default"><span class="fa fa-arrow-left "></span> Revenir à l'album photo</a>


        </div>
    </div>






@endsection