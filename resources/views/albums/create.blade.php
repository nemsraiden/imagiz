
@extends('nosidebar')

@section('include_js')
    <script language="JavaScript">

        $("#annee").change(function(){
           var montth = $("#annee").val();
            if(montth == 0){
                $("#datemonth").hide();
            }
            else{
                $("#datemonth").slideDown();
            }
        });
    </script>
@endsection

@section('content')

    <div class="gestion">

        <h1>@lang('albums.album_create')</h1>
    </div>


    {!! BootForm::open()->post()->action('/albums')->id('createAlbum') !!}
    {!! BootForm::text(trans('albums.album_nom'), 'nom') !!}
    {!! BootForm::textarea(trans('albums.description'), 'description') !!}
    {!! BootForm::select(trans('albums.annee'), 'annee')->options($listYear) !!}
    <div id="datemonth" style="display: none">
        {!! BootForm::select(trans('albums.mois'), 'mois')->options($listMonth) !!}
    </div>

    {!! BootForm::submit(trans('albums.enregistrer')) !!}
    {!! BootForm::close() !!}






@endsection