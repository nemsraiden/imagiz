@lang('albums.connected_as',['first_name' => Auth::user()->first_name]) <br/>
<br/>
<h4>>> @lang('albums.albums_photos')</h4>
<ul>
    @foreach($albums as $album)
        <li><a href="/albums/{{$album->id}}">{{$album->nom}}</a></li>
    @endforeach

</ul>
<a href="/albums/create" class="btn btn-default">@lang('albums.album_create')</a>

<h4>>> @lang('albums.mon_profil')</h4>
<ul>
    <li><a href="/user/edit">@lang('albums.mon_profil')</a></li>
</ul>
<a class="btn btn-default" href="/user/logout">@lang('albums.se_deconnecter')</a>