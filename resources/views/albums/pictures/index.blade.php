@extends('app')

@section('page-class')pictures-index @endsection

@section('include_js')
    <script language="JavaScript">

           var height = $('.cadre-photo').height() +60;
            $('#picture_left_').height(height);
            $(".pivoter").show();

        $("#move_and_delete").change(function(){
           if($(this).is(':checked')){
               $("#action_move_images").html('Déplacer cette image');
           }
            else{
               $("#action_move_images").html('Copier cette image');
           }
        });

       $("body").keydown(function(e) {
           if(e.which == 37) { // left
               if($(".cadre-photo .left a").length > 0) {
                   document.location.href = $(".cadre-photo .left a").attr('href');
               }

           }
           else if(e.which == 39) { // right
               if($(".cadre-photo .right a").length > 0) {
                   document.location.href = $(".cadre-photo .right a").attr('href');
               }

           }
       });

    </script>
@endsection

@section('content')

    <div class="gestion">

        <h1>Photo #{{$picture->id}}</h1>

        <div class="actions">
            <ul>
                <li>
                    <a href="/albums/{{$album->id}}/photos" class="btn btn-default">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i> <span>@lang('albums.photo_add')</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="btn btn-default submenu">
                        <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                    </a>
                    <ul>
                        @if($album->ID_proprietaire == Auth::id())
                            <li id="deplacer_image"><a href="#"><i class="fa fa-folder" aria-hidden="true"></i>@lang('albums_pictures.move_image')</a></li>
                        @endif
                        <li>
                            <a href="{{route('downloadPicture',[$album->id,$picture->id])}}">
                                <i class="fa fa-download" aria-hidden="true"></i>@lang('albums_pictures.telecharger_image')
                            </a>
                        </li>
                        <li>
                            <a href="{{route('downloadPictureZip', $album->id)}}">
                                <i class="fa fa-file-archive-o" aria-hidden="true"></i>@lang('albums_pictures.telecharger_zip')
                            </a>
                        </li>
                        @if($album->ID_proprietaire == Auth::id() or $picture->iamownerandadmin)
                            <li>
                                <a href="{{route('pivoter',[$album->id,$picture->id,'gauche'])}}"><i class="fa fa-repeat" aria-hidden="true"></i> @lang('albums_pictures.pivoter_gauche')</a>
                            </li>
                            <li>
                                <a href="{{route('pivoter',[$album->id,$picture->id,'droite'])}}" ><i class="fa fa-undo" aria-hidden="true"></i> @lang('albums_pictures.pivoter_droite') </a>
                            </li>
                            <li>
                                <a href="#" id="supprimer_image"><i class="fa fa-trash-o" aria-hidden="true"></i>@lang('albums_pictures.delete_image')</a>
                            </li>

                        @endif
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    @if(Session::has('success'))

        <div class="alert alert-success">{!! Session::get('success') !!}</div>

    @endif
    <br/>
    <div class="row pictures left">
        <div class="col-md-8">
            <div class="cadre-photo">
                <div class="left">
                    @if($picture->previous)
                        <a  href="{{route($routePicture,[$album->id,$picture->previous])}}"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a>
                    @endif
                </div>
                <div class="center">
                    {!! HTML::image($picture->image, '') !!}
                </div>
                <div class="right">
                    @if($picture->next)
                     <a href="{{route($routePicture,[$album->id,$picture->next])}}"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-4 left-border">

            <h2 class="bordered first">@lang('albums_pictures.informations')</h2>

            <div id="untaged">
                @if($picture->type == null  and ($album->ID_proprietaire == Auth::id() or (in_array(Auth::id(),$album->adminList) && $picture->users_id == Auth::id())))

                    <div class="alert alert-warning">@lang('albums_pictures.txt_1')</div>

                @endif

                @if(($picture->type === 'personnes' and count($tagsList) === 0) and ($album->ID_proprietaire == Auth::id() or (in_array(Auth::id(),$album->adminList) && $picture->users_id == Auth::id())))

                    <div class="alert alert-warning">@lang('albums_pictures.txt_1_b')</div>

                @endif
            </div>

            @if(isset($picture->users))
                <strong>@lang('albums_pictures.add_by') :</strong> {{ $picture->users->first_name }} {{ $picture->users->last_name }} <br/> <br>
            @endif

            @if($album->ID_proprietaire == Auth::id() or $picture->iamownerandadmin)

                {!! BootForm::open()->post()->action(URL::route('tagJaquetteOnPhoto',[$album->id,$picture->id]))->id('picture_checkbox_jaquette') !!}

                @if($album->ID_proprietaire == Auth::id())
                    <div class="checkbox" style="margin: 0">
                        <input type="checkbox" id="picture_jaquette" @if($picture->is_jaquette) checked @endif>
                        <label for="picture_jaquette">
                            @lang('albums_pictures.is_jaquette')
                        </label>
                    </div>
                    <div id="picture_change_data_loading">
                        <div>
                            <i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Sauvegarde...
                        </div>
                    </div>
                    <br>
                @endif

                {!! BootForm::close() !!}

                <input type="hidden" id="picture_id" value="{{ $picture->id }}">
                <input type="hidden" id="album_id" value="{{ $album->id }}">

                {!! BootForm::open()->post()->action(URL::route('tagTypeOnPhoto',[$album->id,$picture->id]))->id('picture_change_type') !!}
                    {!! BootForm::select('Type de photo', 'picture_type_photo')->options(['inconnu' => 'Choisissez un type...', 'paysage' => 'Paysage', 'personnes' => 'Personnes', 'personnes_non_participants' => 'Personnes (non participants)'])->select($picture->type) !!}
                {!! BootForm::close() !!}

                <div id="picture_tag_personnes" @if($picture->type != 'personnes') style="display: none" @endif>

                    @if(count($selectInfos) > 0)
                        {!! BootForm::open()->post()->action(URL::route('tagOnPhoto',[$album->id,$picture->id]))->id('tag_personne') !!}
                        <div class="row">
                            <div class="col-md-8">
                                {!! BootForm::select('Ajouter une personne', 'user_id')->options($selectInfos) !!}
                            </div>
                            <div class="col-md-4">
                                {!! BootForm::submit('Ajouter') !!}
                            </div>
                        </div>
                        {!! BootForm::close() !!}
                    @endif

                    <strong>@lang('albums_pictures.txt_2')</strong>


                    <ul>
                        @foreach($tagsList as $tag)
                            <li>{{$tag['nom']}}
                                <a title="Retirer cette personne de la photo" href="{{  route('tagOnPhotoRemove',[$album->id,$picture->id,$tag['id']]) }}" class="delete untag-user">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </a>
                            </li>
                        @endforeach
                    </ul>

                    <div class="checkbox" id="only_tagged_onphoto" @if(empty($tagsList)) style="display: none" @endif >
                        <input type="checkbox" id="only_tagged_people" @if($picture->onlytagged) checked @endif>
                        <label for="only_tagged_people" style="font-weight:normal; font-size:12px;">
                            @lang('albums_pictures.txt_3')
                            <i title="@lang('albums_pictures.txt_4')"
                               class="fa fa-question-circle" style="font-size:16px;" aria-hidden="true"></i>
                        </label>
                        <span  style="display: none">{{ URL::route('onlyTagged',[$album->id,$picture->id,'onlytagged']) }}</span>
                    </div>
                </div>



            @else

                    <strong>Type de photo :</strong> @if($picture->type != null) {{ $picture->type }} @endif <br/>
                    <br>
                    @if($picture->type == 'personnes')
                        <strong>@lang('albums_pictures.txt_2')</strong>
                    @endif
                    <ul>
                        @foreach($tagsList as $tag)
                            <li>{{$tag['nom']}}</li>
                        @endforeach
                    </ul>

            @endif
        </div>

    </div>


    <div class="move-image">
        <div class="cadre">

            <div class="message">
                <h4>@lang('albums_pictures.deplacement_image')</h4>
                {!! BootForm::open()->post()->action(URL::route('deplacer_picture',[$album->id,$picture->id,'albums_id','keep_files']))->class('search-form')->id('search-form') !!}

                {!! BootForm::select('Dans quel albums vous désirez chercher ? (choix multiples)', 'albums_selection')->options($selectListAlbums)->multiple()->class('selectpicker')->attribute('data-actions-box',true) !!}
                <div class="alert alert-danger" id="move_images_error" style="display: none">@lang('albums_pictures.txt_8')</div>

                <div class="checkbox checkbox-primary checkbox-circle" style="margin-top: 0px">
                    <div style="display: inline-block">
                        <input type="checkbox" id="move_and_delete" name="move_and_delete" checked>
                        <label id="all_together" for="move_and_delete">@lang('albums_pictures.txt_9')</label>
                    </div>
                </div>

                <br><br>
                <button class="btn btn-primary" id="action_move_images">@lang('albums_pictures.deplacer_image')</button>
                <br> <br>
                <button class="btn btn-default" id="action_annuler">@lang('albums_pictures.annuller')</button>




                {!! BootForm::close() !!}
            </div>

            <div class="load-deplacement-img" style="display: none">
                {!! HTML::image('img/cube.gif', '') !!}
            </div>

            <div class="deplacement-ok" style="display: none">

                <h4>@lang('albums_pictures.image_copie')</h4>

                @lang('albums_pictures.txt_10') <br>
                <br>

                <button class="btn btn-primary" onclick="$('.move-image').fadeOut()">@lang('albums_pictures.fermer')</button>

            </div>

        </div>
    </div>


@endsection