@extends('app')

@section('page-class')album-participant-index @endsection

@section('include_js')
    <script language="JavaScript">

        $("#remove_token_invitaion").click(function(){

            $.get(
                    '/albums/'+$("#album_id").val()+'/participants/inviter/tokenRemove',
                    {
                    },
                    function( data ) {
                        if(!data.success){
                            //alert('Il y a eu une erreur.');
                        }
                        else{
                            $("#invitation_url").html('');
                            $("#invitation_url").hide();
                            $("#remove_token_invitaion").hide();
                            $("#generate_token_invitaion").show();
                        }
                    },
                    'json'
            );

        });


        $("#generate_token_invitaion").click(function(){

            $.get(
                    '/albums/'+$("#album_id").val()+'/participants/inviter/tokenGenerate',
                    {
                    },
                    function( data ) {
                        if(!data.success){
                            //alert('Il y a eu une erreur.');
                        }
                        else{
                            $("#invitation_url").html($("#invitation_url_data").val()+data.token+'<br/>');
                            $("#invitation_url").show();
                            $("#generate_token_invitaion").hide();
                            $("#remove_token_invitaion").show();

                        }
                    },
                    'json'
            );

        });
    </script>
@endsection


@section('content')


    <div class="gestion">

        <h1>{{$album->nom}} : Participants (inviter)</h1>

    </div>
    <div class="row">
        <div class="col-md-12">

            <input type="hidden" id="album_id" value="{{$album->id}}">
            <input type="hidden" id="invitation_url_data" value="<?php echo 'http://'.$_SERVER['HTTP_HOST'].'/albums/'.$album->id.'/join/'; ?>">

            @lang('albums_participants.txt_13')

            <strong>@lang('albums_participants.txt_14') :</strong> <br>
            <div id="invitation_url" @if($album->token_participants == '') style="display: none" @endif >
                <a href="{{ 'http://'.$_SERVER['HTTP_HOST'].'/albums/'.$album->id.'/join/'.$album->token_participants }}">{{ 'http://'.$_SERVER['HTTP_HOST'].'/albums/'.$album->id.'/join/'.$album->token_participants }}</a>
                <br>
            </div>
            <br>
            <button id="remove_token_invitaion" class="btn btn-danger" @if($album->token_participants == '') style="display: none" @endif >@lang('albums_participants.supprimer_lien')</button>
            <button id="generate_token_invitaion" class="btn btn-primary" @if($album->token_participants != '') style="display: none" @endif >@lang('albums_participants.creer_lien')</button>


        </div>

    </div>


@endsection