@extends('app')


@section('content')

    <div class="gestion">

        <h1>@lang('albums_participants.associer_compte')</h1>

    </div>

    @if(Session::has('success'))

        <div class="alert alert-success stay">{!! Session::get('success') !!}</div>

    @endif

    @if(Session::has('error'))

        <div class="alert alert-danger stay">{!! Session::get('error') !!}</div>

    @endif



    <div class="ajout-participant">

        @lang('albums_participants.txt_10')

        <b>@lang('albums_participants.nom') :</b> {{ $user_participant_fictif->first_name }} <br>
        <b>@lang('albums_participants.prenom') :</b> {{ $user_participant_fictif->last_name }} <br>

        <b>@lang('albums_participants.txt_11') :</b>

        <div class="form-search">
            {!! BootForm::open(['autocomplete' => 'off'])->post() !!}

            {!! BootForm::select('', 'amis_selection')->options($selectAmis)->class('selectpicker')->attribute('data-live-search',true) !!}
            <a href="/amis" class="green">@lang('albums_participants.txt_7')</a>

            <br>

            {!! BootForm::submit('Associer') !!}
            <input type="hidden" id="q_id" name="q_id"/>
            {!! BootForm::close() !!}
        </div>
    </div>


@endsection