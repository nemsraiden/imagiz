@extends('app')

@section('page-class')album-participant-index @endsection


@section('content')


    <div class="gestion">

        <h1>{{$album->nom}} : Participants</h1>

    </div>

    <div class="row">
        <div class="col-md-12">


            @if(Session::has('success'))

                <div class="alert alert-success">{!! Session::get('success') !!}</div>

            @endif
            @if(Session::has('error'))

                <div class="alert alert-danger">{!! Session::get('error') !!}</div>

            @endif


            <h4>@lang('albums_participants.txt_1')</h4>

            <table class="table table-bordered table-participants">
                <thead>
                <tr>
                    <td>@lang('albums_participants.nom')</td>
                    <td>@lang('albums_participants.privilege')</td>
                    <td>@lang('albums_participants.action')</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><strong>{{$album->proprietaire_nom}}</strong></td>
                    <td>@lang('albums_participants.proprietaire')</td>
                    <td></td>
                </tr>
                @foreach($listParticipants as $participant)

                    <tr>

                        <td><strong>{{$participant->users->first_name}} {{$participant->users->last_name}}</strong></td>
                        <td>
                            @if($participant->users->fictif)
                                @lang('albums_participants.txt_2')
                            @else
                                {{$participant->type_privilege}}
                            @endif
                        </td>
                        <td>
                            @if($participant->users->fictif)
                                <a href="{{route('participantAssocier',['id' => $album->id,'id_participant' => $participant->users->id])}}" class="fictif">
                                    <i class="fa fa-user-plus" aria-hidden="true"></i> @lang('albums_participants.txt_3')
                                </a>
                                <br>
                            @endif
                            <a class="delete" href="{{route('participantRemove',['id' => $album->id,'id_participant' => $participant->users->id])}}">
                                <i class="glyphicon glyphicon-trash"></i>
                                @lang('albums_participants.supprimer')
                            </a>
                            @if($participant->type_privilege == 'Administrateur')
                                <a class="privilege" href="{{route('participantPrivilege',['id' => $album->id,'id_participant' => $participant->users->id,'type_privilege' => 'remove'])}}">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                    @lang('albums_participants.retirer_privilege')
                                </a>
                            @else
                                @if(!$participant->users->fictif)
                                    <a class="privilege" href="{{route('participantPrivilege',['id' => $album->id,'id_participant' => $participant->users->id,'type_privilege' => 'Administrateur'])}}">
                                        <i class="fa fa-user-secret" aria-hidden="true"></i>
                                        @lang('albums_participants.mettre_administrateur')
                                    </a>
                                @endif
                            @endif
                        </td>
                    </tr>

                @endforeach

                </tbody>
            </table>

            <i>@lang('albums_participants.txt_4')</i>

            <br><br>

            <a class="btn btn-yellow" href="/albums/{{$album->id}}/participants/inviter">@lang('albums_participants.inviter_participant')</a>

            <hr>

            <div class="ajout-participant">
                <h4>@lang('albums_participants.txt_5') :</h4>

                @lang('albums_participants.txt_6')

                <div class="form-search">
                    {!! BootForm::open(['autocomplete' => 'off'])->post() !!}

                    {!! BootForm::select('', 'amis_selection')->options($selectAmis)->class('selectpicker')->attribute('data-live-search',true) !!}
                    <a href="/amis" class="green">@lang('albums_participants.txt_7')</a>

                    <br>
                    <br>
                    <div class="checkbox" style="margin: 0">
                        <input type="checkbox" id="is_admin" name="is_admin">
                        <label style="font-size: 14px;" for="picture_jaquette">Mettre cette personne administrateur</label>
                    </div>

                    {!! BootForm::submit('Ajouter') !!}
                    <input type="hidden" id="q_id" name="q_id"/>
                    {!! BootForm::close() !!}
                </div>
            </div>


            <div class="ajout-participant">
                <h4>@lang('albums_participants.txt_8')</h4>

                @lang('albums_participants.txt_9')

                {!! BootForm::open()->action(URL::route('participantAddFictif',[$album->id]))->post() !!}
                {!! BootForm::text("Nom", 'last_name')->placeholder('Indiquer un nom')->required() !!}
                {!! BootForm::text("Prénom", 'first_name')->placeholder('Indiquer un prénom')->required() !!}
                {!! BootForm::submit('Ajouter') !!}
                {!! BootForm::close() !!}
            </div>

        </div>

    </div>


@endsection