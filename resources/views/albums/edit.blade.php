@extends('app')

@section('include_js')
    <script language="JavaScript">

        $("#annee").change(function(){
            var montth = $("#annee").val();
            if(montth == 0){
                $("#datemonth").hide();
            }
            else{
                $("#datemonth").slideDown();
            }
        });
    </script>
@endsection

@section('page-class')albumedit @endsection


@section('content')


    <div class="gestion">

        <h1>@lang('albums.album_edit', ['nom' => $album->nom])</h1>

        <div class="actions">
            <ul>
                <li>
                    <a href="#" class="btn btn-default submenu">
                        <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="#" id="supprimer_album">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>@lang('albums.album_delete')
                            </a>
                        </li>
                        <li>
                            <a href="#" id="supprimer_album_photos">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>@lang('albums.photos_delete')
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>



        </div>

    </div>

    <div class="row">
        <div class="col-md-12">

            <input type="hidden" id="album_id" value="{{$album->id}}">


            {!! BootForm::open()->put()->action('/albums/'.$album->id)->id('createAlbum') !!}
            {!! BootForm::text(trans('albums.album_nom'), 'nom')->defaultValue($album->nom) !!}
            {!! BootForm::textarea(trans('albums.description'), 'description')->defaultValue($album->description) !!}
            {!! BootForm::select(trans('albums.annee'), 'annee')->options($listYear)->select($album->annee) !!}


            <div id="datemonth" @if($album->annee == 0) style="display: none" @endif>
                {!! BootForm::select(trans('albums.mois'), 'mois')->options($listMonth)->defaultValue($album->mois) !!}
            </div>


            {!! BootForm::submit(trans('albums.album_save')) !!}

            {!! BootForm::close() !!}

        </div>

    </div>





@endsection