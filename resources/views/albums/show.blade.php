@extends('app')

@section('page-class')albumshow @endsection

@section('content')

    <div class="gestion">

        <h1>{{$album->nom}}</h1>

        <div class="actions">
            <ul>
                @if($album->ID_proprietaire == Auth::id() or in_array(Auth::id(),$album->adminList))
                    <li>
                        <a href="/albums/{{$album->id}}/photos" class="btn btn-default">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i> <span>@lang('albums.photo_add')</span>
                        </a>
                    </li>
                @endif
                <li>
                    <a href="{{route('diaporama',$album->id)}}" class="btn btn-default">
                        <i class="fa fa-picture-o"></i> <span>@lang('albums.afficher_diaporama')</span>
                    </a>
                </li>
                @if($album->ID_proprietaire == Auth::id() or in_array(Auth::id(),$album->adminList))
                    <li class="showonphone">
                        <a href="/albums/{{$album->id}}/config" class="btn btn-default">
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li class="showonphone">
                        <a href="/albums/{{$album->id}}/edit" class="btn btn-default">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                    </li>
                    @if(!empty($next_untaged_picture) and ($album->ID_proprietaire == Auth::id() or in_array(Auth::id(),$album->adminList)))
                        <li class="showonphone">
                            <a href="{{route('PictureUntagedInAlbum',[$album->id,$next_untaged_picture->id])}}" class="btn btn-default">
                                <i class="fa fa-tag" aria-hidden="true"></i>
                            </a>
                        </li>
                    @endif

                @endif
            </ul>



        </div>

    </div>


    @if(Session::has('success'))

        <div class="alert alert-success">{!! Session::get('success') !!}</div>

    @endif


    <div class="row">
        <div class="col-md-8">

            @if(!empty($next_untaged_picture) and ($album->ID_proprietaire == Auth::id() or in_array(Auth::id(),$album->adminList)))
                <div class="alert alert-warning hideonphone">
                    <h4>@lang('albums.txt_22')</h4>
                    @lang('albums.txt_23')
                    <ul>
                        <li >@lang('albums.txt_24')</li>
                        <li >@lang('albums.txt_25')</li>
                    </ul>
                    <a href="{{route('PictureUntagedInAlbum',[$album->id,$next_untaged_picture->id])}}" class="btn-primary btn">@lang('albums.gerer_photos')</a>


                </div>

            @endif


            <div id="albums_photos">


                @if(empty($pictures))
                    <div class="col-md-12">
                        @lang('albums.txt_27')
                    </div>
                @else
                    <div class="grid" >
                        @foreach($pictures as $picture)

                                <div class=" grid-item ">

                                    <a href="{{url('/albums/'.$album->id.'/pictures/'.$picture['id'])}}" data-ngthumb="{{$picture['thumb']}}" data-ngdesc="Description1" >
                                        {!! HTML::image($picture["thumb"], '', array('class' => 'img-responsive')) !!}
                                    </a>

                                </div>


                        @endforeach
                    </div>
                    <div class="waiting-loader">
                        <span class="waiting-loader-children"></span>
                        {!! HTML::image('img/cube.gif','Loading...' ) !!}
                    </div>


                    <!--<div id="nanoGallery3">

                    @foreach($pictures as $picture)

                        <a href="{{$picture['big']}}" data-ngthumb="{{$picture['thumb']}}" data-ngdesc=""  ></a>


                    @endforeach

                    </div>-->

                @endif

            </div>

            @if(!empty($next_untaged_picture) and ($album->ID_proprietaire == Auth::id() or in_array(Auth::id(),$album->adminList)))
                {{--<div class="alert alert-warning">
                    <h4>@lang('albums.txt_22')</h4>
                    @lang('albums.txt_23')
                    <ul>
                        <li >@lang('albums.txt_24')</li>
                        <li >@lang('albums.txt_25')</li>
                    </ul>

                    <a href="{{route('PictureUntagedInAlbum',[$album->id,$next_untaged_picture->id])}}" class="btn-primary btn">@lang('albums.gerer_photos')</a>
                </div>--}}
            @endif
        </div>

        <div class="col-md-4 hideonphone">

            @include('albums.right-menu-about-the-album')

        </div>

    </div>


@endsection