
@extends('nosidebar')

@section('page-class')albumindex @endsection

@section('content')

    @if(Session::has('success'))

        <div class="alert alert-success">{!! Session::get('success') !!}</div>

    @endif

    <div class="gestion">

        <h1>@lang('albums.albums_photos')</h1>

        <div class="actions">
            <ul>
                <li>
                    <a href="/albums/create" class="btn btn-default">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i> <span>@lang('albums.creer_album')</span>
                    </a>
                </li>
            </ul>

        </div>
    </div>


    @if(Auth::check())

        @if(count($albums) === 0)

            <p>@lang('albums.txt_6')</p>

        @else

            <div class="row">

                 @foreach($albums as $album)

                    <div class="col-md-3 col-xs-6 tumb">
                        <a href="/albums/{{$album->id}}" class="thumbnail">
                            <div class="albums-item-thumbs">
                                {!! HTML::image($album->image, '', array('class' => 'img-responsive')) !!}
                            </div>
                            <h4>
                               {{ $album->nom }}
                            </h4>
                        </a>


                    </div>

                 @endforeach

            </div>

        @endif



        <h2 style="margin-top: 15px;">@lang('albums.dossier_temporaire')</h2>

        <p>@lang('albums.txt_30')</p>

        <a href="albums-waiting/" class="btn btn-default"><span class="fa fa-folder"></span> @lang('albums.txt_2')</a>

        <br><br>

        @if(count($last_uploaded_image) > 0)

        <div class="last_photos_uploaded">
            <h2>@lang('albums.txt_3')</h2>

            <div class="row">
                <div class="col-md-2 col-xs-5 text-center">
                    {!! HTML::image($last_uploaded_image->directory.'/thumb_'.$last_uploaded_image->name, '', array('class' => 'img-responsive','style' => 'max-width:150px; max-height:150px')) !!}
                </div>
                <div class="col-md-10 col-xs-7">
                    <p>@lang('albums.txt_4')</p>
                    <p>@lang('albums.txt_5',['date' => date('d M Y à H:i:s', $last_uploaded_image->created_at->timestamp)])</p>
                </div>
            </div>

            <br><br>
        </div>

        @endif




    @else

        <p>@lang('albums.txt_8')</p>
        <p>@lang('albums.txt_9')</p>

    @endif





@endsection