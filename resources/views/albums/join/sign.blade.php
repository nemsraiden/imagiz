@extends('app')

@section('content')


    <div class="col-md-12">

        @if($error)

            <div class="alert alert-danger stay">{{ $message }}</div>

        @else

            <div class="alert alert-success stay">{{ $message }}</div>

        @endif

        <br>
        <a href="/albums" class="btn btn-default"><span class="fa fa-arrow-left "></span> @lang('albums_join.afficher_albums')</a>




    </div>

@endsection
