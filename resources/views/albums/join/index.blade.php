@extends('app')

@section('page-class')diaporama @endsection

@section('include_js')
    <script>
        $(document).ready(function(){
            $("#nanoGallery1a").nanoGallery({
                thumbnailWidth: 'auto',
                thumbnailHeight: 200,

                colorScheme: 'none',
                thumbnailHoverEffect: [{ name: 'labelAppear75', duration: 300 }],
                theme: 'light',
                thumbnailGutterWidth : 0,
                thumbnailGutterHeight : 0,
                i18n: { thumbnailImageDescription: 'Afficher la photo', thumbnailAlbumDescription: 'Open Album' },
                thumbnailLabel: { display: true, position: 'overImageOnMiddle', align: 'center' },
                items:[
                        @foreach($pictures as $picture)

                            {
                                src: '/{{$picture['big']}}',
                                srct: '/{{$picture['middle']}}',	// thumbnail url
                                title: '{{ $album->nom }}'       // thumbnail title
                            },

                        @endforeach
                ],
                itemsBaseURL: '../../../',
                dataSorting: 'random',
            });

            $(window).load(function(){
                $(".waiting-loader").hide();
                $("#nanoGallery1a").fadeIn();

            });
        })

    </script>
@endsection

@section('content')


    <div class="col-md-12">

        <div style="display: none">
            {!! HTML::image('img/album_vacances.jpg', '', array('width' => '300')) !!}
        </div>

        @if($album == null)

            <div class="alert alert-danger">@lang('albums_join.txt_1')</div>

        @else

            <h1 class="bordered">@lang('albums_join.albums_photo',['album_bom' => $album->nom])</h1>

            <h4 class="lead-green">@lang('albums_join.txt_2')</h4>

            <div id="nanoGallery1a" style="display: none; ">

            </div>

            <div class="waiting-loader" style="position: fixed">
                <span class="waiting-loader-children"></span>
                {!! HTML::image('img/cube.gif','Loading...' ) !!}
            </div>

            <hr>


            @if($album->token_participants == $token)

                <h2>@lang('albums_join.avoir_acces')</h2>

                @lang('albums_join.txt_3')
                <ul>
                    <li>@lang('albums_join.txt_5')</li>
                    <li>@lang('albums_join.txt_6')</li>
                    <li>@lang('albums_join.txt_7')</li>
                    <li>@lang('albums_join.txt_8')</li>
                    <li>@lang('albums_join.txt_9')</li>
                    <li>@lang('albums_join.txt_10')</li>
                    <li>...</li>
                </ul>
                <br>
                <a href="/albums/{{$album->id}}/join/{{$token}}/sign" class="btn btn-primary">@lang('albums_join.rejoindre_album')</a>

            @elseif($album->token_viewers == $token)

                <h2>@lang('albums_join.avoir_acces')</h2>

                @lang('albums_join.txt_4')
                <ul>
                    <li>@lang('albums_join.txt_11')</li>
                    <li>@lang('albums_join.txt_12')</li>
                    <li>@lang('albums_join.txt_13')</li>
                    <li>@lang('albums_join.txt_14')</li>
                    <li>...</li>
                </ul>
                <br>
                <a href="/albums/{{$album->id}}/join/{{$token}}/sign" class="btn btn-primary">@lang('albums_join.rejoindre_album')</a>


            @endif

        @endif

    </div>

@endsection
