<h2 class="bordered first">@lang('albums.informations_album')</h2>

<table class="table table-condensed table-bordered">
    <tr>
        <td width="120">@lang('albums.album_nom')</td>
        <td>{{ $album->nom }}</td>
    </tr>
    <tr>
        <td>@lang('albums.date')</td>
        <td>
            @if($album->annee != 0)
                {{ ucfirst($album->mois) }} {{ $album->annee }}
            @else
                @lang('albums.txt_19')
            @endif
        </td>
    </tr>
    <tr>
        <td>@lang('albums.proprietaire')</td>
        <td>{{ $album->owner_nom }}</td>
    </tr>
    <tr>
        <td>@lang('albums.description')</td>
        <td>{!! nl2br($album->description) !!}</td>
    </tr>

</table>

@if($album->ID_proprietaire == Auth::id())
    <a href="/albums/{{$album->id}}/edit" class="btn btn-default"><span class="fa fa-pencil"> @lang('albums.modifier_album')</span></a>
@endif

<h2 class="bordered">@lang('albums.liste_participants')</h2>


<ul class="user-list">
    <li>{{ $album->proprietaire_nom }}
        @if($album->ID_proprietaire != Auth::id())
            <a href="{{url('/amis/add/'.$album->ID_proprietaire.'/fromalbum/'.$album->id)}}"><i title="Ajouter cette personne dans vos amis" class="fa fa-user-plus" aria-hidden="true"></i></a>
        @endif
    </li>
    @foreach($listParticipants as $participant)

        <li>
            {{$participant->users->first_name}} {{$participant->users->last_name}}
            @if($participant->type_privilege == 'Administrateur') (Admin) @endif
            @if(!in_array($participant->users_id, array_column($amis->toArray(), 'users_id_amis')) && $participant->id != Auth::id() && !$participant->users->fictif)
                <a href="{{url('/amis/add/'.$participant->users_id.'/fromalbum/'.$album->id)}}"><i title="Ajouter cette personne dans vos amis" class="fa fa-user-plus" aria-hidden="true"></i></a>
            @endif
        </li>


    @endforeach
</ul>
<br/>
@if($album->ID_proprietaire == Auth::id())
<a href="/albums/{{$album->id}}/participants" class="btn btn-default"><span class="fa fa-pencil"> @lang('albums.modifier_participants')</span></a>
@endif

<br/><br/>

<h2 class="bordered">@lang('albums.liste_viewers')</h2>

<ul class="user-list">
    @foreach($listViewers as $viewer)

        <li>
            {{$viewer->users->first_name}} {{$viewer->users->last_name}}
            @if(!in_array($viewer->users_id, array_column($amis->toArray(), 'users_id_amis')) && $viewer->id != Auth::id() && !$viewer->users->fictif)
                <a href="{{url('/amis/add/'.$viewer->users_id.'/fromalbum/'.$album->id)}}"><i title="Ajouter cette personne dans vos amis" class="fa fa-user-plus" aria-hidden="true"></i></a>
            @endif
        </li>

    @endforeach
</ul>
<br/>
@if($album->ID_proprietaire == Auth::id())
    <a href="/albums/{{$album->id}}/viewers" class="btn btn-default"><span class="fa fa-pencil"> @lang('albums.modifier_viewers')</span></a>
@endif
