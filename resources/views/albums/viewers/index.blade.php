@extends('app')

@section('page-class')album-viewers-index @endsection


@section('content')


    <div class="gestion">

        <h1>{{$album->nom}} : Viewers</h1>

    </div>
    <div class="row">
        <div class="col-md-12">

            @if(Session::has('success'))

                <div class="alert alert-success">{!! Session::get('success') !!}</div>

            @endif
            @if(Session::has('error'))

                <div class="alert alert-danger">{!! Session::get('error') !!}</div>

            @endif


            <h4>@lang('albums_viewers.txt_1')</h4>

            <table class="table table-bordered table-participants">
                <thead>
                <tr>
                    <td>@lang('albums_viewers.nom')</td>
                    <td>@lang('albums_viewers.action')</td>
                </tr>
                </thead>
                <tbody>
                @foreach($listViewers as $viewer)

                    <tr>

                        <td><strong>{{$viewer->users->first_name}} {{$viewer->users->last_name}}</strong></td>
                        <td>
                            <a class="delete" href="{{route('viewerRemove',['id' => $album->id,'id_participant' => $viewer->users->id])}}">
                                <i class="glyphicon glyphicon-trash"></i>
                                @lang('albums_viewers.supprimer')
                            </a>
                        </td>
                    </tr>

                @endforeach

                </tbody>
            </table>

            <i>@lang('albums_viewers.txt_2')</i>

            <br><br>

            <a class="btn btn-yellow" href="/albums/{{$album->id}}/viewers/inviter">@lang('albums_viewers.inviter_viewer')</a>

            <hr>

            <div class="ajout-participant">
                <h4>@lang('albums_viewers.ajouter_viewer') :</h4>


                @lang('albums_viewers.txt_3')

                <div class="form-search">
                    {!! BootForm::open(['autocomplete' => 'off'])->post() !!}


                    {!! BootForm::select('', 'amis_selection')->options($selectAmis)->class('selectpicker')->attribute('data-live-search',true) !!}
                    <a href="/amis" class="green">@lang('albums_viewers.txt_4')</a>

                    <br>

                    {!! BootForm::submit('Ajouter') !!}
                    <input type="hidden" id="q_id" name="q_id"/>
                    {!! BootForm::close() !!}
                </div>
            </div>

        </div>

    </div>


@endsection