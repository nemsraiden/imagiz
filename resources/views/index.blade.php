<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Créer et gérer vos albums photos, partager vos albums photo avec vos amis, système de recherche avancé">
    <meta name="author" content="">

    <title>Imagiz, gestion de vos images</title>

    <!-- Bootstrap core CSS -->
    {!! HTML::style( asset('css/bootstrap.css') ) !!}
    {!! HTML::style( asset('css/bootstrap-select.min.css') ) !!}
    {!! HTML::style( asset('css/font-awesome.min.css') ) !!}
    {!! HTML::style( asset('css/fileinput.css') ) !!}
    {!! HTML::style( asset('css/nanogallery.css') ) !!}
    {!! HTML::style( asset('css/themes/clean/nanogallery_clean.min.css') ) !!}
    {!! HTML::style( asset('css/themes/light/nanogallery_light.min.css') ) !!}
    {!! HTML::style( asset('css/main.css') ) !!}
    {!! HTML::style( asset('css/mobile.css') ) !!}
    {!! HTML::style( asset('css/awesome-bootstrap-checkbox.css') ) !!}

    @yield('include_css')


<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-52257844-7"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-52257844-7');
    </script>



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
</head>

<body>
<div id="page">

    <div class="body_lightbox_loading">
        <div class="cube_loading"><div class='uil-cube-css' style='-webkit-transform:scale(0.6)'><div></div><div></div><div></div><div></div></div></div>
    </div>



    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Imagiz.be</a>
            </div>

            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/"><span class="fa fa-home"></span> @lang('menu.home')</a></li>
                    @if(Auth::user())
                        <li ><a href="/albums/"><span class="fa fa-camera"></span> @lang('menu.mes_albums_photos')</a></li>
                        <li ><a href="/amis/"><span class="fa fa-users"></span> @lang('menu.mes_amis')</a></li>
                        <li ><a href="/notifications/"><span class="fa fa-bell"></span>
                                @if($myalerts_count > 0)
                                    <b class="notifs"> @lang('menu.notifications') ({{ $myalerts_count }})</b>
                                @else
                                    @lang('menu.notifications') ({{ $myalerts_count }})
                                @endif
                            </a></li>
                        <li><a href="/recherche"><span class="fa fa-search"></span> Rechercher une photo</a></li>
                    @endif
                </ul>
               @if(Auth::check())
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown dropdown-submenu"><a href="/user/edit" class="dropdown-toggle"><span class="fa fa-user"></span> Mon compte</a>

                        </li>
                        <li><a href="/user/logout"><span class="fa fa-sign-out "></span> @lang('menu.logout')</a></li>
                    </ul>


                @else

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/user/login"><span class="fa fa-user "></span> @lang('menu.se_connecter')</a></li>
                    </ul>

                @endif
            </div><!--/.nav-collapse -->



        </div>
    </nav>

    @yield('app')

    <div class="body-lightbox-delete">
        <div class="supprimer-img">
            <strong>Attention,</strong> @lang('alertes.alert_1')<br>
            <br>
            @lang('alertes.alert_2')<br>
            <br>
            <a href="{{ url('albums/{id}/pictures/{id_picture}/remove') }}" class="btn btn-danger" style="width: 340px">@lang('alertes.alert_3')</a>
            <br><br>
            <button class="btn btn-default">@lang('alertes.Annuler')</button>
        </div>

        <div class="supprimer-album">
            <strong>Attention,</strong> @lang('alertes.alert_4') <br>
            <br>
            @lang('alertes.alert_5') <br>
            <br>
            <a href="{{ url('albums/{id}/remove') }}" class="btn btn-danger" style="width: 340px">@lang('alertes.alert_6')</a>
        </div>

        <div class="supprimer-album-photos">
            <strong>Attention,</strong> @lang('alertes.alert_7') <br>
            <br>
            @lang('alertes.alert_8') <br>
            <br>
            <a href="{{ url('albums/{id}/removePhotos') }}" class="btn btn-danger" style="width: 340px">@lang('alertes.alert_9')</a>
        </div>
    </div>


    <footer class="footer">
        <div class="big">Imagiz.be</div>
        Created by NemSRaiden
    </footer>

    <input type="hidden" id="get_js_url" name="get_js_url" value="{{ url() }}"/>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    {!! HTML::script('js/bootstrap.min.js') !!}
    {!! HTML::script('js/fileinput.min.js') !!}
    {!! HTML::script('js/fileinput_locale_fr.js') !!}
    {!! HTML::script('js/isotope.pkgd.min.js') !!}
    {!! HTML::script('js/jquery.nanogallery.min.js') !!}
    {!! HTML::script('js/bootstrap-select.min.js') !!}

    @yield('include_js')


    {!! HTML::script('js/main.js') !!}

</div>

</body>
</html>
