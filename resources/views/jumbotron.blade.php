<?php
$imgSlider = [];
$imgSlider[] = 'slider1.jpg';
$imgSlider[] = 'slider2.jpg';
$imgSlider[] = 'slider3.jpg';
$imgSlider[] = 'slider4.jpg';
shuffle($imgSlider);
?>

<div class="container-fluid headerJumbo ">
    <div class="row">
        <div class="jumbotron jumbotron-light " style="background-image: url({{url('/img/'.$imgSlider[0].'')}} ) ">

            <div class="presentation">
                <h1>@lang('home.label_1')</h1>
                <p class="lead">@lang('home.label_2')</p>
            </div>
        </div>
    </div>
</div>