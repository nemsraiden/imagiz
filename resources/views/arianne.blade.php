@if(isset($arianne))
    <ul class="arianne">
        @foreach($arianne as $key => $value)
            @if($value !== '')
                <li><a href="{{$value}}">{{ $key }}</a></li>
            @else
                <li><span>{{ $key }}</span></li>
            @endif

        @endforeach
    </ul>
@endif