@extends('app')

@section('include_css')
    {!! HTML::style( asset('css/lightgallery.min.css') ) !!}
    {!! HTML::style( asset('css/lg-transitions.min.css') ) !!}
@endsection

@section('include_js')
    {!! HTML::script('js/lightgallery.js') !!}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
    {!! HTML::script('js/lg-thumbnail.min.js') !!}
    {!! HTML::script('js/lg-fullscreen.min.js') !!}
    {!! HTML::script('js/lg-zoom.min.js') !!}
    {!! HTML::script('js/lg-autoplay.min.js') !!}

    <script language="JavaScript">
        $(document).ready(function(){
           $("#personnes_selection").on('change', function() {
               var selectedValues = $('#personnes_selection').val();
               if(selectedValues && selectedValues.length > 0){
                   selectedValues.forEach(function(element) {

                   });

                   $(".search-checkbox").show();
                   if(selectedValues.length == 1){
                       $("#alone_1 span").text($("#personnes_selection option[value='"+selectedValues+"']").text());
                       $("#alone_on_photo label").show();
                       $("#alone_1").show();
                       $("#alone_2").hide();
                   }
                   else if(selectedValues.length > 1 && selectedValues.length <= 4){

                       var txt_alone = '';
                       var i=1;
                       selectedValues.forEach(function(element){
                           if(i == selectedValues.length - 1) var separator = ' et ';
                           else if(i == selectedValues.length) var separator = '';
                           else var separator = ', ';
                           txt_alone += $("#personnes_selection option[value='"+element+"']").text()+separator;
                           i++;
                       });
                       txt_alone = txt_alone.substr(0,txt_alone.length - 1);
                       $("#alone_on_photo label").show();
                       $("#alone_2 span").text(txt_alone);
                       $("#alone_1").hide();
                       $("#alone_2").show();
                   }
                   else{
                       $("#alone_on_photo label").show();
                       $("#alone_2 span").text(' ces personnes ');
                       $("#alone_1").hide();
                       $("#alone_2").show();
                   }

                   if(selectedValues.length > 1){
                       $("#all_together").show();
                   }


               }
               else{
                   $(".search-checkbox").hide();
                   $("#alone_on_photo label").hide();
                   $("#all_together").hide();
                   $("#checkbox_alone_on_photo").attr('checked', false);
                   $("#checkbox_together_on_photo").attr('checked', false);
               }
           });

            $("#albums_selection").change(function(){
                $("#alone_on_photo label").hide();
                $("#alone_1").hide();
                $("#alone_2").hide();

                $("#alone_1 span").text('');
                $("#alone_2 span").text('');
            })
        });
    </script>


@endsection

@section('content')

    <div class="gestion">
        <h1>@lang('recherche.recherche_photos')</h1>
    </div>

    <div id="recherche">

        {!! BootForm::open()->post()->action(URL::route('search_ajax'))->class('search-form')->id('search-form') !!}

        <div >

            {!! BootForm::select('Veuillez sélectionner 1 ou plusieurs albums', 'albums_selection')->options($selectList)->multiple()->class('selectpicker')->attribute('data-actions-box',true) !!}
            <div id="type_de_photo" style="display: none">
                <label >@lang('recherche.type') :</label>
                <div class="checkbox checkbox-primary checkbox-circle" style="margin-top: 0px">
                    <div style="min-width: 100px; display: inline-block">
                        <input type="checkbox" id="checkbox_paysage" name="checkbox_paysage" checked>
                        <label for="checkbox_paysage">@lang('recherche.Paysage')</label>
                    </div>
                    <div style="display: inline-block">
                        <input type="checkbox" id="checkbox_personnes" name="checkbox_personnes" checked>
                        <label for="checkbox_personnes">@lang('recherche.Personne')</label>
                    </div>
                </div>
            </div>

            <div class="personnes_selection">
            {!! BootForm::select(trans('recherche.txt_1'), 'personnes_selection')->multiple()->class('selectpicker')->attribute('data-actions-box',true) !!}
            </div>

            <div class="search-checkbox" id="alone_on_photo">
                <div class="checkbox checkbox-primary checkbox-circle" style="margin-top: 0px">
                    <div style="display: inline-block">
                        <input type="checkbox" id="checkbox_alone_on_photo" name="checkbox_alone_on_photo">
                        <label for="checkbox_alone_on_photo">
                            <div id="alone_1">@lang('recherche.txt_2')</div>
                            <div id="alone_2">@lang('recherche.txt_3')</div>
                        </label>
                    </div>
                </div>
            </div>

            <div class="search-checkbox">
                <div class="checkbox checkbox-primary checkbox-circle" style="margin-top: 0px">
                    <div style="display: inline-block">
                        <input type="checkbox" id="checkbox_together_on_photo" name="checkbox_together_on_photo" checked>
                        <label id="all_together" for="checkbox_together_on_photo">@lang('recherche.txt_4')</label>
                    </div>
                </div>
            </div>

        </div>

        <button type="submit" id="recherche_photo" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> @lang('recherche.recherche_photos')</button>

        <div class="erreur_search"> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <span class="txt"></span></div>

        {!! BootForm::close() !!}
        <div style="clear: both"></div>

    </div>

    <hr>


    <div id="search_results">

    </div>

@endsection