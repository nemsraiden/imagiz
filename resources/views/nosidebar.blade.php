
@extends('index')



@section('app')

    @yield('jumbotron')

    <div class="container @yield('page-class','')" id="main">

        @include('arianne')

        <div class="row">
            <div class="col-md-12">
                @yield('content')
            </div>
        </div>

    </div>

@endsection

