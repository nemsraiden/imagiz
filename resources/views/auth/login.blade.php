@extends('app')

@section('page-class')login @endsection

@section('jumbotron')

	@include('jumbotron')

@endsection

@section('content')

	@if(Session::has('success'))

		<div class="alert alert-success">{!! Session::get('success') !!}</div>

	@endif

	<h1>Login</h1>
	@include('auth.login_form')


@endsection
