@if(!Auth::check())
    @if (count($errors) > 0)
        <div class="alert alert-danger">

            <strong>Whoops!</strong> @lang('auth.form_error')<br><br>
            <ul>

                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach

            </ul>

        </div>
    @endif
@endif

@if(Auth::check())

    @include('albums.right-menu')


@else

<form class="form-horizontal" id="form-connexion" role="form" method="POST" action="{{ url('/user/login') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
        <label class="col-md-12 control-label">@lang('auth.email')</label>
        <div class="col-md-12">
            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-12 control-label">@lang('auth.password')</label>
        <div class="col-md-12">
            <input type="password" class="form-control" name="password">
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12 control-label" style="margin-top: 0px; padding-top: 0px">
            <div class="checkbox checkbox-primary checkbox-circle" style="margin-top: 0px; padding-top: 0px">
                <input type="checkbox" name="remember" id="remember"> <label for="remember">@lang('auth.stay_connected')</label>
            </div>
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary">@lang('auth.connect')</button>
            <a href="{{ url('/user/inscription') }}"class="btn btn-primary">@lang('auth.create_account')</a>
            <br/>
            <a  href="{{ url('/password/email') }}">- @lang('auth.pwd_forget')</a>
        </div>
    </div>
</form>

@endif
