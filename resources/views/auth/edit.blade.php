@extends('app')

@section('page-class')register @endsection

@section('content')

    <div class="gestion">
        <h1>Mon compte</h1>
    </div>


    <div class="panel panel-default">
        <div class="panel-heading">@lang('auth.modifier_compte')</div>
        <div class="panel-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong>@lang('auth.form_error')<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(Session::has('success'))

                <div class="alert alert-success">{!! Session::get('success') !!}</div>

            @endif

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/user/edit') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group  @if($errors->has('last_name')) has-error @endif ">
                            <label class="col-md-12 control-label">@lang('auth.nom')</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="last_name" value="{{ old('last_name') ? old('last_name') : $user->last_name }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group @if($errors->has('first_name')) has-error @endif">
                            <label class="col-md-12 control-label">@lang('auth.prenom')</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="first_name" value="{{ old('first_name') ? old('first_name') : $user->first_name }}">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group @if($errors->has('email')) has-error @endif">
                    <label class="col-md-12 control-label">@lang('auth.email')</label>
                    <div class="col-md-12">
                        <input type="email" class="form-control" name="email" value="{{ old('email') ? old('email') : $user->email }}">
                    </div>
                </div>

                <div class="form-group @if($errors->has('password')) has-error @endif">
                    <label class="col-md-12 control-label">@lang('auth.password')</label>
                    <div class="col-md-12">
                        <input type="password" class="form-control" name="password">
                    </div>
                </div>

                <div class="form-group @if($errors->has('password_confirmation')) has-error @endif">
                    <label class="col-md-12 control-label">@lang('auth.password_confirm')</label>
                    <div class="col-md-12">
                        <input type="password" class="form-control" name="password_confirmation">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12 ">
                        <button type="submit" class="btn btn-primary">
                            @lang('auth.save')
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
