@extends('app')

@section('jumbotron')

	@include('jumbotron')

@endsection

@section('page-class')login @endsection

@section('content')
    <h1>@lang('auth.pwd_recup')</h1>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> @lang('auth.txt_1')<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <div class="col-md-6">
                <label class="control-label">@lang('auth.email')</label>
                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary">
                    @lang('auth.txt_2')
                </button>
            </div>
        </div>
    </form>
@endsection
