@extends('app')

@section('content')

    <h1>Ajouter des vidéos</h1>

    <div class="row">
        <div class="col-md-12">



            @if(isset($authUrl))

                <h2 class="bordered">Autorisation nécessaire</h2>

                Vous devez authorize <a href="{{$authUrl}}">youtube</a> avant de continuer

            @else

                {!! $htmlBody !!}

            @endif


        </div>
    </div>






@endsection