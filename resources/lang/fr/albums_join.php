<?php

return [
    "albums_photo" => "Album photo <strong>:album_bom</strong>",
    "avoir_acces" => "Vous désirez avoir accès à cet album ?",
    "rejoindre_album" => "Rejoindre cet album",
    "afficher_albums" => "Afficher mes albums",
    "txt_1" => "Ce token n'est pas valide",
    "txt_2" => "Voici quelques photos de cet album....",
    "txt_3" => "<p>En tant que participant, cet album sera ajouté automatiquement à la liste de vos albums. <br/>
                    Vous pourrez intérargir avec cet album :</p>",
    "txt_4" => "<p>En tant que viewer, cet album sera ajouté automatiquement à la liste de vos albums. <br/>
                Vous pourrez intérargir avec cet album :</p>",
    "txt_5" => "Consulter toutes les photos de l'album",
    "txt_6" => "Ajouter des photos et les gérées (si vous avez les autorisations nécéssaires)",
    "txt_7" => "Etre taggué sur les photos",
    "txt_8" => "Effectuer des recherches",
    "txt_9" => "Télécharger les photos",
    "txt_10" => "Télécharger un zip contenant toutes les photos",
    "txt_11" => "Consulter toutes les photos de l'album",
    "txt_12" => "Effectuer des recherches",
    "txt_13" => "Télécharger les photos",
    "txt_14" => "Télécharger un zip contenant toutes les photos",
    "txt_15" => "",
    "txt_16" => "",
    "txt_17" => "",
];