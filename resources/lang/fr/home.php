<?php

return [
    "label_1"      => "Imagiz, gestion de vos images !",
    "label_2"             => "Gérer vos albums photos en toute facilité !",
    "alt_1"             => "Gérer vos albums photos",
    "with_imagiz_1"             => "Avec Imagiz, tu peux gérer facilement tes albums photos :",
    "with_imagiz_2"             => "Création d'album photo par thèmes",
    "with_imagiz_3"             => "Partager vos albums photos à vos amis",
    "with_imagiz_4"             => "Identifie tes amis sur les photos",
    "with_imagiz_5"             => "Commente les photos",
    "with_imagiz_6"             => "Recherche avancée de photos",
    "jumbotron_1"             => "Créer et gérer vos albums photos",
    "jumbotron_2"             => "Partager vos albums photo avec vos amis",
    "jumbotron_3"             => "Système de recherche avancé",
    "espace_membres"             => "Espace membres",
	"compte_explication" => "Gérer vos compte Imagiz.be"
];