<?php

return [
    "alert_1" => "cette image sera supprimé définitivement de votre album.",
    "alert_2" => "Vous êtes sûr de vouloir supprimer cette image ?",
    "alert_3" => "Je confirme la suppresion de cette image",
    "alert_4" => "cette album sera supprimé définitivement et toutes les images aussi.",
    "alert_5" => "Vous êtes sûr de vouloir supprimer cet album ?",
    "alert_6" => "Je confirme la suppresion de cette album",
    "alert_7" => "toutes les photos de cet album vont être supprimée.",
    "alert_8" => "Vous êtes sûr de vouloir supprimer toutes les photos de cet album ?",
    "alert_9" => "Je confirme la suppresion",
    "alert_10" => "",
    "Annuler" => "Annuler",
];