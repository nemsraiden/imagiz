<?php

return [
    "albums_photo" => "Album photos  << :album_nom >>",
    "back_to_album" => "Revenir à l'album",
    "viewers" => "Viewers de l'album ':album_nom'",
    "nom" => "Nom",
    "action" => "Action",
    "supprimer" => "Supprimer",
    "inviter_viewer" => "Inviter un viewer",
    "ajouter_viewer" => "Ajouter un viewer",
    "supprimer_lien" => "Supprimer le lien",
    "creer_lien" => "Créér un lien d'invitaion",
    "txt_1" => "Voici les différentes personnes qui sont 'viewers' de cet album :",
    "txt_2" => "Un viewer est une personne qui peut consulter votre album photo mais qui n'était pas présent lors de l'événement",
    "txt_3" => "<p>Vous pouvez choisir une personne parmis votre liste d'amis. <br/>
                    Lorsque cette personne se connectera, elle pourra consulter votre album photos. <br>
                    Elle ne pourra cependant pas en modifier le contenu.</p>",
    "txt_4" => "Ajouter des personnes à cette liste",
    "txt_5" => "<p>
                Vous pouvez partager le lien ci-dessous pour inviter un viewer à votre album. <br>
                La personne qui accède au lien aura un bref apercu de votre album photo. <br>
                Il sera ensuite invité à se connecter ou s'enregistrer pour rejoindre automatiquement votre album en tant que viewer. <br>
                Ce lien peut etre utilisé plusieurs fois

            </p>",
    "txt_6" => "Lien d'invitation d'un viewer à votre album",
];