<?php

return [
    "albums_photo" => "Album photos  << :album_nom >>",
    "back_to_album" => "Revenir à l'album",
    "participants_album" => "Participants de l'album ':album_nom'",
    "nom" => "Nom",
    "prenom" => "Prénom",
    "privilege" => "Privilège",
    "action" => "Action",
    "proprietaire" => "Propriétaire",
    "supprimer" => "Supprimer",
    "retirer_privilege" => "Retirer les privilèges",
    "mettre_administrateur" => "Mettre administrateur",
    "associer_compte" => "Associer un compte",
    "inviter_participant" => "Inviter un participant",
    "supprimer_lien" => "Supprimer le lien",
    "creer_lien" => "Créér un lien d'invitaion",
    "txt_1" => "Voici les différentes personnes qui ont accès à cet album :",
    "txt_2" => "Utilisateur non enregistré",
    "txt_3" => "Associé un compte existant",
    "txt_4" => "Un participant est une personne qui peut consulter votre album et qui était présent lors de l'événement<br/>Il faut mettre un participant administrateur pour lui permettre d'ajouter des photos",
    "txt_5" => "Ajouter un participant (déjà inscrit sur le site)",
    "txt_6" => "<p>Vous pouvez choisir une personne parmis votre liste d'amis. <br/>
                    Lorsque cette personne se connectera, elle aura accès à votre album photos. <br>
                    Si elle est administrateur, elle pourra ajouter et gérer ses propres photos</p>",
    "txt_7" => "Ajouter des personnes à cette liste",
    "txt_8" => "Ajouter un participant (pas encore inscrit) :",
    "txt_9" => "<p>Il est possible d'ajouter une personne a votre album, meme si celle-ci n'est pas (encore) inscrit sur le site.
                    <br> Cela permettra de pouvoir l'identifier sur les photos. <br>
                    Par la suite il sera possible d'associer cette personne a un compte existant.</p>",
    "txt_10" => "<p>
                    L'utilisateur suivant n'existe pas vraiment sur imagiz.be, il est fictif. Cela vous permet de le taguer sur les photos de votre album.<br/>
                    En associant un utilisateur fictif à un utilisateur existant, l'utilisateur existant sera tagué à la place de l'utilisateur fictif, qui sera lui supprimé.
                    <br>
                    L'utilisateur existant pourra se connecter et voir votre album photos.
                    <br>
                </p>",
    "txt_11" => "Je veux associer ce compte avec",
    "txt_12" => "Revenir à la liste des participants",
    "txt_13" => "<p>
                Vous pouvez partager le lien ci-dessous pour inviter un participant à votre album. <br>
                La personne qui accède au lien aura un bref apercu de votre album photo. <br>
                Il sera ensuite invité à se connecter ou s'enregistrer pour rejoindre automatiquement votre album en tant que participant. <br>
                Ce lien peut etre utilisé plusieurs fois

            </p>",
    "txt_14" => "Lien d'invitation d'un participant à votre album",
];