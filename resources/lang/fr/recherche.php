<?php



return [
    "recherche_photos" => "Rechercher des photos",
    "type" => "Type de photo",
    "Paysage" => "Paysage",
    "Personne" => "Personne",
    "txt_1" => "Vous recherchez des personnes en particulier ?",
    "txt_2" => "Seul <span></span> sera présent sur les photos",
    "txt_3" => "Seul <span></span> seront présents sur les photos",
    "txt_4" => "Toutes les personnes sélectionnées doivent etre présentes sur chaque photos",
];