<?php

return [
    "back_to_album" => "Revenir à l'album",
    "txt_1" => "Diaporama de l'album <strong>:album_nom</strong>",
];