<?php

return [
    "home" => "Home",
    "en_savoir_plus" => "En savoir plus",
    "mes_albums_photos" => "Mes albums photos",
    "mes_amis" => "Mes amis",
    "notifications" => "Notifications",
    "recherche_photo" => "Rechercher une photo",
    "mon_compte" => "Mon compte",
    "mes_informations" => "Mes informations",
    "logout" => "Logout",
    "se_connecter" => "Se connecter",
];