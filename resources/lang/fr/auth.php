<?php

return [

    "failed"             => "L'email ou le mot de passe est incorrect",
    "modifier_compte" => "Modifier mon compte",
    "form_error" => "Il y a des erreurs avec le formulaire.",
    "nom" => "Nom",
    "prenom" => "Prénom",
    "email" => "Email",
    "password" => "Password",
    "password_confirm" => "Confirmation du password",
    "save" => "Sauvegarder",
    "stay_connected" => "Reste connecté",
    "connect" => "Se connecter",
    "create_account" => "Créer un compte",
    "pwd_forget" => "Mot de passe oublié ?",
    "pwd_recup" => "Récupérer le mot de passe",
    "txt_1" => "Il y a un problème avec vos données",
    "txt_2" => "Envoyer un lien de récupération de mot de passe",
    "register" => "S'enregistrer",
    "pwd_change" => "Changer votre mot de passe",
    "pwd_change2" => "Changer mon mot de passe"
];