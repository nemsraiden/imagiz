<?php

return [
    "txt_1" => "Créer et partager vos albums en 3 étapes",
    "txt_2" => "Crée ton album",
    "txt_3" => "Il suffit de choisir un nom, une date, une location et en quelques clics ton album photo est créé",
    "txt_4" => "Ajoute des photos",
    "txt_5" => "Uploader des images dans ton album est très facile !. Tu peux uploader plusieurs images simultanément pour gagner du temps.",
    "txt_6" => "Partage le avec tes amis",
    "txt_7" => "Donne accès a ton album a tes amis, ta famile,... Ils pourront venir sur ce site, consulter ton album et y participer",
    "txt_8" => "Un système de recherche avancé",
    "txt_9" => "Imagiz.be te permet de retrouver facilement des photos",
    "txt_10" => "Tu peux utiliser le système de recherche pour retrouver des photos en fonction de critères particulier...",
    "txt_11" => "Recherche des photos dans 1 ou plusieurs albums simultanément",
    "txt_12" => "Recherche uniquement des paysages",
    "txt_13" => "Tu peux rechercher des photos liées aux personnes qui sont tagués dessus.",
];