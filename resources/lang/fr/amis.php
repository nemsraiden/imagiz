<?php

return [
    "add_amis" => "Ajouter un amis",
    "txt_1" => "Vous pouvez ajouter des personnes qui sont inscrites sur imagiz.be via leur adresse mail ou nom/prénom <br/>
                Vous pourrez ensuite ajouter ces amis à vos différents album en tant que participant ou viewer.",
    "txt_2" => "Par adresse mail :",
    "txt_3" => "Indiquer une adresse email",
    "txt_4" => "Ajouter cet utilisateur",
    "txt_5" => "Par nom ou prénom",
    "txt_6" => "Indiquer un nom ou un prénom",
    "txt_7" => "Ajouter cet utilisateur",
	"txt_8" => "Avoir des amis vous permets de les ajouter à vos albums et de les taguer sur vos photos",
    "amis_list" => "Liste de mes amis",
    "nom" => "Nom",
    "prenom" => "Prénom",
    "email" => "Email",
    "supprimer" => "Supprimer",
];