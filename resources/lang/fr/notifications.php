<?php



return [
    "mes_notifications" => "Mes notifications",
    "new_album" => "Nouvel album",
    "new_photos" => "Nouvelles photos",
    "tagged" => "Vous avez été taggué",
    "album_delete" => "Album supprimé",
    "album_remove" => "Album retiré",
    "new_participant" => "Nouveau participant",
    "new_viewers" => "Nouveau viewer",
	"explication" => "Grâce à vos notifications vous êtes tenu au courant des différents changements dans vos albums",
    "txt_1" => "<b>:first_name :last_name</b> vous a ajouté a l'album photo <b>:album_nom</b> en tant que <u>participant</u>",
    "txt_2" => "Vous pouvez désormais consulter cet album, ajouter et gérer des photos et également etre tagué sur des photos.",
    "txt_3" => "Découvrir cet album",
    "txt_4" => "Ouvrir cet album",
    "txt_5" => "<b>:first_name :last_name</b> a ajouté <b>:count</b> photos dans l'album photo <b>:album_nom</b>",
    "txt_6" => "<b>:first_name :last_name</b> vous a ajouté a l'album photo <b>:album_nom</b> en tant que <u>viewer</u>",
    "txt_7" => "Cela signifie que vous pouvez consulter cet album meme si vous n'étiez pas présent lors de l'événement.",
    "txt_8" => "Vous ne pouvez pas ajouter de nouvelles photos n'y etre tagué sur des photos de cet album.",
    "txt_9" => "<b>:first_name :last_name</b> vous a tagué sur une photo de l'album <b>:album_nom</b>",
    "txt_10" => "Cela permet de mieux savoir qui était visible sur une photo mais également d'obtenir de meilleurs résultats lors de la recherche",
    "txt_11" => "Afficher la photo",
    "txt_12" => "<b>:first_name :last_name</b> a supprimé l'album photo <b>:album_nom</b>",
    "txt_13" => "Vous ne pouvez donc plus y avoir accès",
    "txt_14" => "Cette personne à probablement accepter l'invitation que vous lui avez envoyé.",
    "txt_15" => "<b>:first_name :last_name</b> vous a retiré de la liste des participants de l'album <b>:album_nom</b>",
    "txt_16" => "<b>:first_name :last_name</b> vous a retiré de la liste des viewers de l'album <b>:album_nom</b>",
    "txt_17" => "<b>:first_name :last_name</b> a été ajouté comme participant à votre album <b>:album_nom</b>",
    "txt_18" => "<b>:first_name :last_name</b> a été ajouté comme viewer à votre album <b>:album_nom</b>",
];