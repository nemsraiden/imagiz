<?php
session_start();

$env_local = 'localhost';
$env_upload = 'staging';

// local env
if($_SERVER['SERVER_NAME'] == 'imagiz.serv-management.fr/'){
    //echo 'distant';
    $env1['host'] = '127.0.0.1';
    $env1['database'] = 'servmana_imagiz';
    $env1['user'] = 'servmana_imagiz';
    $env1['password'] = 'imagiz123';
    $env1['table'] = '';
}
else{
    //echo 'local';
    $env1['host'] = 'localhost';
    $env1['database'] = 'imagiz';
    $env1['user'] = 'root';
    $env1['password'] = 'root';
    $env1['table'] = '';
}



######################################################################################

function Export_Database($host,$user,$pass,$name,  $tables=false, $backup_name=false )
{
    $mysqli = new mysqli($host,$user,$pass,$name);
    $mysqli->select_db($name);
    $mysqli->query("SET NAMES 'utf8'");

    $queryTables    = $mysqli->query('SHOW TABLES');
    while($row = $queryTables->fetch_row())
    {
        $target_tables[] = $row[0];
    }
    if($tables !== false)
    {
        $target_tables = array_intersect( $target_tables, $tables);
    }
    foreach($target_tables as $table)
    {
        $result         =   $mysqli->query('SELECT * FROM '.$table);
        $fields_amount  =   $result->field_count;
        $rows_num=$mysqli->affected_rows;
        $res            =   $mysqli->query('SHOW CREATE TABLE '.$table);

        $TableMLine     =   $res->fetch_row();
        $content        = (!isset($content) ?  '' : $content) . "\n\nDROP TABLE IF EXISTS `$table`;".$TableMLine[1].";\n\n";

        for ($i = 0, $st_counter = 0; $i < $fields_amount;   $i++, $st_counter=0)
        {
            while($row = $result->fetch_row())
            { //when started (and every after 100 command cycle):
                if ($st_counter%100 == 0 || $st_counter == 0 )
                {
                    $content .= "\nINSERT INTO ".$table." VALUES";
                }
                $content .= "\n(";
                for($j=0; $j<$fields_amount; $j++)
                {
                    $row[$j] = str_replace("\n","\\n", addslashes($row[$j]) );
                    if (isset($row[$j]))
                    {
                        $content .= '"'.$row[$j].'"' ;
                    }
                    else
                    {
                        $content .= '""';
                    }
                    if ($j<($fields_amount-1))
                    {
                        $content.= ',';
                    }
                }
                $content .=")";
                //every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
                if ( (($st_counter+1)%100==0 && $st_counter!=0) || $st_counter+1==$rows_num)
                {
                    $content .= ";";
                }
                else
                {
                    $content .= ",";
                }
                $st_counter=$st_counter+1;
            }
        } $content .="\n\n\n";
    }
    //$backup_name = $backup_name ? $backup_name : $name."___(".date('H-i-s')."_".date('d-m-Y').")__rand".rand(1,11111111).".sql";
    $backup_name = $backup_name ? $backup_name : $name.".sql";

    header('Content-Type: application/octet-stream');
    header("Content-Transfer-Encoding: Binary");
    header("Content-disposition: attachment; filename=\"".$backup_name."\"");
    echo $content; exit;
}

if(isset($_POST['check_mdp'])){
    $md5_password = md5($_POST['password']);
    if($md5_password == 'a8b6cfcaff1f58d5df0485a50b33a885'){
        $_SESSION['bddswitch'] = 'ok';
    }
}

if(isset($_POST['get_sql'])){
    Export_Database($env1['host'],$env1['user'],$env1['password'],$env1['database'],  $tables=false, $backup_name=false );
}

if(isset($_POST['exec_sql'])){

    $mysqli = new mysqli($env1['host'], $env1['user'], $env1['password'], $env1['database']);
    $sql = $_POST['sql'];
    $bdd = new PDO('mysql:host='.$env1['host'].';dbname='.$env1['database'].';charset=utf8', $env1['user'], $env1['password']);
    $result = $bdd->query('SET FOREIGN_KEY_CHECKS = 0;'.$sql.'SET FOREIGN_KEY_CHECKS = 1;');

    ?>
    <div class="alert alert-success">Query exécutée avec succès !</div>
    <?php

}

?>

<html>
<head>
    <meta charset="UTF-8">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<?php





?>

<div class="container">

    <div class="col col-s12">

    <form action="" method="post">

        <?php

        if(!isset($_SESSION['bddswitch']) && $_SESSION['bddswitch'] != 'ok'){
            ?>
            <div class="form-group">
                <br><br>
                <label for="">Password : <br/></label>
                <input type="password" name="password" id="password" class="form-control">
                <br>
                <input name="check_mdp" class="btn btn-primary" type="submit" value="Submit" />
            </div>
            <?php
        }
        else{

        ?>

            <h1>Export current database</h1>

            <strong>Env local = </strong><?= $env_local; ?> <br/>
            <strong>Env upload = </strong><?= $env_upload; ?> <br/>
            <br>


            <input name="get_sql" class="btn btn-primary" type="submit" value="Get sql file" />


            <h1>Exec mysql query</h1>

            <div class="form-group">
                <textarea name="sql" class="form-control" rows="10" id="comment"></textarea>
                <br>
                <input name="exec_sql" class="btn btn-primary" type="submit" value="Exec query" />
            </div>

        <?php

        }

        ?>

    </form>

    </div>
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</body>
</html>