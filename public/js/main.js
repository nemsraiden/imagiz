is_mobile = false;
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    is_mobile = true;
}
$(window).load(function(){
    if($(".grid").length > 0){
        $(".waiting-loader").hide();
        $(".grid").fadeIn();

        $('.grid').isotope({
            // options
            itemSelector: '.grid-item',
            layoutMode: 'masonry'
        });
    }

});


$(document).ready(function(){
    var album_id = $("#album_id").val();

    if(is_mobile){
        var showPreview = false;
    }
    else{
        var showPreview = true;
    }

    $("#images_file").fileinput({
        language: 'fr',
        showUpload:true,
        showPreview : showPreview,
        previewFileType:'any',
        allowedFileTypes: ["image"],
        maxFileCount : 20,
        initialPreviewAsData : true,
        previewSettings: {
            image: {width: "250px", height: "auto"}
        },
        uploadUrl: "/albums/"+album_id+"/photosPost",
        uploadAsync: true,
        maxFileSize: 10000,



    });

    $("#zip_file").fileinput({
        showUpload: true,
        showRemove: false,
        showPreview : false,
        required: false,
        allowedFileExtensions: ["zip"],
        maxFileCount : 1,
        language: 'fr',
        uploadUrl: "/albums/"+album_id+"/photosZip",
        uploadAsync : false,
        maxFileSize: 122880
    });

    var upload_error_total = 0;
    var upload_sucess_total = 0;
    $('#images_file').on('fileuploaderror', function(event, data, previewId, index) {
        upload_error_total++;
    });

    $('#images_file').on('fileuploaded ', function(event, data, previewId, index) {
        upload_sucess_total++;
    });

    $(".fileinput-upload-button").click(function() {
        upload_error_total = 0;
        upload_sucess_total = 0;
    });


    $('#images_file').on('filebatchuploadcomplete', function(event, files, extra) {

        if(upload_error_total == 0) {
            $("#upload_all_success").show();
            $('#images_file').fileinput('clear');
        } else {
            var total = upload_error_total + upload_sucess_total;
            $("#upload_all_error strong").text(upload_error_total + '/' + total);
            $("#upload_all_error").show();
            if(upload_sucess_total > 0){
                $("#upload_all_success").show();
            }
        }
        upload_error_total = 0;
        upload_sucess_total = 0;

        setTimeout(function(){
            $("#upload_all_success").slideUp();
            $("#upload_all_error").slideUp();
        },6000);
    });

    $('#zip_file').on('filebatchuploadcomplete', function(event, files, extra) {
        $('#zip_file').fileinput('clear');
    });

    $('#zip_file').on('filebatchuploadsuccess', function(event, data, previewId, index) {
        if(!data.response.result){
            $('#images_file').fileinput('clear');
            $("#zipUploadError").html(data.response.message);
            $("#zipUploadError").show();
        }
        else{
            $("#zipUploadError").hide();
            $("#zipUploadSuccess").show();
            setTimeout(function(){
                $("#zipUploadSuccess").slideUp();
            },4000);
        }
    });



    url = $("#get_js_url").val();

    if($( ".alert-success" ).length){

        function hideSuccess() {
            $( ".alert-success:not(.stay)").slideUp(0500);
        }

        // use setTimeout() to execute
        setTimeout(hideSuccess, 4000);
    }






    $(function()
    {
        if($( "#q").length > 0){
            $( "#q" ).autocomplete({
                source: $("#get_js_url").val()+"/search/autocomplete",
                minLength: 3,
                select: function(event, ui) {
                    $('#q').html(ui.item.value);
                    $('#q_id').val(ui.item.id);
                    $(".liste-participants-resultats").hide();
                },
                response: function( event, ui ) {
                    $(".liste-participants-resultats").css('border-bottom','solid 1px #4e5d6c');
                },
                appendTo:'.liste-participants-resultats'
            });
        }

    });


    //Enable Bootstrap-Select via JavaScript:

    $('#albums_selection.selectpicker').selectpicker({
        title : "Choisir un ou plusieurs albums..."
    });
    $('#albums_selection.selectpicker').val('');

    $('#albums_selection.selectpicker').on('changed.bs.select', function (e) {
        albumsSelectionChange();
    });
    $("#albums_selection").on('change', function() {
        albumsSelectionChange();
    });

    /*$('#albums_selection.selectpicker').on('change', function(){
        albumsSelectionChange();
    });*/

    function albumsSelectionChange(){
        $(".erreur_search").hide();
        $("#type_de_photo").show();
        $(".personnes_selection").show();

        var albums_selection = $( '#albums_selection' ).val();
        if(albums_selection != null){
            albums_selection = albums_selection.toString();

            $.get( '/recherche/list_participants/'+albums_selection)
                .done(function( participants ) {
                    //personnes_selection
                    $("#personnes_selection").html('');

                    $.each(participants, function(index, value) {
                        if($("#personnes_selection option[value="+index+"]").length == 0){
                            $("#personnes_selection").append('<option value="'+index+'">'+value+'</option>');
                        }
                    });
                    $('.selectpicker').selectpicker('refresh');
                },
                'json');

            $("#recherche").trigger('click');
        }
        else{
            $("#personnes_selection").html('');
            $('.selectpicker').selectpicker('refresh');
            $("#type_de_photo").hide();
            $(".personnes_selection").hide();
        }
    }

    $('#personnes_selection.selectpicker').selectpicker({
        title : "Choisir des personnes..."
    });
    $('#personnes_selection.selectpicker').val('');

    $("#checkbox_personnes").change(function() {
        $(".erreur_search").hide();
        if($(this).is(":checked")) {
            $(".personnes_selection").show();
        }
        else{
            $(".personnes_selection").hide();
        }
    });

    $("#search-form").on('submit',function(){

        var albums_selection = $( '#albums_selection' ).val();
        var personnes_selection = $( '#personnes_selection' ).val();
        var checkbox_personnes = $( '#checkbox_personnes' ).is(":checked");
        var checkbox_paysage = $( '#checkbox_paysage' ).is(":checked");
        var checkbox_together_on_photo = $( '#checkbox_together_on_photo' ).is(":checked");
        var checkbox_alone_on_photo = $( '#checkbox_alone_on_photo' ).is(":checked");

        var modeList = [
            'lg-slide',
            'lg-fade',
            'lg-zoom-in',
            'lg-zoom-in-big',
            'lg-zoom-out',
            'lg-zoom-out-big',
            'lg-zoom-out-in',
            'lg-zoom-in-out',
            'lg-soft-zoom',
            'lg-scale-up',
            'lg-slide-skew-only',
            'lg-slide-skew-only-rev',
            'lg-slide-skew-only-y',
            'lg-slide-skew-only-y-rev',
            'lg-slide-skew',
            'lg-slide-skew-rev',
            'lg-lollipop',
            'lg-lollipop-rev',
            'lg-rotate',
            'lg-rotate-rev',
            'lg-tube'
        ];


        if(albums_selection == null){
            $(".erreur_search .txt").html('Vous devez choisir au minimum 1 album');
            $(".erreur_search").show();
            $(".erreur_search").effect( "shake", {times:1}, 400 );
        }
        else if(!checkbox_personnes && !checkbox_paysage){
            $(".erreur_search .txt").html('Vous devez choisir au minimum 1 type de photo');
            $(".erreur_search").show();
            $(".erreur_search").effect( "shake", {times:1}, 400 );
        }
        else{
            $("#search_results").html('');

            $(".body_lightbox_loading").fadeIn();

            jQuery.post(

                $( this ).prop( 'action' ),
                {
                    "_token": $( this ).find( 'input[name=_token]' ).val(),
                    "albums_selection": albums_selection,
                    "checkbox_personnes": checkbox_personnes,
                    "checkbox_paysage": checkbox_paysage,
                    "personnes_selection": personnes_selection,
                    "checkbox_together_on_photo": checkbox_together_on_photo,
                    "checkbox_alone_on_photo": checkbox_alone_on_photo
                },
                function( data ) {
                    $(".body_lightbox_loading").fadeOut();
                    if(!data.success){
                        alert('Il y a eu une erreur.');
                    }
                    else{
                        if(data.html == '<div id="lightgallery"><\/div>'){
                            $("#search_results").append("<div class='search_aucun_resultat'>Aucun résultat trouvé pour cette recherche.<br/>Merci d'essayer d'autres critères de recherche </div>");
                        }
                        else{
                            $("#search_results").append(data.html);
                            var $lightgallery = $("#lightgallery");
                            $lightgallery.lightGallery({
                                thumbnail:true,
                                mode : 'lg-zoom-in'
                            });

                            $lightgallery.on('onAfterSlide.lg', function(event, prevIndex, index){
                                var random = Math.floor(Math.random() * (modeList.length - 1)) + 1;
                                $lightgallery.data('lightGallery').changeMode(modeList[random]);
                            });
                        }

                    }
                },
                'json'
            );
        }

        return false;
    });

    $('#amis_selection.selectpicker').selectpicker({
        title : "Choisir un amis",
    });



    // retirer personne a une image
    $(".delete.untag-user").click(function(e){
        if($("#picture_tag_personnes ul li").length == 1){
            // il y en aura donc 0 après suppression
            $("#only_tagged_onphoto").hide();
        }
    });

    // tag de personne a une image
    $("#tag_personne").on('submit',function(){

        var user_id = $( '#user_id' ).val();
        $("#only_tagged_onphoto").show();

        $("#picture_change_data_loading div").show();

        if(user_id != null && user_id != ''){
            $.post(

                $( this ).prop( 'action' ),
                {
                    "_token": $( this ).find( 'input[name=_token]' ).val(),
                    "picture_id": $( '#picture_id' ).val(),
                    "album_id": $( '#album_id' ).val(),
                    "user_id": user_id
                },
                function( data ) {
                    if(!data.success){
                        alert('Il y a eu une erreur.');
                    }
                    else{
                        var txt = $('#tag_personne option[value="'+user_id+'"]').text();
                        $('#tag_personne option[value="'+user_id+'"]').remove();

                        if($('#tag_personne option').length == 0){
                            // la liste est vide
                            $('#tag_personne select').html('<option></option>');
                            $("#tag_personne").hide();
                        }

                        $("#picture_tag_personnes ul").append('<li>'+txt+'</li>')
                        $("#untaged").hide();
                    }
                    $("#picture_change_data_loading div").hide();
                },
                'json'
            );
        }



        return false;
    });


    // detecte change in picture tag (change type photo)
    $('#picture_type_photo option:first').attr('disabled', true);

    $("#picture_type_photo").on('change',function(){
        $(this).closest('form').submit();
    });

    $("#picture_change_type").submit( function() {

        $("#picture_change_data_loading div").show();

        $.post(
            $( this ).prop( 'action' ),
            {
                "_token": $( this ).find( 'input[name=_token]' ).val(),
                "picture_id": $( '#picture_id' ).val(),
                "album_id": $( '#album_id' ).val(),
                "picture_type_photo": $( '#picture_type_photo' ).val()
            },
            function( data ) {
                if(!data.success){
                    alert('Il y a eu une erreur.');
                }
                else{
                    var val = $("#picture_type_photo").val();
                    if(val != 'personnes'){
                        $("#picture_tag_personnes").slideUp();
                        $("#picture_tag_personnes ul").html('');
                        $("#only_tagged_people").attr('checked',false);
                        $("#untaged").hide();
                    }
                    else{
                        $("#picture_tag_personnes").slideDown();
                    }
                }
                $("#picture_change_data_loading div").hide();
            },
            'json'
        );

        //prevent the form from actually submitting in browser
        return false;
    } );


    // definir une picture en tant que jaquette de l'album

    $("#picture_jaquette").change(function(){
        $(this).closest('form').submit();
    });
    $("#picture_checkbox_jaquette").submit( function() {

        $("#picture_change_data_loading div").show();

        $.post(
            $( this ).prop( 'action' ),
            {
                "_token": $( this ).find( 'input[name=_token]' ).val(),
                "picture_id": $( '#picture_id' ).val(),
                "album_id": $( '#album_id' ).val(),
                "picture_jaquette": $("#picture_jaquette").is( ':checked' )
            },
            function( data ) {
                if(!data.success){
                    //alert('Il y a eu une erreur.');
                }
                $("#picture_change_data_loading div").hide();
            },
            'json'
        );

        //prevent the form from actually submitting in browser
        return false;
    } );

    // seul les personnes taguées sont présentes sur une photo

    $("#only_tagged_people").change(function(){
        var is_checked = $(this).is(':checked');
        var href = $("#only_tagged_onphoto").children('span').html();
        $("#picture_change_data_loading div").show();
        href = href.replace('onlytagged',is_checked);
        $.get(
           href,
           {
           },
           function( data ) {
               $("#picture_change_data_loading div").hide();

           },
           'json'
        );

    });

    $( "#only_tagged_onphoto .fa-question-circle" ).tooltip({
        position: {
            using: function( position, feedback ) {
                $( this ).css( 'width','500px' );
            }
        }
    });


    // remove le tag d'une personne sur une poto.
    // empeche le lien normal pour faire un appel ajax

    $(".untag-user").click(function(e){
        e.stopPropagation();
        e.preventDefault();
        var that = this;

        var href = $(this).attr('href');
        $.get(
            href,
            {
            },
            function( data ) {
                if(!data.success){
                    //alert('Il y a eu une erreur.');
                }
                else{
                    $(that).parent().remove();
                }
            },
            'json'
        );
    });


    $("#closealertes").click(function(){
       $(".myalertesbgc").fadeOut();
    });


    $("#supprimer_image").click(function(){

        var picture_id = $("#picture_id").val();
        var album_id = $("#album_id").val();

        var url = $(".body-lightbox-delete .supprimer-img a").attr('href');
        url = url.replace('{id}',album_id);
        url = url.replace('{id_picture}',picture_id);
        $(".body-lightbox-delete a").attr('href',url);

        $(".body-lightbox-delete .supprimer-img").show();
        $(".body-lightbox-delete").fadeIn(0700);
    });

    $(".body-lightbox-delete").click(function(){
        if ($('.body-lightbox-delete div:hover').length == 0) {
            $(".body-lightbox-delete div").hide();
            $(".body-lightbox-delete").fadeOut(0700);
        }
    });
    $(".body-lightbox-delete button").click(function(){
        $(".body-lightbox-delete div").hide();
        $(".body-lightbox-delete").fadeOut(0700);
    });

    $("#supprimer_album").click(function(){

        var album_id = $("#album_id").val();

        var url = $(".body-lightbox-delete .supprimer-album a").attr('href');
        url = url.replace('{id}',album_id);
        $(".body-lightbox-delete a").attr('href',url);

        $(".body-lightbox-delete .supprimer-album").show();
        $(".body-lightbox-delete").fadeIn(0700);
    });


    $("#supprimer_album_photos").click(function(){
        var album_id = $("#album_id").val();

        var url = $(".body-lightbox-delete .supprimer-album-photos a").attr('href');
        url = url.replace('{id}',album_id);
        $(".body-lightbox-delete a").attr('href',url);

        $(".body-lightbox-delete .supprimer-album-photos").show();
        $(".body-lightbox-delete").fadeIn(0700);
    });



    $("#deplacer_image").click(function(){

        $(".move-image .cadre .message").show();
        $(".move-image .cadre .load-deplacement-img").hide();
        $("#move_images_error").hide();
        $(".deplacement-ok").hide();

        $(".move-image").fadeIn();
        $(".move-image").unbind();
        $(".move-image").click(function(){
            if(!$(".move-image .cadre").is(':hover')){
                $(this).fadeOut();

            }
        });


        $(".move-image .cadre .close").unbind();
        $(".move-image .cadre #action_annuler").click(function(){
            $(".move-image").fadeOut();
        });
    });

    $("#action_move_images").click(function(){
        $("#move_images_error").hide();

        var picture_id = $("#picture_id").val();
        var picture_id = $("#picture_id").val();
        var albums = $("#albums_selection").val();
        var url = $(this).closest('form').attr('action');
        var move_and_delete = $("#move_and_delete").is(':checked');

        if(!albums){
            $("#move_images_error").show();
        }
        else{
            var albums_ids = albums.join(',');
            url = url.replace('albums_id',albums_ids);
            url = url.replace('keep_files',move_and_delete);

            $(".move-image .cadre .message").hide();
            $(".move-image .cadre .load-deplacement-img").show();


            $.get(
                url,
                {
                },
                function( data ) {
                    if(data.success){

                        if(move_and_delete == false){
                            $(".move-image .cadre .load-deplacement-img").hide();
                            $(".deplacement-ok").show();
                        }
                        else{
                            $(location).attr('href','/albums/'+$("#album_id").val()+'/');
                        }

                    }
                    else{
                        $(".move-image .cadre .load-deplacement-img").hide();
                        $(".move-image").hide();

                    }
                },
                'json'
            );
        }

    });


    $(".albumshow #albums_show_photos").click(function(){
        $(".albumshow #albums_videos").hide();
        $(".albumshow #albums_photos").show();
        $(this).prop('disabled',true);
        $(".albumshow #albums_show_videos").prop('disabled',false);

    });

    $(".albumshow #albums_show_videos").click(function(){
        $(".albumshow #albums_photos").hide();
        $(".albumshow #albums_videos").show();
        $(this).prop('disabled',true);
        $(".albumshow #albums_show_photos").prop('disabled',false);
    });

    $(".actions .submenu").click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        if($(this).parent().children('ul').css('display') === 'none') {
            $(this).parent().children('ul').slideDown();
        } else {
            $(this).parent().children('ul').slideUp();
        }

    });

    $(document).click(function() {
        $('.actions ul li ul').slideUp();
    });


});



